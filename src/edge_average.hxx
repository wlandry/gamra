#pragma once

/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

/// Helper templates that make it easy to refer to node/edge data
/// without having to duplicate code for 2D and 3D.

#include "Dir.hxx"

#include <SAMRAI/pdat/EdgeData.h>
#include <SAMRAI/pdat/SideIndex.h>

inline double edge_average(const SAMRAI::pdat::EdgeData<double> &edge,
                           const SAMRAI::pdat::SideIndex &s,
                           const Gamra::Dir &ix,
                           const SAMRAI::hier::Index pp[],
                           const int &depth, const Gamra::Dir &dim)
{
  const Gamra::Dir iy(ix.next(dim));
  SAMRAI::pdat::EdgeIndex ey(s,iy,SAMRAI::pdat::EdgeIndex::LowerLeft);
  if(dim==2)
    {
      return (edge(ey,depth) + edge(ey+pp[iy],depth))/2;
    }
  else
    {
      const Gamra::Dir iz(iy.next(dim));
      SAMRAI::pdat::EdgeIndex ez(s,iz,SAMRAI::pdat::EdgeIndex::LowerLeft);
      return (edge(ey,depth) + edge(ey+pp[iy],depth)
              + edge(ez,depth) + edge(ez+pp[iz],depth))/4;
    }
}
