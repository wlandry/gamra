/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../Visit_Writer.hxx"
#include "Constants.hxx"

#include <SAMRAI/pdat/CellData.h>
#include <SAMRAI/pdat/SideData.h>
#include <SAMRAI/pdat/EdgeData.h>
#include <SAMRAI/geom/CartesianPatchGeometry.h>

/// Strain correction is stored on the cell centers and edges

void Visit_Writer::pack_applied_strain(double* buffer,
                                       const SAMRAI::hier::Patch& patch,
                                       const SAMRAI::hier::Box& region,
                                       const int &depth) const
{
  boost::shared_ptr<SAMRAI::pdat::CellData<double> > dstrain_diagonal;
  boost::shared_ptr<SAMRAI::pdat::EdgeData<double> > dstrain_mixed;
  bool have_applied_strain(dstrain_diagonal_id!=invalid_id
                           && dstrain_mixed_id!=invalid_id);
  if(have_applied_strain)
    {
      dstrain_diagonal=
        boost::dynamic_pointer_cast<SAMRAI::pdat::CellData<double> >
        (patch.getPatchData(dstrain_diagonal_id));
      dstrain_mixed=boost::dynamic_pointer_cast<SAMRAI::pdat::EdgeData<double> >
        (patch.getPatchData(dstrain_mixed_id));
    }

  const Gamra::Dir dim=dimension.getValue();

  /// In 2D, set iz to iy to get proper edge behavior
  Gamra::Dir ix(Gamra::Dir::from_int(depth/dim)),
    iy(Gamra::Dir::from_int(depth%dim)), iz(dim==2 ? iy : iy.next(dim));
  if(iz==ix)
    iz=iz.next(dim);
  
  const SAMRAI::hier::Index zero(SAMRAI::hier::Index::getZeroIndex(dimension));
  SAMRAI::hier::Index jp(zero);
  jp[iy]=1;

  SAMRAI::pdat::CellIterator iend(SAMRAI::pdat::CellGeometry::end(region));
  for(SAMRAI::pdat::CellIterator
        icell(SAMRAI::pdat::CellGeometry::begin(region));
      icell!=iend; ++icell)
    {
      if(have_applied_strain)
        {
          if(ix==iy)
            {
              *buffer=(*dstrain_diagonal)(*icell,ix);
            }
          else
            {
              /// FIXME: This is incorrect if applied strain is not
              /// symmetric

              /// FIXME: Is this really correct?  In 2D, it averages
              /// over iy.  In 3D, it averages over iz.  Shouldn't it
              /// average over 4 points: 0, ix, iy, ix+iy?
              const int dstrain_ix_iy(dstrain_index_map(ix,iy,dim));
              SAMRAI::pdat::EdgeIndex
                edge_z(*icell,iz,SAMRAI::pdat::EdgeIndex::LowerLeft);
              *buffer=((*dstrain_mixed)(edge_z,dstrain_ix_iy)
                       + (*dstrain_mixed)(edge_z+jp,dstrain_ix_iy))/2;
            }
        }
      else
        {
          *buffer=0;
        }
      ++buffer;
    }
}
