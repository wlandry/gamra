/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../Visit_Writer.hxx"
#include "Constants.hxx"

#include <SAMRAI/pdat/CellData.h>
#include <SAMRAI/pdat/SideData.h>

void Visit_Writer::pack_christof(double* buffer,
                                 const SAMRAI::hier::Patch& patch,
                                 const SAMRAI::hier::Box& region,
                                 const int &depth,
                                 const int &index0) const
{
  const Gamra::Dir dim=dimension.getValue();
  int index2(depth%dim);

  auto &christof_cell
    (*boost::dynamic_pointer_cast<SAMRAI::pdat::CellData<double> >
     (patch.getPatchData(christof_cell_id)));

  /// 2D: C_side={xxx, xxy, yyx, yyy}
  int element(index0*dim + index2);
      
  auto cell_end(SAMRAI::pdat::CellGeometry::end(region));
  for (auto cell(SAMRAI::pdat::CellGeometry::begin(region));
       cell!=cell_end; ++cell)
    {
      *buffer=christof_cell(*cell,element);
      ++buffer;
    }
}
