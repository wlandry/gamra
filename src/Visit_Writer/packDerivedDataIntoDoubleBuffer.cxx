/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../Visit_Writer.hxx"

#include <boost/algorithm/string.hpp>

bool Visit_Writer::packDerivedDataIntoDoubleBuffer
(double* buffer,
 const SAMRAI::hier::Patch& patch,
 const SAMRAI::hier::Box& region,
 const std::string& variable_name,
 int depth, double) const
{
  /// No need to ghostfill.  This function will only be called after a
  /// check to computeCompositeResidualOnLevel(), which does its own
  /// ghostfill.
  if(variable_name=="Induced Strain" || variable_name=="Strain")
    {
      pack_induced_strain(buffer,patch,region,depth);
    }
  else if(variable_name=="Induced Stress" || variable_name=="Deviatoric Stress")
    {
      pack_induced_stress(buffer,patch,region,depth);
    }
  else if(variable_name=="Applied Strain")
    {
      pack_applied_strain(buffer,patch,region,depth);
    }
  else if(variable_name=="Initial Displacement")
    {
      pack_Input_Expression(buffer,patch,region,depth,v_initial);
    }
  else if (variable_name == "Displacement" || variable_name == "Velocity")
    {
      pack_vector(buffer,patch,region,depth,v_id);
    }
  else if (variable_name == "Fault Correction + RHS")
    {
      pack_vector(buffer,patch,region,depth,v_rhs_id);
    }
  else if (variable_name == "Metric")
    {
      pack_vector(buffer,patch,region,depth,gg_id);
    }
  else if (boost::starts_with(variable_name,"Christof_"))
    {
      pack_christof(buffer,patch,region,depth,(*variable_name.rbegin())-'0');
    }
  else
    {
      TBOX_ERROR("Unregistered variable name '" << variable_name << "' in\n"
                 << "Elastic::Visit_Writer::packDerivedDataIntoDoubleBuffer");
    }
  /// Always return true, since every patch has derived data.
  return true;
}
