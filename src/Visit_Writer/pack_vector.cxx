/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../Visit_Writer.hxx"
#include "Constants.hxx"

#include <SAMRAI/pdat/CellData.h>
#include <SAMRAI/pdat/SideData.h>

void Visit_Writer::pack_vector(double* buffer,
                               const SAMRAI::hier::Patch& patch,
                               const SAMRAI::hier::Box& region,
                               const int &depth,
                               const int &id) const
{
  boost::shared_ptr<SAMRAI::pdat::SideData<double> > ptr;
  ptr = boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
    (patch.getPatchData(id));
  SAMRAI::pdat::SideData<double>& v = *ptr;

  SAMRAI::hier::Index dp(dimension,0);
  dp[depth]=1;
  SAMRAI::pdat::CellIterator iend(SAMRAI::pdat::CellGeometry::end(region));
  for (SAMRAI::pdat::CellIterator
         icell(SAMRAI::pdat::CellGeometry::begin(region));
       icell!=iend; ++icell)
    {
      const SAMRAI::pdat::SideIndex
        d(*icell,depth,SAMRAI::pdat::SideIndex::Lower);
      *buffer=v(d);
      if(!offset_vector_on_output)
        { *buffer=(v(d+dp) + *buffer)/2; }
      ++buffer;
    }
}
