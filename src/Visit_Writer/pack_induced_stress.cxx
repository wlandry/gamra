/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../Visit_Writer.hxx"
#include "Constants.hxx"

#include <SAMRAI/pdat/CellData.h>
#include <SAMRAI/pdat/SideData.h>
#include <SAMRAI/pdat/EdgeData.h>
#include <SAMRAI/geom/CartesianPatchGeometry.h>

void Visit_Writer::pack_induced_stress(double* buffer,
                                       const SAMRAI::hier::Patch& patch,
                                       const SAMRAI::hier::Box& region,
                                       const int &depth) const
{
  boost::shared_ptr<SAMRAI::pdat::SideData<double> > v_ptr=
    boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
    (patch.getPatchData(v_id));
  SAMRAI::pdat::SideData<double>& v = *v_ptr;

  const bool have_faults=(dv_diagonal_id!=invalid_id && dv_mixed_id!=invalid_id);
  boost::shared_ptr<SAMRAI::pdat::CellData<double> > dv_diagonal;
  boost::shared_ptr<SAMRAI::pdat::SideData<double> > dv_mixed;
  if(have_faults)
    {
      dv_diagonal=boost::dynamic_pointer_cast<SAMRAI::pdat::CellData<double> >
        (patch.getPatchData(dv_diagonal_id));
      dv_mixed=boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
        (patch.getPatchData(dv_mixed_id));
    }
  const int Lame_depth(incompressible ? 0 : 1);
  boost::shared_ptr<SAMRAI::pdat::CellData<double> >
    Lame_param(boost::dynamic_pointer_cast<SAMRAI::pdat::CellData<double> >
               (patch.getPatchData(Lame_param_id)));
         
  const Gamra::Dir dim=dimension.getValue();
  Gamra::Dir ix(Gamra::Dir::from_int(depth/dim)),
    iy(Gamra::Dir::from_int(depth%dim));
  
  SAMRAI::hier::Index ip(dimension,0), jp(dimension,0);
  ip[ix]=1;
  jp[iy]=1;

  const double *dx(boost::dynamic_pointer_cast
                   <SAMRAI::geom::CartesianPatchGeometry>
                   (patch.getPatchGeometry())->getDx());

  SAMRAI::pdat::CellIterator iend(SAMRAI::pdat::CellGeometry::end(region));
  for(SAMRAI::pdat::CellIterator
        icell(SAMRAI::pdat::CellGeometry::begin(region));
      icell!=iend; ++icell)
    {
      if(ix==iy)
        {
          const SAMRAI::pdat::SideIndex
            s(*icell,ix,SAMRAI::pdat::SideIndex::Lower);
          double strain=uncorrected_strain(s,ip,jp,ix,iy,dx,v);
          if(have_faults)
            { strain+=strain_correction(s,*icell,ip,jp,ix,iy,dim,dx,*dv_diagonal,
                                        *dv_mixed); }
          *buffer=2*(*Lame_param)(*icell,Lame_depth)*strain;
          if(!incompressible)
            {
              for(Gamra::Dir d=ix.next(dim); d!=ix; d=d.next(dim))
                {
                  const SAMRAI::pdat::SideIndex
                    s_d(*icell,d,SAMRAI::pdat::SideIndex::Lower);
                  SAMRAI::hier::Index dp(dimension,0);
                  dp[d]=1;
                  strain+=uncorrected_strain(s_d,dp,dp,d,d,dx,v);
                  if(have_faults)
                    { strain+=strain_correction(s_d,*icell,dp,dp,d,d,dim,dx,
                                                *dv_diagonal,*dv_mixed); }
                }
              *buffer+=(*Lame_param)(*icell,0)*strain;
            }
        }
      else
        {
          const SAMRAI::pdat::SideIndex
            s_x(*icell,ix,SAMRAI::pdat::SideIndex::Lower),
            s_y(*icell,iy,SAMRAI::pdat::SideIndex::Lower);
            
          double strain_xy(uncorrected_strain(s_x,ip,jp,ix,iy,dx,v)),
            strain_yx(uncorrected_strain(s_y,jp,ip,iy,ix,dx,v));
          if(have_faults)
            {
              strain_xy+=strain_correction(s_x,*icell,ip,jp,ix,iy,dim,dx,
                                           *dv_diagonal,*dv_mixed);
              strain_yx+=strain_correction(s_y,*icell,jp,ip,iy,ix,dim,dx,
                                           *dv_diagonal,*dv_mixed);
            }
          *buffer=(*Lame_param)(*icell,Lame_depth)*(strain_xy + strain_yx);
        }
      ++buffer;
    }
}
