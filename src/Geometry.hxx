#pragma once

enum class Geometry
{
  CARTESIAN, SPHERICAL, TOPOGRAPHY
};
