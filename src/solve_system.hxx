#pragma once

/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "Stokes/Adaptive_Solver.hxx"
#include "Elastic/Adaptive_Solver.hxx"
#include "Visit_Writer.hxx"

#include <SAMRAI/tbox/Database.h>
#include <SAMRAI/tbox/InputDatabase.h>
#include <SAMRAI/hier/PatchHierarchy.h>
#include <SAMRAI/mesh/StandardTagAndInitialize.h>
#include <SAMRAI/mesh/BergerRigoutsos.h>
#include <SAMRAI/mesh/TreeLoadBalancer.h>
#include <SAMRAI/mesh/GriddingAlgorithm.h>

void setup_visit_fields(const std::vector<std::string> &output_fields,
                        const Elastic::Adaptive_Solver &elastic,
                        SAMRAI::appu::VisItDataWriter &data_writer,
                        Visit_Writer &writer);
void setup_visit_fields(const std::vector<std::string> &output_fields,
                        const Stokes::Adaptive_Solver &stokes,
                        SAMRAI::appu::VisItDataWriter &data_writer,
                        Visit_Writer &writer);

template<class T>
bool solve_system
(T &adaptive_solver,
 boost::shared_ptr<SAMRAI::tbox::Database> &main_db,
 boost::shared_ptr<SAMRAI::tbox::InputDatabase> &input_db,
 boost::shared_ptr<SAMRAI::hier::PatchHierarchy> &patch_hierarchy,
 const std::string &base_name,
 const SAMRAI::tbox::Dimension &dim)
{
  boost::shared_ptr<SAMRAI::mesh::StandardTagAndInitialize>
    tag_and_initializer(new SAMRAI::mesh::StandardTagAndInitialize
                        ("CellTaggingMethod",&adaptive_solver,
                         input_db->getDatabase("StandardTagAndInitialize")));

  boost::shared_ptr<SAMRAI::mesh::BergerRigoutsos>
    box_generator(new SAMRAI::mesh::BergerRigoutsos(dim));
  boost::shared_ptr<SAMRAI::mesh::TreeLoadBalancer>
    load_balancer(new SAMRAI::mesh::TreeLoadBalancer
                  (dim,
                   "load balancer",
                   boost::shared_ptr<SAMRAI::tbox::Database>()));
  load_balancer->setSAMRAI_MPI(SAMRAI::tbox::SAMRAI_MPI::getSAMRAIWorld());

  SAMRAI::mesh::GriddingAlgorithm
    gridding_algorithm(patch_hierarchy,
                       "Gridding Algorithm",
                       input_db->getDatabase("GriddingAlgorithm"),
                       tag_and_initializer,
                       box_generator,
                       load_balancer);
  gridding_algorithm.makeCoarsestLevel(0.0);

  bool intermediate_output(main_db->getBoolWithDefault("intermediate_output",
                                                       false));
  const std::vector<std::string> output_fields
    (main_db->keyExists("output_fields")
     ? main_db->getStringVector("output_fields")
     : adaptive_solver.default_output_fields());
  SAMRAI::appu::VisItDataWriter
    samrai_visit_writer(dim,"Visit Writer",base_name);
  Visit_Writer visit_writer(adaptive_solver,main_db->getBoolWithDefault
                            ("offset_vector_on_output",false));
  setup_visit_fields(output_fields,adaptive_solver,samrai_visit_writer,
                     visit_writer);

  if(main_db->getBoolWithDefault("print_input_file",true))
    input_db->printClassData(SAMRAI::tbox::plog);

  int lnum = 0;
  bool converged(adaptive_solver.solve_hierarchy());
  for (;patch_hierarchy->levelCanBeRefined(lnum) && converged; ++lnum)
    {
      if (intermediate_output)
        samrai_visit_writer.writePlotData(patch_hierarchy, lnum);
      std::vector<int> tag_buffer(patch_hierarchy->getMaxNumberOfLevels(),1);
      gridding_algorithm.regridAllFinerLevels(0,tag_buffer,0,0.0);
      SAMRAI::tbox::plog << "Newly adapted hierarchy\n";

      converged=adaptive_solver.solve_hierarchy();
      SAMRAI::tbox::TimerManager::getManager()->print(SAMRAI::tbox::plog);
    }
  samrai_visit_writer.writePlotData(patch_hierarchy, lnum);

  return converged;
}


