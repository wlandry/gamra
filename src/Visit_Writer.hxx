#pragma once
/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "Elastic/Adaptive_Solver.hxx"
#include "Stokes/Adaptive_Solver.hxx"

#include <SAMRAI/appu/VisDerivedDataStrategy.h>
#include <SAMRAI/appu/VisItDataWriter.h>

class Visit_Writer: public SAMRAI::appu::VisDerivedDataStrategy
{
public:
  SAMRAI::tbox::Dimension dimension;
  /// Whether to offset the vector when outputing.  This is useful
  /// for convergence tests, because it removes interpolation
  /// errors.  This is especially important near faults where there
  /// is a jump in values

  bool offset_vector_on_output, incompressible;
  int v_id, v_rhs_id, dv_diagonal_id, dv_mixed_id, dstrain_diagonal_id,
    dstrain_mixed_id, Lame_param_id, xyz_id{invalid_id}, gg_id{invalid_id},
    christof_cell_id{invalid_id};
  const std::vector<Input_Expression> &v_initial;

  Visit_Writer(const Elastic::Adaptive_Solver &elastic,
               const bool &Offset_vector_on_output):
    dimension(elastic.dimension),
    offset_vector_on_output(Offset_vector_on_output),
    incompressible(false),
    v_id(elastic.v_id), v_rhs_id(elastic.v_rhs_id),
    dv_diagonal_id(elastic.dv_diagonal_id),
    dv_mixed_id(elastic.dv_mixed_id),
    dstrain_diagonal_id(elastic.dstrain_diagonal_id),
    dstrain_mixed_id(elastic.dstrain_mixed_id),
    Lame_param_id(elastic.cell_moduli_id),
    xyz_id(elastic.xyz_id),
    gg_id{elastic.gg_id},
    christof_cell_id{elastic.christof_cell_id},
    v_initial(elastic.v_initial) {}

  Visit_Writer(const Stokes::Adaptive_Solver &stokes,
               const bool &Offset_vector_on_output):
    dimension(stokes.dimension),
    offset_vector_on_output(Offset_vector_on_output),
    incompressible(true),
    v_id(stokes.v_id), v_rhs_id(invalid_id),
    dv_diagonal_id(invalid_id),
    dv_mixed_id(invalid_id),
    dstrain_diagonal_id(invalid_id),
    dstrain_mixed_id(invalid_id),
    Lame_param_id(stokes.cell_viscosity_id),
    v_initial(stokes.v_initial) {}

  virtual ~Visit_Writer() {}

  virtual bool
  packDerivedDataIntoDoubleBuffer(double* buffer,
                                  const SAMRAI::hier::Patch& patch,
                                  const SAMRAI::hier::Box& region,
                                  const std::string& variable_name,
                                  int depth, double) const;

  void pack_induced_stress(double* buffer,
                           const SAMRAI::hier::Patch& patch,
                           const SAMRAI::hier::Box& region,
                           const int &depth) const;

  void pack_induced_strain(double* buffer,
                           const SAMRAI::hier::Patch& patch,
                           const SAMRAI::hier::Box& region,
                           const int &depth) const;

  void pack_applied_strain(double* buffer,
                           const SAMRAI::hier::Patch& patch,
                           const SAMRAI::hier::Box& region,
                           const int &depth) const;

  void pack_vector(double* buffer,
                   const SAMRAI::hier::Patch& patch,
                   const SAMRAI::hier::Box& region,
                   const int &depth,
                   const int &v_id) const;

  void pack_Input_Expression(double* buffer,
                             const SAMRAI::hier::Patch& patch,
                             const SAMRAI::hier::Box& region,
                             const int &depth,
                             const std::vector<Input_Expression> &input) const;

  void pack_metric(double* buffer,
                   const SAMRAI::hier::Patch& patch,
                   const SAMRAI::hier::Box& region,
                   const int &depth) const;

  void pack_christof(double* buffer,
                     const SAMRAI::hier::Patch& patch,
                     const SAMRAI::hier::Box& region,
                     const int &depth,
                     const int &index) const;

  double uncorrected_strain(const SAMRAI::pdat::SideIndex &s,
                            const SAMRAI::hier::Index &ip,
                            const SAMRAI::hier::Index &jp,
                            const Gamra::Dir &ix,
                            const Gamra::Dir &iy,
                            const double dx[],
                            const SAMRAI::pdat::SideData<double> &v) const
  {
    if(ix==iy)
      return (v(s+ip)-v(s))/dx[ix];
    return (- v(s-jp) - v(s+ip-jp) + v(s+jp)  + v(s+ip+jp))/(4*dx[iy]);
  }
  double strain_correction(const SAMRAI::pdat::SideIndex &s,
                           const SAMRAI::pdat::CellIndex &cell,
                           const SAMRAI::hier::Index &ip,
                           const SAMRAI::hier::Index &jp,
                           const Gamra::Dir &ix,
                           const Gamra::Dir &iy,
                           const Gamra::Dir &dim,
                           const double dx[],
                           const SAMRAI::pdat::CellData<double> &dv_diagonal,
                           const SAMRAI::pdat::SideData<double> &dv_mixed) const
  {
    if(ix==iy)
      return -dv_diagonal(cell,ix)/dx[ix];
    const int dv_ix_iy(dv_index_map(ix,iy,dim));
    return (dv_mixed(s,dv_ix_iy+1) - dv_mixed(s-jp,dv_ix_iy)
            + dv_mixed(s+ip,dv_ix_iy+1) - dv_mixed(s+ip-jp,dv_ix_iy)
            + dv_mixed(s+jp,dv_ix_iy+1) - dv_mixed(s,dv_ix_iy)
            + dv_mixed(s+ip+jp,dv_ix_iy+1) - dv_mixed(s+ip,dv_ix_iy))/(4*dx[iy]);
  }
                
};
