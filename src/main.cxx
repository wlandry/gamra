/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <boost/filesystem.hpp>
#include <SAMRAI/tbox/InputManager.h>
#include <SAMRAI/geom/CartesianGridGeometry.h>
#include <SAMRAI/hier/RefineOperator.h>
#include <SAMRAI/hier/CoarsenOperator.h>
#include <SAMRAI/pdat/CellVariable.h>
#include <SAMRAI/pdat/SideVariable.h>
#include <SAMRAI/geom/CartesianCellDoubleWeightedAverage.h>

#include "solve_system.hxx"

int main(int argc, char* argv[])
{
  SAMRAI::tbox::SAMRAI_MPI::init(&argc, &argv);
  SAMRAI::tbox::SAMRAIManager::initialize();
  SAMRAI::tbox::SAMRAIManager::startup();

  bool converged;
  /// The original example from SAMAI said that we have to create a
  /// block in order to force pointer deallocation and prevent leaks.
  /// Not sure about this, but it is easy enough to add.
  {
    std::string input_filename;
    if (argc != 2)
      {
        TBOX_ERROR("USAGE:  " << argv[0] << " <input file> \n");
      }
    else
      {
        input_filename = argv[1];
      }

    SAMRAI::tbox::SAMRAI_MPI::setCallAbortInSerialInsteadOfExit(true);
    SAMRAI::tbox::SAMRAI_MPI::setCallAbortInParallelInsteadOfMPIAbort(true);

    boost::shared_ptr<SAMRAI::tbox::InputDatabase>
      input_db(SAMRAI::tbox::InputManager::getManager()->parseInputFile
               (input_filename));
    if(input_db->isDatabase("TimerManager"))
      SAMRAI::tbox::TimerManager::createManager(input_db->getDatabase
                                                ("TimerManager"));
    boost::shared_ptr<SAMRAI::tbox::Database>
      main_db(input_db->getDatabase("Main"));
    const SAMRAI::tbox::Dimension
      dim(static_cast<unsigned short>(main_db->getInteger("dim")));

    std::string base_name(main_db->getString("base_name"));

    const std::string log_file_name = base_name + "/log";
    bool log_all_nodes(main_db->getBoolWithDefault("log_all_nodes",false));
    boost::filesystem::create_directories(base_name);
    if (log_all_nodes)
      { SAMRAI::tbox::PIO::logAllNodes(log_file_name); }
    else
      { SAMRAI::tbox::PIO::logOnlyNodeZero(log_file_name); }

    boost::shared_ptr<SAMRAI::geom::CartesianGridGeometry>
      grid_geometry(new SAMRAI::geom::CartesianGridGeometry
                    (dim, "CartesianGridGeometry",
                     input_db->getDatabase("CartesianGridGeometry")));

    boost::shared_ptr<SAMRAI::hier::PatchHierarchy>
      patch_hierarchy(new SAMRAI::hier::PatchHierarchy
                      ("PatchHierarchy", grid_geometry,
                       input_db->getDatabase("PatchHierarchy")));

    if(input_db->isDatabase("Stokes"))
      {
        Stokes::Adaptive_Solver stokes(dim, *input_db->getDatabase("Stokes"));
        converged=solve_system(stokes,main_db,input_db,patch_hierarchy,
                               base_name,dim);
      }
    else
      {
        Elastic::Adaptive_Solver elastic(dim, *input_db->getDatabase("Elastic"));
        grid_geometry->addCoarsenOperator
          (typeid(SAMRAI::pdat::CellVariable<double>).name(),
           boost::make_shared<SAMRAI::geom::CartesianCellDoubleWeightedAverage>
           ());

        converged=solve_system(elastic,main_db,input_db,patch_hierarchy,
                               base_name,dim);
      }
  }
  if(converged)
    SAMRAI::tbox::pout << "PASSED\n";
  else
    SAMRAI::tbox::pout << "FAILED\n";


  SAMRAI::tbox::SAMRAIManager::shutdown();
  SAMRAI::tbox::SAMRAIManager::finalize();
  SAMRAI::tbox::SAMRAI_MPI::finalize();

  return (converged ? 0 : 1);
}
