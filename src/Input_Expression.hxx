#pragma once

/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <string>

#include <muParser.h>
#include <okada.hxx>

#include "Patch.hxx"

class Input_Expression
{
public:
  mu::Parser equation;
  std::vector<Patch> patches;

  int dim, slice;
  mutable double coord[3];
  bool use_equation, is_valid;

  Input_Expression(): is_valid(false) {}

  Input_Expression(const std::string &name,
                   SAMRAI::tbox::Database &database,
                   const SAMRAI::tbox::Dimension& dimension,
                   const bool &error_if_missing,
                   const int num_components=1,
                   const int Slice=-1):
    dim(dimension.getValue()), slice(Slice)
  {
    Init(name,database,num_components,error_if_missing);
  }

  Input_Expression(const std::string &name,
                   SAMRAI::tbox::Database &database,
                   const SAMRAI::tbox::Dimension& dimension,
                   const int num_components=1,
                   const int Slice=-1):
    dim(dimension.getValue()), slice(Slice)
  {
    Init(name,database,num_components,false);
  }

  /// Make this private so that I do not accidentally use it and not
  /// set dim or slice.
private:
  void Init(const std::string &name,
            SAMRAI::tbox::Database &database,
            const int num_components,
            const bool &error_if_missing);
public:
  Input_Expression(const Input_Expression &e): dim(e.dim)
  {
    *this=e;
  }

  Input_Expression& operator=(const Input_Expression &e);
  double eval(const double Coord[3]) const;
};
