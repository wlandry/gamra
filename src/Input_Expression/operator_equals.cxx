#include "../Input_Expression.hxx"

Input_Expression& Input_Expression::operator=(const Input_Expression &e)
{
  is_valid=e.is_valid;
  if(is_valid)
    {
      equation=e.equation;
      patches=e.patches;
      use_equation=e.use_equation;
      dim=e.dim;
      slice=e.slice;

      if(use_equation)
        {
          equation.DefineVar("x",&coord[0]);
          equation.DefineVar("y",&coord[1]);
          equation.DefineVar("z",&coord[2]);
        }
    }
  return *this;
}
