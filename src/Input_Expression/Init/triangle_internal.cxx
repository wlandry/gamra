#include <okada.hxx>
#include <vector>

okada::Displacement triangle_internal(const std::vector<double> &args)
{
  const FTensor::Tensor1<double,3> origin(args[0],args[1],args[2]),
    p1(args[3],args[4],args[5]), p2(args[6],args[7],args[8]),
    jump(args[9],args[10],args[11]), 
    coord(args[14],args[15],args[16]);
  FTensor::Tensor1<double,3> u, v;
  const double lambda(args[12]), mu(args[13]);
  FTensor::Index<'i',3> i;
  u(i)=p1(i)-origin(i);
  v(i)=p2(i)-origin(i);
  return okada::Triangle_Fault(origin,u,v,jump,lambda,mu).displacement(coord);
}

