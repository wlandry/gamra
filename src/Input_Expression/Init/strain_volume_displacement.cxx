#include "strain_volume_internal.hxx"
#include <SAMRAI/tbox/Utilities.h>

double strain_volume_displacement(const double* arg_array, const int n)
{
  std::vector<double> args(arg_array,arg_array+n);
  const size_t num_args (19);
  if(args.size()!=num_args)
    { TBOX_ERROR("Wrong number of arguments for strain_volume.  Expected "
                 << num_args << ", got "
                 << args.size() << "\n"); }
      
  int index(static_cast<int>(args[num_args-1]));
  if(index<0 || index>2)
    { TBOX_ERROR("Bad index for strain_volume.  Expected 0, 1, or 2, got "
                 << args[14] << "\n"); }

  return strain_volume_internal(args).first(index);
}

