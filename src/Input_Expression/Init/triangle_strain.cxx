#include "triangle_internal.hxx"
#include <SAMRAI/tbox/Utilities.h>

double triangle_strain(const double* arg_array, const int n)
{
  std::vector<double> args(arg_array,arg_array+n);
  if(args.size()!=19)
    { TBOX_ERROR("Wrong number of arguments for triangle_strain.  Expected 19, got "
                 << args.size() << "\n"); }
      
  int index0(static_cast<int>(args[17])), index1(static_cast<int>(args[18]));
  if(index0<0 || index0>2 || index1<0 || index1>2)
    { TBOX_ERROR("Bad index for triangle_strain.  Expected 0, 1, or 2, got "
                 << args[14] << " and " << args[15] << "\n"); }

  return triangle_internal(args).second(index0,index1);
}

