#pragma once

#include <okada.hxx>
#include <vector>

okada::Displacement triangle_internal(const std::vector<double> &args);
