#include "okada_internal.hxx"
#include <SAMRAI/tbox/Utilities.h>

double okada_strain(const double* arg_array, const int n)
{
  std::vector<double> args(arg_array,arg_array+n);
  if(args.size()!=16)
    { TBOX_ERROR("Wrong number of arguments for okada_strain.  "
                 "Expected 16, got "
                 << args.size() << "\n"); }
      
  int index0(static_cast<int>(args[14])), index1(static_cast<int>(args[15]));
  if(index0<0 || index0>2 || index1<0 || index1>2)
    { TBOX_ERROR("Bad index for okada_strain.  Expected 0, 1, or 2, got "
                 << args[14] << " and " << args[15] << "\n"); }

  return okada_internal(args).second(index0,index1);
}
