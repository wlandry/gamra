#include "strain_volume_internal.hxx"
#include <SAMRAI/tbox/Utilities.h>

double strain_volume_strain(const double* arg_array, const int n)
{
  std::vector<double> args(arg_array,arg_array+n);
  const size_t num_args (20);
  if(args.size()!=num_args)
    { TBOX_ERROR("Wrong number of arguments for strain_volume_strain.  Expected "
                 << num_args << ", got "
                 << args.size() << "\n"); }
      
  int index0(static_cast<int>(args[num_args-2])),
    index1(static_cast<int>(args[num_args-1]));
  if(index0<0 || index0>2 || index1<0 || index1>2)
    { TBOX_ERROR("Bad index for strain_volume_strain.  Expected 0, 1, or 2, got "
                 << args[14] << " and " << args[15] << "\n"); }

  return strain_volume_internal(args).second(index0,index1);
}

