#include <okada.hxx>
#include <vector>

okada::Displacement okada_internal(const std::vector<double> &args)
{
  FTensor::Tensor1<double,3> origin(args[3],args[4],args[5]),
    coord(args[11],args[12],args[13]);
  /// Opening=0
  return okada::Okada(args[0],args[1],args[2],0.0,args[7],args[6],args[8],
                      args[9],args[10],origin).displacement(coord);
}
