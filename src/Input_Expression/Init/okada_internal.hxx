#pragma once

#include <okada.hxx>
#include <vector>

okada::Displacement okada_internal(const std::vector<double> &args);
