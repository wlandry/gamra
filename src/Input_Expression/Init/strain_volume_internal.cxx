#include <okada.hxx>
#include <vector>

okada::Displacement strain_volume_internal(const std::vector<double> &args)
{
  const FTensor::Tensor1<double,3> origin(args[0],args[1],args[2]),
    shape(args[3],args[4],args[5]),
    coord(args[15],args[16],args[17]);
  const FTensor::Tensor2_symmetric<double,3>
    strain(args[7],args[8],args[9],args[10],args[11],args[12]);
  const double strike(args[6]), lambda(args[13]), mu(args[14]);
  return okada::Strain_Volume(origin,shape,strike,strain,lambda,mu)
    .displacement(coord);
}
