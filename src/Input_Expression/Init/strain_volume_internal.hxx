#pragma once

#include <okada.hxx>
#include <vector>

okada::Displacement strain_volume_internal(const std::vector<double> &args);
