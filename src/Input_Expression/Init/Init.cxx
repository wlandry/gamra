#include "../../Input_Expression.hxx"

double* variable_factory(const char *, void *);
double okada_displacement(const double* arg_array, const int n);
double okada_strain(const double* arg_array, const int n);
double triangle_displacement(const double* arg_array, const int n);
double triangle_strain(const double* arg_array, const int n);
double strain_volume_displacement(const double* arg_array, const int n);
double strain_volume_strain(const double* arg_array, const int n);

void Input_Expression::Init(const std::string &name,
                            SAMRAI::tbox::Database &database,
                            const int num_components,
                            const bool &error_if_missing)
{
  is_valid=true;
  if(database.keyExists(name))
    {
      equation.DefineVar("x",&coord[0]);
      equation.DefineVar("y",&coord[1]);
      equation.DefineVar("z",&coord[2]);
      equation.DefineFun("okada",okada_displacement);
      equation.DefineFun("okada_strain",okada_strain);
      equation.DefineFun("triangle",triangle_displacement);
      equation.DefineFun("triangle_strain",triangle_strain);
      equation.DefineFun("strain_volume",strain_volume_displacement);
      equation.DefineFun("strain_volume_strain",strain_volume_strain);
      equation.SetVarFactory(variable_factory, NULL);
      equation.SetExpr(database.getString(name));
      use_equation=true;
    }
  else if(database.keyExists(name+"_data"))
    {
      patches.push_back(Patch(database,name,num_components,dim,slice));
      use_equation=false;
    }
  else if(database.keyExists(name+"_patches"))
    {
      if(!database.isDatabase(name+"_patches"))
        { TBOX_ERROR("The entry for " + name + "_patches must be a struct."); }

      boost::shared_ptr<SAMRAI::tbox::Database>
        patches_database(database.getDatabase(name+"_patches"));

      /// There is no way to get an iterator.  You have to get all
      /// of the keys and look up each one.
      std::vector<std::string>
        patch_names(database.getDatabase(name+"_patches")->getAllKeys());
      for(std::vector<std::string>::iterator patch=patch_names.begin();
          patch!=patch_names.end(); ++patch)
        {
          if(!patches_database->isDatabase(*patch))
            { TBOX_ERROR("The entry for " + *patch + " in " + name
                         + "_patches must be a struct."); }
          patches.push_back(Patch(*patches_database->getDatabase(*patch),
                                  num_components,dim,slice));
        }
      use_equation=false;
    }
  else
    {
      is_valid=false;
      if(error_if_missing)
        { TBOX_ERROR("Could not find an entry for " + name); }
    }
}
