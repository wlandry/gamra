#include "okada_internal.hxx"
#include <SAMRAI/tbox/Utilities.h>

double okada_displacement(const double* arg_array, const int n)
{
  std::vector<double> args(arg_array,arg_array+n);
  if(args.size()!=15)
    { TBOX_ERROR("Wrong number of arguments for okada.  Expected 15, got "
                 << args.size() << "\n"); }
      
  int index(static_cast<int>(args[14]));
  if(index<0 || index>2)
    { TBOX_ERROR("Bad index for okada.  Expected 0, 1, or 2, got "
                 << args[14] << "\n"); }


  /// If trying to get displacement above the surface, then compute
  /// the derivative and use that to extend the solution.  This
  /// works since we only need the displacement above the surface in
  /// order to compute a numerical derivative at the surface.
  double z(args[13]), result;
  if(z<0)
    {
      std::vector<double> diff_args(args);
      args[13]=-z;
      diff_args[13]=0;
      result=okada_internal(args).first(index)
        + 2*z*okada_internal(diff_args).second(index,2);
    }
  else
    {
      result=okada_internal(args).first(index);
    }
  return result;
}
