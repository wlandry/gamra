#include "triangle_internal.hxx"
#include <SAMRAI/tbox/Utilities.h>

double triangle_displacement(const double* arg_array, const int n)
{
  std::vector<double> args(arg_array,arg_array+n);
  if(args.size()!=18)
    { TBOX_ERROR("Wrong number of arguments for triangle.  Expected 18, got "
                 << args.size() << "\n"); }
      
  int index(static_cast<int>(args[17]));
  if(index<0 || index>2)
    { TBOX_ERROR("Bad index for triangle.  Expected 0, 1, or 2, got "
                 << args[14] << "\n"); }

  return triangle_internal(args).first(index);
}

