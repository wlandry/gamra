#include "../Input_Expression.hxx"

double Input_Expression::eval(const double Coord[3]) const
{
  if(!is_valid)
    { TBOX_ERROR("INTERNAL ERROR: Tried to use an invalid Input_Expression"); }

  double result;
  if(use_equation)
    {
      for(int d=0; d<dim; ++d)
        { coord[d]=Coord[d]; }
      result=equation.Eval();
    }
  else
    {
      /// If there is only a single patch, then allow extrapolation
      /// from the edge of the patch.
      if (patches.size()==1)
        {
          result=patches[0].eval(Coord);
        }
      else
        {
          std::vector<Patch>::const_iterator patch=patches.begin();
          for (; patch!=patches.end(); ++patch)
            {
              if (patch->contains(Coord))
                {
                  result=patch->eval(Coord);
                  break;
                }
            }
          if (patch==patches.end())
            {
              std::stringstream ss;
              ss << "None of the patches contain the point ("
                 << Coord[0];
              for (int d=1; d<dim; ++d)
                { ss << ", " << Coord[d]; }
              ss << ")";
              TBOX_ERROR(ss.str());
              /// Add an abort() to remove the compiler warning
              /// about using uninitialized variables.
              abort();
            }
        }
    }
  return result;
}
