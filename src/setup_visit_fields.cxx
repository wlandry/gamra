/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "Visit_Writer.hxx"
#include "Elastic/Adaptive_Solver.hxx"
#include "Stokes/Adaptive_Solver.hxx"

void setup_visit_fields(const std::vector<std::string> &output_fields,
                        const Elastic::Adaptive_Solver &elastic,
                        SAMRAI::appu::VisItDataWriter &samrai_visit_writer,
                        Visit_Writer &writer)
{
  samrai_visit_writer.setDefaultDerivedDataWriter(&writer);
  for(std::vector<std::string>::const_iterator output=output_fields.begin();
      output!=output_fields.end(); ++output)
    {
      if(*output=="Displacement"
         || *output=="Fault Correction + RHS"
         || *output=="Initial Displacement" || *output=="Metric")
        { samrai_visit_writer.registerDerivedPlotQuantity(*output,"VECTOR"); }
      else if(*output=="Lambda")
        { samrai_visit_writer.registerPlotQuantity("Lambda","SCALAR",
                                                   elastic.cell_moduli_id,0); }
      else if(*output=="Mu")
        { samrai_visit_writer.registerPlotQuantity("Mu","SCALAR",
                                                   elastic.cell_moduli_id,1); }
      else if(*output=="Induced Strain" || *output=="Applied Strain"
              || *output=="Induced Stress")
        { samrai_visit_writer.registerDerivedPlotQuantity(*output,"TENSOR"); }
      else if(*output=="Christof")
        {
          samrai_visit_writer.registerDerivedPlotQuantity("Christof_0","VECTOR");
          samrai_visit_writer.registerDerivedPlotQuantity("Christof_1","VECTOR");
          if(writer.dimension.getValue()==3)
            { samrai_visit_writer.registerDerivedPlotQuantity("Christof_2",
                                                              "VECTOR"); }
        }
    }
  if(elastic.xyz_id!=invalid_id)
    { samrai_visit_writer.registerNodeCoordinates(elastic.xyz_id); }
}

void setup_visit_fields(const std::vector<std::string> &output_fields,
                        const Stokes::Adaptive_Solver &stokes,
                        SAMRAI::appu::VisItDataWriter &samrai_visit_writer,
                        Visit_Writer &writer)
{
  samrai_visit_writer.setDefaultDerivedDataWriter(&writer);
  for(std::vector<std::string>::const_iterator output=output_fields.begin();
      output!=output_fields.end(); ++output)
    {
      if(*output=="Velocity")
        { samrai_visit_writer.registerDerivedPlotQuantity(*output,"VECTOR"); }
      else if(*output=="Pressure")
        { samrai_visit_writer.registerPlotQuantity("Pressure","SCALAR",
                                                   stokes.p_id); }
      else if(*output=="Exact Pressure")
        { samrai_visit_writer.registerPlotQuantity("Exact Pressure","SCALAR",
                                                   stokes.p_exact_id); }
      else if(*output=="Stokes Source")
        { samrai_visit_writer.registerPlotQuantity("Stokes Source","SCALAR",
                                                   stokes.p_rhs_id); }
      else if(*output=="Viscosity")
        { samrai_visit_writer.registerPlotQuantity("Viscosity","SCALAR",
                                                   stokes.cell_viscosity_id); }
      else if(*output=="Strain" || *output=="Deviatoric Stress")
        { samrai_visit_writer.registerDerivedPlotQuantity(*output,"TENSOR"); }
    }
}
