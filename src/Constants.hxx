/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

/// Special values such as whether we are at a boundary, plus the
/// function index_map

#pragma once

#include <limits>
#include "Dir.hxx"

constexpr double boundary_value=1.1111e100;
constexpr double invalid_value=3.1415926*boundary_value;
constexpr int invalid_id=-1;
inline Gamra::Dir dstrain_index_map(const Gamra::Dir &ix,
                                    const Gamra::Dir &iy,
                                    const int &dim)
{
  if(dim==2)
    return 0;

  const Gamra::Dir max=std::numeric_limits<Gamra::Dir>::max();
  const Gamra::Dir
    index[3][3]={{max,0,1},
                 {1,max,0},
                 {0,1,max}};
  return index[ix][iy];
}

inline Gamra::Dir dv_index_map(const Gamra::Dir &ix,
                               const Gamra::Dir &iy,
                               const int &dim)
{
  return Gamra::Dir::from_int(dstrain_index_map(ix,iy,dim)*2);
}
