#pragma once

#include <FTensor.hpp>

namespace Elastic
{
class Affine_Parameters_2D
{
public:
  Affine_Parameters_2D (const FTensor::Tensor1<double,3> &xyz,
                        const FTensor::Tensor1<double,3> &dx,
                        const FTensor::Tensor1<double,3> &fault_segment,
                        const FTensor::Tensor1<double,3> &scaled_normal)
  {
    init(xyz,dx,fault_segment,scaled_normal);
  }
  Affine_Parameters_2D (const FTensor::Tensor1<double,3> &xyz,
                        const FTensor::Tensor1<double,3> &dx,
                        const FTensor::Tensor1<double,3> &fault_segment,
                        const FTensor::Tensor1<double,3> &scaled_normal,
                        const FTensor::Tensor1<double,3> &offset)
  {
    FTensor::Index<'a',2> a;
    FTensor::Tensor1<double,3> xyz_offset;
    xyz_offset(a)=xyz(a)-offset(a);
    init(xyz_offset,dx,fault_segment,scaled_normal);
  }
  void init (const FTensor::Tensor1<double,3> &xyz,
             const FTensor::Tensor1<double,3> &dx,
             const FTensor::Tensor1<double,3> &fault_segment,
             const FTensor::Tensor1<double,3> &scaled_normal);
  double r;
  int sign;
  bool intersects, line_intersects;
  bool is_parallel () const
  {
    return sign==0;
  }
};
}
