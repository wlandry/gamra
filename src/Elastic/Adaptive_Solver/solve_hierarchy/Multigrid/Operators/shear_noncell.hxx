#pragma once

/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

/// The mixed derivative of the stress.  Written as if it is dtau_xy_dy.

#include <SAMRAI/pdat/SideData.h>
#include <SAMRAI/pdat/EdgeData.h>

namespace Elastic
{
  inline double shear_noncell(const SAMRAI::pdat::SideData<double> &v,
                              const SAMRAI::pdat::EdgeData<double> &edge_moduli,
                              const SAMRAI::pdat::SideIndex &x,
                              const SAMRAI::pdat::SideIndex &y,
                              const SAMRAI::pdat::EdgeIndex &edge,
                              const SAMRAI::hier::Index &ip,
                              const SAMRAI::hier::Index &jp,
                              const double &dx,
                              const double &dy)
  {
    return 
      edge_moduli(edge+jp,1)*(v(x+jp)-v(x   ))/(dy*dy)
      -edge_moduli(edge   ,1)*(v(x   )-v(x-jp))/(dy*dy)
      +edge_moduli(edge+jp,1)*(v(y+jp)-v(y+jp-ip))/(dx*dy) 
      -edge_moduli(edge   ,1)*(v(y   )-v(y-ip   ))/(dx*dy);
  }
}
