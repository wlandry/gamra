/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../../Operators.hxx"
namespace Elastic
{
  void set_physical_boundaries
  (const Boundary_Conditions &boundary_conditions, const int &v_id,
   const SAMRAI::hier::PatchLevel &patch_level, const bool &is_homogeneous);
}

void Elastic::Operators::ghostfill(const int &v_id, const int &dest_level,
                                   const bool &fill_from_coarse,
                                   const SAMRAI::hier::PatchLevel &patch_level,
                                   const bool &is_homogeneous)
{
  ghostfill_nocoarse(v_id, dest_level);
  if (fill_from_coarse)
    {
      if (!ghostfill_schedules[dest_level])
        TBOX_ERROR("Expected schedule not found.");
      SAMRAI::xfer::RefineAlgorithm refiner;

      refiner.registerRefine(v_id,v_id,v_id,ghostfill_operator);
      if(have_faults())
        {
          refiner.registerRefine
            (dv_diagonal_id,dv_diagonal_id,dv_diagonal_id,
             boost::shared_ptr<SAMRAI::hier::RefineOperator>());
          refiner.registerRefine
            (dv_mixed_id,dv_mixed_id,dv_mixed_id,
             boost::shared_ptr<SAMRAI::hier::RefineOperator>());
        }
      refiner.resetSchedule(ghostfill_schedules[dest_level]);
      ghostfill_schedules[dest_level]->fillData(0.0,false);
    }
  set_physical_boundaries(boundary_conditions,v_id,patch_level,is_homogeneous);
}

