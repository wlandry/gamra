/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../../Operators.hxx"

namespace Elastic
{
void set_physical_boundaries
(const Boundary_Conditions &boundary_conditions, const int &v_id,
 const SAMRAI::hier::PatchLevel &patch_level, const bool &is_homogeneous)
{
  for (SAMRAI::hier::PatchLevel::Iterator p(patch_level.begin());
       p!=patch_level.end(); ++p)
    { boundary_conditions.set_physical_boundary(**p,v_id,is_homogeneous); }
}
}
