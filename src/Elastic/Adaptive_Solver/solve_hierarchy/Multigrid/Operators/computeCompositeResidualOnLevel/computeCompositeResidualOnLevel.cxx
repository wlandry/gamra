/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University
/// Copyright © 2013-2016 Nanyang Technical University

#include "../../Operators.hxx"
#include "../Coarse_Fine_Boundary_Refine.hxx"

namespace Elastic
{
  void residual_2D
  (const SAMRAI::pdat::SideData<double> &v,
   const SAMRAI::pdat::CellData<double> &cell_moduli,
   const SAMRAI::pdat::EdgeData<double> &edge_moduli,
   const SAMRAI::pdat::SideData<double> &v_rhs,
   const SAMRAI::hier::Box &pbox,
   const double dxy[],
   SAMRAI::pdat::SideData<double> &v_resid);

  void residual_3D
  (const SAMRAI::pdat::SideData<double> &v,
   const SAMRAI::pdat::CellData<double> &cell_moduli,
   const SAMRAI::pdat::EdgeData<double> &edge_moduli,
   const SAMRAI::pdat::SideData<double> &v_rhs,
   const SAMRAI::hier::Box &pbox,
   const double dxyz[],
   SAMRAI::pdat::SideData<double> &v_resid);
}

void Elastic::Operators::computeCompositeResidualOnLevel
(SAMRAI::solv::SAMRAIVectorReal<double>& residual,
 const SAMRAI::solv::SAMRAIVectorReal<double>& solution,
 const SAMRAI::solv::SAMRAIVectorReal<double>& rhs,
 int level,
 bool error_equation_indicator)
{
  t_compute_composite_residual->start();

  if (residual.getPatchHierarchy() != solution.getPatchHierarchy()
      || rhs.getPatchHierarchy() != solution.getPatchHierarchy())
    { TBOX_ERROR(__FILE__ << ": residual, solution, and rhs hierarchies "
                 << "are not consistent."); }
  const SAMRAI::hier::PatchHierarchy &hierarchy=*residual.getPatchHierarchy();

  const SAMRAI::hier::PatchLevel &patch_level = *hierarchy.getPatchLevel(level);

  const int v_id = solution.getComponentDescriptorIndex(0);
  v_refine_patch_strategy.data_id=v_id;
  v_refine_patch_strategy.is_residual=error_equation_indicator;
  Coarse_Fine_Boundary_Refine::is_residual=error_equation_indicator;

  ghostfill(v_id, level, level > level_min,patch_level,error_equation_indicator);
  for (SAMRAI::hier::PatchLevel::Iterator p(patch_level.begin());
       p!=patch_level.end(); ++p)
    {
      SAMRAI::hier::Patch &patch = **p;
      boost::shared_ptr<SAMRAI::pdat::SideData<double> > v_ptr =
        boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
        (patch.getPatchData(v_id));
      boost::shared_ptr<SAMRAI::pdat::CellData<double> > cell_moduli_ptr =
        boost::dynamic_pointer_cast<SAMRAI::pdat::CellData<double> >
        (patch.getPatchData(cell_moduli_id));
      boost::shared_ptr<SAMRAI::pdat::SideData<double> > v_rhs_ptr =
        boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
        (rhs.getComponentPatchData(0,patch));
      boost::shared_ptr<SAMRAI::pdat::SideData<double> > v_resid_ptr =
        boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
        (residual.getComponentPatchData(0,patch));

      SAMRAI::hier::Box pbox=patch.getBox();
      pbox.growUpper(SAMRAI::hier::IntVector::getOne(dimension));
      boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> geom =
        boost::dynamic_pointer_cast<SAMRAI::geom::CartesianPatchGeometry>
        (patch.getPatchGeometry());

      switch(dimension.getValue())
        {
        case 2:
          {
            boost::shared_ptr<SAMRAI::pdat::EdgeData<double> > edge_moduli_ptr =
              boost::dynamic_pointer_cast<SAMRAI::pdat::EdgeData<double> >
              (patch.getPatchData(edge_moduli_id));

            switch(geometry)
              {
              case Geometry::CARTESIAN:
                residual_2D(*v_ptr,*cell_moduli_ptr,*edge_moduli_ptr,*v_rhs_ptr,
                            pbox,geom->getDx(),*v_resid_ptr);
                break;
              case Geometry::SPHERICAL:
                {
                  boost::shared_ptr<SAMRAI::pdat::SideData<double> > gg_ptr =
                    boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
                    (patch.getPatchData(gg_id));
                  boost::shared_ptr<SAMRAI::pdat::CellData<double> >
                    christof_cell_ptr =
                    boost::dynamic_pointer_cast<SAMRAI::pdat::CellData<double> >
                    (patch.getPatchData(christof_cell_id));
                  boost::shared_ptr<SAMRAI::pdat::EdgeData<double> >
                    christof_edge_ptr =
                    boost::dynamic_pointer_cast<SAMRAI::pdat::EdgeData<double> >
                    (patch.getPatchData(christof_edge_id));
                  
                  // residual_2D_metric(*v_ptr,*cell_moduli_ptr,*edge_moduli_ptr,
                  //                    *gg_ptr, *christof_cell_ptr,
                  //                    *christof_edge_ptr,
                  //                    *v_rhs_ptr, *v_resid_ptr,pbox,
                  //                    geom->getDx());
                }
                break;
              case Geometry::TOPOGRAPHY:
                break;
              }
          }
          break;
        case 3:
          {
            boost::shared_ptr<SAMRAI::pdat::EdgeData<double> > edge_moduli_ptr =
              boost::dynamic_pointer_cast<SAMRAI::pdat::EdgeData<double> >
              (patch.getPatchData(edge_moduli_id));
            residual_3D(*v_ptr,*cell_moduli_ptr,*edge_moduli_ptr,*v_rhs_ptr,
                        pbox,geom->getDx(),*v_resid_ptr);
          }
          break;
        default:
          abort();
        }
    }

  t_compute_composite_residual->stop();
}

