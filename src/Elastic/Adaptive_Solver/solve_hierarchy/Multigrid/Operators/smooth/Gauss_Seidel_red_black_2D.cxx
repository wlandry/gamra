/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../v_operator_2D.hxx"

#include "../../Operators.hxx"
#include "../Coarse_Fine_Boundary_Refine.hxx"
#include "../dRm_dv.hxx"

void Elastic::Operators::Gauss_Seidel_red_black_2D
(SAMRAI::solv::SAMRAIVectorReal<double>& solution,
 const SAMRAI::solv::SAMRAIVectorReal<double>& residual,
 int level,
 int num_sweeps,
 double residual_tolerance)
{
  const int v_id(solution.getComponentDescriptorIndex(0)),
    v_rhs_id(residual.getComponentDescriptorIndex(0));

  const SAMRAI::hier::PatchHierarchy &hierarchy = *residual.getPatchHierarchy();
  const SAMRAI::hier::PatchLevel &patch_level = *hierarchy.getPatchLevel(level);

  v_refine_patch_strategy.data_id=v_id;
  v_refine_patch_strategy.is_residual=true;
  Coarse_Fine_Boundary_Refine::is_residual=true;
  double theta_momentum=1.0;

  const SAMRAI::hier::Index
    unit[]={SAMRAI::hier::Index(1,0),SAMRAI::hier::Index(0,1)};
  bool converged = false;
  for (int sweep=0; sweep < num_sweeps && !converged; ++sweep)
    {
      double max_residual(0);

      const Gamra::Dir dim(2);
      for(Gamra::Dir ix=0; ix<dim; ++ix)
        {
          const Gamra::Dir iy(ix.next(dim));
          const SAMRAI::hier::Index ip(unit[ix]), jp(unit[iy]);
          for(int rb=0;rb<2;++rb)
            {
              ghostfill(v_id, level, level > level_min,patch_level,true);
              for (SAMRAI::hier::PatchLevel::Iterator p(patch_level.begin());
                   p!=patch_level.end(); ++p)
                {
                  SAMRAI::hier::Patch &patch = **p;

                  boost::shared_ptr<SAMRAI::pdat::SideData<double> > v_ptr =
                    boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
                    (patch.getPatchData(v_id));
                  SAMRAI::pdat::SideData<double> &v(*v_ptr);
                  boost::shared_ptr<SAMRAI::pdat::SideData<double> > v_rhs_ptr =
                    boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
                    (patch.getPatchData(v_rhs_id));
                  SAMRAI::pdat::SideData<double> &v_rhs(*v_rhs_ptr);
                
                  boost::shared_ptr<SAMRAI::pdat::CellData<double> >
                    cell_moduli_ptr=
                    boost::dynamic_pointer_cast<SAMRAI::pdat::CellData<double> >
                    (patch.getPatchData(cell_moduli_id));
                  SAMRAI::pdat::CellData<double> &cell_moduli(*cell_moduli_ptr);
                  boost::shared_ptr<SAMRAI::pdat::EdgeData<double> >
                    edge_moduli_ptr=
                    boost::dynamic_pointer_cast<SAMRAI::pdat::EdgeData<double> >
                    (patch.getPatchData(edge_moduli_id));
                  SAMRAI::pdat::EdgeData<double> &edge_moduli(*edge_moduli_ptr);

                  SAMRAI::hier::Box pbox=patch.getBox();
                  boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> geom =
                    boost::dynamic_pointer_cast
                    <SAMRAI::geom::CartesianPatchGeometry>
                    (patch.getPatchGeometry());
                  double dx = geom->getDx()[ix];
                  double dy = geom->getDx()[iy];

                  for(int j=pbox.lower(1); j<=pbox.upper(1)+unit[ix][1]; ++j)
                    {
                      /// Do the red-black skip
                      int i_min=pbox.lower(0)
                        + (abs(pbox.lower(0) + j + rb))%2;
                      for(int i=i_min; i<=pbox.upper(0)+unit[ix][0]; i+=2)
                        {
                          SAMRAI::pdat::CellIndex
                            center(SAMRAI::hier::Index(i,j));
                          update_V_2D(ix,pbox,center,unit[ix],unit[iy],
                                      v,v_rhs,max_residual,dx,dy,cell_moduli,
                                      edge_moduli,theta_momentum);
                        }
                    }
                }
            }
        }

      if (residual_tolerance >= 0.0)
        {
          converged = max_residual < residual_tolerance;
          const SAMRAI::tbox::SAMRAI_MPI& mpi(hierarchy.getMPI());
          int tmp= converged ? 1 : 0;
          if (mpi.getSize() > 1)
            { mpi.AllReduce(&tmp, 1, MPI_MIN); }
          converged=(tmp==1);
        }
    }
}

