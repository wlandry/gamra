/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <SAMRAI/xfer/CoarsenAlgorithm.h>

#include "../Operators.hxx"

void Elastic::Operators::coarsen_resid(const int &v_dst, const int &v_src,
                                       const int &dest_level)
{
  /// Need to do a sync because coarsening uses ghost zones.  Elements
  /// on the coarse-fine boundary get fixed up later, so we do not
  /// need to use the full ghostfill() function.
  ghostfill_nocoarse(v_src,dest_level+1);

  if (!coarsen_resid_schedules[dest_level])
    { TBOX_ERROR("Expected schedule not found."); }

  SAMRAI::xfer::CoarsenAlgorithm coarsener(dimension);
  coarsener.registerCoarsen(v_dst,v_src,coarsen_resid_operator);
  coarsener.resetSchedule(coarsen_resid_schedules[dest_level]);
  coarsen_resid_schedules[dest_level]->coarsenData();
}

