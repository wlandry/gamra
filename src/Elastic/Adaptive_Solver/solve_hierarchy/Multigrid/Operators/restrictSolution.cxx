/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../Operators.hxx"
#include "Coarse_Fine_Boundary_Refine.hxx"

void Elastic::Operators::restrictSolution
(const SAMRAI::solv::SAMRAIVectorReal<double>& s,
 SAMRAI::solv::SAMRAIVectorReal<double>& d,
 int dest_level)
{
  t_restrict_solution->start();

  int v_src(s.getComponentDescriptorIndex(0)),
    v_dst(d.getComponentDescriptorIndex(0));

  v_coarsen_patch_strategy.data_id=v_src;
  v_coarsen_patch_strategy.is_residual=false;
  coarsen_solution(v_dst,v_src,dest_level);

  v_refine_patch_strategy.is_residual=false;
  v_refine_patch_strategy.data_id=d.getComponentDescriptorIndex(0);
  Coarse_Fine_Boundary_Refine::is_residual=false;

  t_restrict_solution->stop();
}
