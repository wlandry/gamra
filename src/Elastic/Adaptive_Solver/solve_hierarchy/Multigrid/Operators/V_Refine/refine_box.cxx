/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <SAMRAI/pdat/CellIterator.h>

#include "../V_Refine.hxx"

void Elastic::V_Refine::refine_box
(SAMRAI::hier::Patch &fine_patch,
 const SAMRAI::hier::Patch &coarse_patch,
 const int dst_component,
 const int src_component,
 const SAMRAI::hier::Box &fine_box,
 const SAMRAI::hier::IntVector &,
 const Gamra::Dir &ix) const
{
   const SAMRAI::tbox::Dimension &dimension(fine_patch.getDim());
   const Gamra::Dir dim(dimension.getValue());

   boost::shared_ptr<SAMRAI::pdat::SideData<double> > v_ptr =
     boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
     (coarse_patch.getPatchData(src_component));
   SAMRAI::pdat::SideData<double> &v(*v_ptr);
   boost::shared_ptr<SAMRAI::pdat::SideData<double> > v_fine_ptr =
     boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
     (fine_patch.getPatchData(dst_component));
   SAMRAI::pdat::SideData<double> &v_fine(*v_fine_ptr);

   SAMRAI::hier::Index ip(SAMRAI::hier::Index::getZeroIndex(dimension)),
     jp(ip), kp(ip);
   ip[0]=1;
   jp[1]=1;
   if(dim>2)
     kp[2]=1;
   SAMRAI::hier::Index unit[]={ip,jp,kp};

   SAMRAI::pdat::CellIterator cend(SAMRAI::pdat::CellGeometry::end(fine_box));

   SAMRAI::hier::Box coarse_box=coarse_patch.getBox();
   boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> coarse_geom =
     boost::dynamic_pointer_cast<SAMRAI::geom::CartesianPatchGeometry>
     (coarse_patch.getPatchGeometry());

   for(SAMRAI::pdat::CellIterator
         ci(SAMRAI::pdat::CellGeometry::begin(fine_box)); ci!=cend; ++ci)
     {
       SAMRAI::pdat::SideIndex fine(*ci,ix,SAMRAI::pdat::SideIndex::Lower);

       SAMRAI::pdat::SideIndex coarse(fine);
       coarse.coarsen(SAMRAI::hier::Index::getOneIndex(dimension)*2);

       if(fine[ix]%2==0)
         {
           v_fine(fine)=refine_along_line(v,ix,dim,unit,fine,coarse,
                                          coarse_box,*coarse_geom);
         }
       else
         {
           v_fine(fine)=(refine_along_line(v,ix,dim,unit,fine,coarse,
                                           coarse_box,*coarse_geom)
                         + refine_along_line(v,ix,dim,unit,fine,
                                             coarse+unit[ix],
                                             coarse_box,*coarse_geom))/2;
         }
     }
}
