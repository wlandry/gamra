/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../V_Coarsen_Patch_Strategy.hxx"

namespace
{
  void
  coarsen_point_2D(const SAMRAI::pdat::SideIndex& coarse,
                   const SAMRAI::pdat::SideIndex& fine,
                   const SAMRAI::hier::Index& ip, const SAMRAI::hier::Index& jp,
                   SAMRAI::pdat::SideData<double>& v,
                   const SAMRAI::pdat::SideData<double>& v_fine )
  {
    v(coarse)=(v_fine(fine) + v_fine(fine+jp))/4
      + (v_fine(fine-ip) + v_fine(fine-ip+jp)
         + v_fine(fine+ip) + v_fine(fine+jp+ip))/8;
  }

  double
  coarsen_correction_2D(const SAMRAI::pdat::SideIndex& fine,
                        const int& axis,
                        const SAMRAI::hier::Index& ip,
                        const SAMRAI::hier::Index& jp,
                        const SAMRAI::pdat::CellData<double>& dv_diagonal,
                        const SAMRAI::pdat::SideData<double>& dv_mixed)
  {
    SAMRAI::pdat::CellIndex cell(fine);
    return (dv_diagonal(cell-ip,axis) - dv_diagonal(cell,axis)
            + dv_diagonal(cell-ip+jp,axis) - dv_diagonal(cell+jp,axis))/8
      + (dv_mixed(fine,0) + dv_mixed(fine+jp,1))/2;
  }
}

void Elastic::V_Coarsen_Patch_Strategy::coarsen_2D
(SAMRAI::pdat::SideData<double>& v,
 const SAMRAI::pdat::SideData<double>& v_fine,
 const boost::shared_ptr<SAMRAI::pdat::SideData<double> > &dv_mixed,
 const boost::shared_ptr<SAMRAI::pdat::CellData<double> > &dv_diagonal,
 SAMRAI::hier::Patch &coarse_patch,
 const SAMRAI::hier::Patch &fine_patch,
 const SAMRAI::hier::Box& coarse_box) const
{
  /// Numbering of v nodes is

  ///    x--x--x--x--x  Fine
  ///    0  1  2  3  4

  ///    x-----x-----x Coarse
  ///    0     1     2

  ///    So the i'th coarse point is affected by the i*2-1,
  ///    i*2, and i*2+1 fine points.  So, for example, i_fine=3
  ///    affects both i_coarse=1 and i_coarse=2.

  ///    |---------------------------------------------------------------|
  ///    |               |               |               |               |
  ///    f       f       f       f       f       f       f       f       f
  ///    |               |               |               |               |
  ///    c               c               c               c               c
  ///    |               |               |               |               |
  ///    f       f       f       f       f       f       f       f       f
  ///    |               |               |               |               |
  ///    |---------------------------------------------------------------|
  ///    |               |               |               |               |
  ///    f       f       f       f       f       f       f       f       f
  ///    |               |               |               |               |
  ///    c               c               c               c               c
  ///    |               |               |               |               |
  ///    f       f       f       f       f       f       f       f       f
  ///    |               |               |               |               |
  ///    |---------------------------------------------------------------|

  ///    In 2D, a coarse point depends on six points.  In this
  ///    case, (i*2,j*2), (i*2,j*2+1), (i*2-1,j*2),
  ///    (i*2-1,j*2+1), (i*2+1,j*2), (i*2+1,j*2+1).

  ///    The coarse/fine boundaries get fixed up later in
  ///    V_Coarsen_Patch_Strategy::postprocessCoarsen.

  SAMRAI::hier::Index ip(1,0), jp(0,1);
  const SAMRAI::hier::Index unit[]={ip,jp};

  /// From reading CoarsenSchedule::coarsenSourceData in
  /// SAMRAI/source/SAMRAI/xfer/CoarsenSchedule.C, it seems that the
  /// coarse box is created from the fine box.  So the coarse box is
  /// always covered by the fine box, meaning we do not have to do an
  /// intersection.

  SAMRAI::hier::Box big_box(coarse_box);
  big_box.growUpper(SAMRAI::hier::IntVector::getOne(fine_patch.getDim()));
  const SAMRAI::pdat::CellIterator end(SAMRAI::pdat::CellGeometry::end(big_box));
  const int dim(2);

  const boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> coarse_geom =
    boost::dynamic_pointer_cast<SAMRAI::geom::CartesianPatchGeometry>
    (coarse_patch.getPatchGeometry());

  for(SAMRAI::pdat::CellIterator
        ci(SAMRAI::pdat::CellGeometry::begin(big_box)); ci!=end; ++ci)
    {
      const SAMRAI::pdat::CellIndex &cell(*ci);
      for(Gamra::Dir ix=0;ix<2;++ix)
        {
          const Gamra::Dir iy(ix.next(dim));
          if(cell[iy]!=coarse_box.upper(iy)+1)
            {
              SAMRAI::pdat::SideIndex
                coarse(cell,ix,SAMRAI::pdat::SideIndex::Lower);
              SAMRAI::pdat::SideIndex fine(coarse*2);
              if((cell[ix]==coarse_box.lower(ix)
                  && coarse_geom->getTouchesRegularBoundary(ix,0))
                 || (cell[ix]==coarse_box.upper(ix)+1
                     && coarse_geom->getTouchesRegularBoundary(ix,1)))
                {
                  v(coarse)=
                    (v_fine(fine) + v_fine(fine+unit[iy]))/2;
                  if(have_faults() && !is_residual)
                    { v(coarse)+=((*dv_mixed)(fine,0)
                                  + (*dv_mixed)(fine+unit[iy],1))/2; }
                }
              else
                {
                  coarsen_point_2D(coarse,fine,unit[ix],unit[iy],
                                   v,v_fine);
                  if(have_faults() && !is_residual)
                    {
                      v(coarse)+=
                        coarsen_correction_2D(fine,ix,unit[ix],
                                              unit[iy],*dv_diagonal,
                                              *dv_mixed);
                    }
                }
            }
        }
    }
}
