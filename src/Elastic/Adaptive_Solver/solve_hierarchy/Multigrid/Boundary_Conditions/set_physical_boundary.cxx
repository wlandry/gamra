/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../Boundary_Conditions.hxx"

void Elastic::Boundary_Conditions::set_physical_boundary
(const SAMRAI::hier::Patch& patch, const int &v_id, const bool &is_homogeneous,
 const bool &apply_normal_stress) const
{
  try
    {
      set_regular_boundary(patch,v_id,is_homogeneous,apply_normal_stress);
    }
  catch(mu::Parser::exception_type &e)
    {
      TBOX_ERROR("Error in input formula\n"
                 << "Message:  " << e.GetMsg() << "\n"
                 << "Formula:  " << e.GetExpr() << "\n"
                 << "Token:    " << e.GetToken() << "\n"
                 << "Position: " << e.GetPos() << "\n"
                 << "Errc:     " << e.GetCode() << "\n");
    }
}
