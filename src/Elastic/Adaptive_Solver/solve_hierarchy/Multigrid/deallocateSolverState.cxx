/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../Multigrid.hxx"

void Elastic::Multigrid::deallocateSolverState()
{
  if (hierarchy)
    {
      preconditioner.deallocateSolverState();
      hierarchy.reset();
      level_min = -1;
      level_max = -1;
      destroyVectorWrappers();
    }
}
