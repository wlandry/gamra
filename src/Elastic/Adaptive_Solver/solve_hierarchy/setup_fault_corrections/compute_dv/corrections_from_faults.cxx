/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "compute_intersections_2D.hxx"
#include "compute_intersections_3D.hxx"
#include "Dir.hxx"
#include "Constants.hxx"
#include "Elastic/Adaptive_Solver/Strain.hxx"
#include "Elastic/Adaptive_Solver/Fault.hxx"

#include <SAMRAI/pdat/SideData.h>
#include <SAMRAI/pdat/CellData.h>
#include <vector>

namespace Elastic
{
  void corrections_from_faults
  (const std::vector<Fault> &faults,
   const Gamra::Dir &dim, const double *dx,
   const double *geom_xlower,
   const SAMRAI::hier::Box &gbox,
   const SAMRAI::hier::Index &pbox_lower,
   const std::vector<FTensor::Tensor1<double,3> > &Dx,
   SAMRAI::pdat::CellData<double> &dv_diagonal,
   SAMRAI::pdat::SideData<double> &dv_mixed)
  {
    for(std::vector<Fault>::const_iterator fault=faults.begin();
        fault!=faults.end(); ++fault)
      {
        for(Gamra::Dir ix=0;ix<dim;++ix)
          {
            double side_offset[]={0.5,0.5,0.5};
            side_offset[ix]=0;
            SAMRAI::pdat::SideIterator
              s_end(SAMRAI::pdat::SideGeometry::end(gbox,ix));
            for(SAMRAI::pdat::SideIterator
                  si(SAMRAI::pdat::SideGeometry::begin(gbox,ix));
                si!=s_end; ++si)
              {
                const SAMRAI::pdat::SideIndex &s(*si);

                FTensor::Tensor1<double,3> xyz_side_relative(0,0,0);
                for(int d=0;d<dim;++d)
                  {
                    xyz_side_relative(d)=
                      geom_xlower[d] + dx[d]*(s[d]-pbox_lower[d]+side_offset[d])
                      - fault->origin(d);
                  }
                if(dim==2)
                  {
                    double intersect, intersect_mixed[2];
                    compute_intersections_2D(xyz_side_relative,*fault,Dx,
                                             dim,ix,intersect,intersect_mixed);

                    if(gbox.contains(s))
                      {
                        SAMRAI::pdat::CellIndex c(s);
                        dv_diagonal(c,ix)+=intersect*fault->jump(ix);
                      }

                    dv_mixed(s,0)+=intersect_mixed[0]*fault->jump(ix);
                    dv_mixed(s,1)-=intersect_mixed[1]*fault->jump(ix);
                  }
                else
                  {
                    double intersect_diagonal, intersect_mixed[4],
                      intersect_corner[4];
                    compute_intersections_3D(xyz_side_relative,*fault,Dx,
                                             dim,ix,
                                             intersect_diagonal,intersect_mixed,
                                             intersect_corner);

                    if(gbox.contains(s))
                      {
                        SAMRAI::pdat::CellIndex c(s);
                        dv_diagonal(c,ix)+=intersect_diagonal*fault->jump(ix);
                      }

                    for(int n=0;n<4;++n)
                      {
                        dv_mixed(s,n)+=intersect_mixed[n]*fault->jump(ix);
                        dv_mixed(s,n+4)+=intersect_corner[n]*fault->jump(ix);
                      }
                  }
              }
          }
      }
  }
}
