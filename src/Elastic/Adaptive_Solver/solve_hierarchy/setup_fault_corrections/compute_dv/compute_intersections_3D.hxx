/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <FTensor.hpp>
#include <vector>

namespace Elastic
{
  template <class T>
  void compute_intersections_3D
  (const FTensor::Tensor1<double,3> &xyz,
   const T &strain,
   const std::vector<FTensor::Tensor1<double,3> > &dx,
   const int &dim,
   const int &ix,
   double &intersect_diagonal,
   double intersect_mixed[4],
   double intersect_corner[4])
  {
    intersect_diagonal = strain.intersect_3D(xyz,dx[ix]);

    FTensor::Tensor1<double,3> dx_2_y, dx_2_z;
    FTensor::Index<'a',3> a;
    int iy((ix+1)%dim), iz((ix+2)%dim);
    dx_2_y(a)=dx[iy](a)/2;
    intersect_mixed[0]=strain.intersect_3D(xyz,dx_2_y);

    dx_2_y(a)=-dx_2_y(a);
    intersect_mixed[1]=strain.intersect_3D(xyz,dx_2_y);

    dx_2_z(a)=dx[iz](a)/2;
    intersect_mixed[2]=strain.intersect_3D(xyz,dx_2_z);

    dx_2_z(a)=-dx_2_z(a);
    intersect_mixed[3]=strain.intersect_3D(xyz,dx_2_z);

    FTensor::Tensor1<double,3> dx_corner;
    dx_corner(ix)=0;
    dx_corner(iy)=dx[iy](iy)/2;
    dx_corner(iz)=dx[iz](iz)/2;
    intersect_corner[0]=strain.intersect_3D(xyz,dx_corner);
    dx_corner(iz)=-dx_corner(iz);
    intersect_corner[1]=strain.intersect_3D(xyz,dx_corner);

    dx_corner(iy)=-dx_corner(iy);
    intersect_corner[2]=strain.intersect_3D(xyz,dx_corner);
    dx_corner(iz)=-dx_corner(iz);
    intersect_corner[3]=strain.intersect_3D(xyz,dx_corner);
  }
}
