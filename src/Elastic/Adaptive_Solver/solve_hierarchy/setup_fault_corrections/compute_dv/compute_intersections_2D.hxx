/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <FTensor.hpp>
#include <vector>

namespace Elastic
{
  template <class T>
  void compute_intersections_2D
  (const FTensor::Tensor1<double,3> &xyz,
   const T &strain_or_fault,
   const std::vector<FTensor::Tensor1<double,3> > &dx,
   const int &dim,
   const int &ix,
   double &intersect_diagonal,
   double intersect_mixed[2])
  {
    intersect_diagonal=strain_or_fault.intersect_2D(xyz, dx[ix]);

    FTensor::Tensor1<double,3> dx_2;
    const int iy((ix+1)%dim);
    FTensor::Index<'a',2> a;
    dx_2(a)=dx[iy](a)/2;
    intersect_mixed[0] = strain_or_fault.intersect_2D(xyz, dx_2);

    dx_2(a)=-dx_2(a);
    intersect_mixed[1] = -strain_or_fault.intersect_2D(xyz, dx_2);
  }
}
