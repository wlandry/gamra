/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "compute_intersections_2D.hxx"
#include "compute_intersections_3D.hxx"
#include "Dir.hxx"
#include "Constants.hxx"
#include "Elastic/Adaptive_Solver/Strain.hxx"
#include "Elastic/Adaptive_Solver/Fault.hxx"

#include <SAMRAI/pdat/SideData.h>
#include <SAMRAI/pdat/CellData.h>
#include <SAMRAI/pdat/EdgeData.h>
#include <vector>

namespace Elastic
{
  void corrections_from_strains
  (const std::vector<Strain> &strains,
   const Gamra::Dir &dim, const double *dx,
   const double *geom_xlower,
   const SAMRAI::hier::Box &gbox,
   const SAMRAI::hier::Index &pbox_lower,
   const std::vector<FTensor::Tensor1<double,3> > &Dx,
   SAMRAI::pdat::CellData<double> &dv_diagonal,
   SAMRAI::pdat::SideData<double> &dv_mixed,
   SAMRAI::pdat::CellData<double> &dstrain_diagonal,
   SAMRAI::pdat::EdgeData<double> &dstrain_mixed)
  {
    for(std::vector<Strain>::const_iterator strain=strains.begin();
        strain!=strains.end(); ++strain)
      {
        for(Gamra::Dir ix=0;ix<dim;++ix)
          {
            const Gamra::Dir iy(ix.next(dim));
            const Gamra::Dir iz(iy.next(dim));
            const FTensor::Index<'a',3> a;
            const double jump(Dx[ix](a)*strain->strain(ix,a)),
              jump_mixed_y(Dx[iy](a)*strain->strain(ix,a)/2),
              jump_mixed_z(Dx[iz](a)*strain->strain(ix,a)/2),
              jump_corner_y_p_z((Dx[iy](a)+Dx[iz](a))*strain->strain(ix,a)/2),
              jump_corner_y_m_z((Dx[iy](a)-Dx[iz](a))*strain->strain(ix,a)/2);
            
            double side_offset[]={0.5,0.5,0.5};
            side_offset[ix]=0;
            double edge_offset_y[]={0,0,0};
            double edge_offset_z[]={0,0,0};
            if(dim!=2)
              {
                edge_offset_y[iy]=0.5;
                edge_offset_z[iz]=0.5;
              }
            const Gamra::Dir ix_iy(dstrain_index_map(ix,iy,dim));
            const Gamra::Dir ix_iz(dstrain_index_map(ix,iz,dim));
            
            SAMRAI::pdat::SideIterator
              s_end(SAMRAI::pdat::SideGeometry::end(gbox,ix));
            for(SAMRAI::pdat::SideIterator
                  si(SAMRAI::pdat::SideGeometry::begin(gbox,ix));
                si!=s_end; ++si)
              {
                const SAMRAI::pdat::SideIndex &s(*si);

                FTensor::Tensor1<double,3> xyz_side_relative(0,0,0),
                  xyz_center(0,0,0), xyz_edge_y(0,0,0), xyz_edge_z(0,0,0);
                for(int d=0;d<dim;++d)
                  {
                    xyz_side_relative(d)=
                      geom_xlower[d] + dx[d]*(s[d]-pbox_lower[d]+side_offset[d])
                      - strain->origin(d);
                    xyz_center(d)=geom_xlower[d]
                      + dx[d]*(s[d]-pbox_lower[d]+0.5);
                    xyz_edge_y(d)=geom_xlower[d]
                      + dx[d]*(s[d]-pbox_lower[d]+edge_offset_y[d]);
                    xyz_edge_z(d)=geom_xlower[d]
                      + dx[d]*(s[d]-pbox_lower[d]+edge_offset_z[d]);
                  }
                if(dim==2)
                  {
                    double intersect, intersect_mixed[2];
                    compute_intersections_2D(xyz_side_relative,*strain,Dx,
                                             dim,ix,intersect,intersect_mixed);
                    if(gbox.contains(s))
                      {
                        SAMRAI::pdat::CellIndex c(s);
                        dv_diagonal(c,ix)+=intersect*jump;
                        dstrain_diagonal(c,ix)+=
                          strain->derivative_correction_2D(ix,ix,xyz_center);
                      }
                    dv_mixed(s,0)+=intersect_mixed[0]*jump_mixed_y;
                    dv_mixed(s,1)+=intersect_mixed[1]*jump_mixed_y;
                    
                    SAMRAI::pdat::EdgeIndex
                      edge_y(s,iy,SAMRAI::pdat::EdgeIndex::LowerLeft);
                    /// FIXME: This is incorrect if applied strain is
                    /// not symmetric
                    dstrain_mixed(edge_y,0)+=
                      strain->derivative_correction_2D(ix,iy,xyz_edge_y);
                  }
                else
                  {
                    double intersect_diagonal, intersect_mixed[4],
                      intersect_corner[4];
                    compute_intersections_3D(xyz_side_relative,*strain,Dx,
                                             dim,ix,
                                             intersect_diagonal,intersect_mixed,
                                             intersect_corner);

                    const FTensor::Index<'a',3> a;
                    if(gbox.contains(s))
                      {
                        SAMRAI::pdat::CellIndex c(s);
                        dv_diagonal(c,ix)+=intersect_diagonal*jump;
                        dstrain_diagonal(c,ix)+=
                          strain->derivative_correction_3D(ix,ix,xyz_center);
                      }

                    dv_mixed(s,0)+=intersect_mixed[0]*jump_mixed_y;
                    dv_mixed(s,1)-=intersect_mixed[1]*jump_mixed_y;
                    dv_mixed(s,2)+=intersect_mixed[2]*jump_mixed_z;
                    dv_mixed(s,3)-=intersect_mixed[3]*jump_mixed_z;
                    dv_mixed(s,4)+=intersect_corner[0]*jump_corner_y_p_z;
                    dv_mixed(s,5)+=intersect_corner[1]*jump_corner_y_m_z;
                    dv_mixed(s,6)-=intersect_corner[2]*jump_corner_y_p_z;
                    dv_mixed(s,7)-=intersect_corner[3]*jump_corner_y_m_z;
                    
                    SAMRAI::pdat::EdgeIndex
                      edge_y(s,iy,SAMRAI::pdat::EdgeIndex::LowerLeft);
                    SAMRAI::pdat::EdgeIndex
                      edge_z(s,iz,SAMRAI::pdat::EdgeIndex::LowerLeft);
                    
                    /// FIXME: This is incorrect if applied strain is
                    /// not symmetric
                    dstrain_mixed(edge_z,ix_iy)+=
                      strain->derivative_correction_3D(ix,iy,xyz_edge_z);
                    dstrain_mixed(edge_y,ix_iz)+=
                      strain->derivative_correction_3D(ix,iz,xyz_edge_y);
                  }
              }
          }
      }
  }
}
