/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "compute_intersections_2D.hxx"
#include "compute_intersections_3D.hxx"
#include "Dir.hxx"
#include "Constants.hxx"
#include "Elastic/Adaptive_Solver/Strain.hxx"
#include "Elastic/Adaptive_Solver/Fault.hxx"

#include <SAMRAI/pdat/SideData.h>
#include <SAMRAI/pdat/CellData.h>
#include <SAMRAI/pdat/EdgeData.h>
#include <vector>

namespace Elastic
{
  void corrections_from_faults
  (const std::vector<Fault> &faults,
   const Gamra::Dir &dim, const double *dx,
   const double *geom_xlower,
   const SAMRAI::hier::Box &gbox,
   const SAMRAI::hier::Index &pbox_lower,
   const std::vector<FTensor::Tensor1<double,3> > &Dx,
   SAMRAI::pdat::CellData<double> &dv_diagonal,
   SAMRAI::pdat::SideData<double> &dv_mixed);

  void corrections_from_strains
  (const std::vector<Strain> &strains,
   const Gamra::Dir &dim, const double *dx,
   const double *geom_xlower,
   const SAMRAI::hier::Box &gbox,
   const SAMRAI::hier::Index &pbox_lower,
   const std::vector<FTensor::Tensor1<double,3> > &Dx,
   SAMRAI::pdat::CellData<double> &dv_diagonal,
   SAMRAI::pdat::SideData<double> &dv_mixed,
   SAMRAI::pdat::CellData<double> &dstrain_diagonal,
   SAMRAI::pdat::EdgeData<double> &dstrain_mixed);
  
  void compute_dv(const std::vector<Strain> &strains,
                  const std::vector<Fault> &faults,
                  const Gamra::Dir &dim, const double *dx,
                  const double *geom_xlower,
                  const SAMRAI::hier::Box &gbox,
                  const SAMRAI::hier::Index &pbox_lower,
                  SAMRAI::pdat::CellData<double> &dv_diagonal,
                  SAMRAI::pdat::SideData<double> &dv_mixed,
                  SAMRAI::pdat::CellData<double> &dstrain_diagonal,
                  SAMRAI::pdat::EdgeData<double> &dstrain_mixed)
  {
    dv_diagonal.fillAll(0);
    dv_mixed.fillAll(0);
    dstrain_diagonal.fillAll(0);
    dstrain_mixed.fillAll(0);

    std::vector<FTensor::Tensor1<double,3> > Dx(dim);
    FTensor::Index<'a',3> a;
    FTensor::Index<'b',3> b;
    for(int d0=0;d0<dim;++d0)
      {
        Dx[d0](a)=0.0;
        Dx[d0](d0)=dx[d0];
      }
    corrections_from_faults(faults,dim,dx,geom_xlower,gbox,pbox_lower,Dx,
                            dv_diagonal,dv_mixed);
    corrections_from_strains(strains,dim,dx,geom_xlower,gbox,pbox_lower,
                             Dx,dv_diagonal,dv_mixed,dstrain_diagonal,
                             dstrain_mixed);
  }
}
