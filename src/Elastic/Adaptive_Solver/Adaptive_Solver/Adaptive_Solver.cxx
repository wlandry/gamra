/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "Constants.hxx"
#include "../../Adaptive_Solver.hxx"

#include <SAMRAI/pdat/NodeVariable.h>
#include <SAMRAI/pdat/CellVariable.h>
#include <SAMRAI/pdat/SideVariable.h>
#include <SAMRAI/pdat/EdgeVariable.h>

namespace Elastic
{
  std::vector<Strain> extract_strains(SAMRAI::tbox::Database &database);
  std::vector<Fault> extract_faults(SAMRAI::tbox::Database &database);
}

Elastic::Adaptive_Solver::Adaptive_Solver
(const SAMRAI::tbox::Dimension& dimension,
 SAMRAI::tbox::Database &Database):
  database(Database),
  dimension(dimension),
  lambda("lambda",database,dimension),
  mu("mu",database,dimension),
  topography("topography",database,dimension)
{
  const int dim(dimension.getValue());

  std::string xyz("xyz");
  for(int d=0; d<dim; ++d)
    {
      v_rhs_input.emplace_back(std::string("v_rhs_")+xyz[d],database,
                               dimension,true);
      v_initial.emplace_back(std::string("v_initial_")+xyz[d],database,
                             dimension);
    }

  SAMRAI::hier::VariableDatabase* vdb =
    SAMRAI::hier::VariableDatabase::getDatabase();

  /// Get a unique context for variables owned by this object.
  context = vdb->getContext("Elastic::FAC:Context");

  /// Register variables with SAMRAI::hier::VariableDatabase and get
  /// the descriptor indices for those variables.  Ghost cells width
  /// are 1 just in case it is needed.

  int depth=2;
  auto cell_moduli_ptr(boost::make_shared<SAMRAI::pdat::CellVariable<double>>
                       (dimension,"Elastic::FAC:cell_moduli",depth));
  cell_moduli_id = vdb->registerVariableAndContext
    (cell_moduli_ptr,context,SAMRAI::hier::IntVector::getOne(dimension));

  strains = extract_strains(database);
  faults = extract_faults(database);
  if(have_jump_corrections())
    {
      auto dv_diagonal_ptr(boost::make_shared<SAMRAI::pdat::CellVariable<double>>
                           (dimension,"Elastic::FAC:dv_diagonal",dim));
      dv_diagonal_id = vdb->registerVariableAndContext
        (dv_diagonal_ptr,context,SAMRAI::hier::IntVector::getOne(dimension));

      auto dv_mixed_ptr(boost::make_shared<SAMRAI::pdat::SideVariable<double>>
                        (dimension,"Elastic::FAC:dv_mixed",
                         dim==2 ? 2 : 8));
      dv_mixed_id = vdb->registerVariableAndContext
        (dv_mixed_ptr,context,SAMRAI::hier::IntVector::getOne(dimension));

      auto dstrain_diagonal_ptr
        (boost::make_shared<SAMRAI::pdat::CellVariable<double>>
         (dimension,"Elastic::FAC:dstrain_diagonal",dim));
      dstrain_diagonal_id = vdb->registerVariableAndContext
        (dstrain_diagonal_ptr,context,
         SAMRAI::hier::IntVector::getOne(dimension));

      auto dstrain_mixed_ptr(boost::make_shared<SAMRAI::pdat::EdgeVariable<double>>
                             (dimension,"Elastic::FAC:dstrain_mixed",
                              dim-1));
      dstrain_mixed_id = vdb->registerVariableAndContext
        (dstrain_mixed_ptr,context,
         SAMRAI::hier::IntVector::getOne(dimension));
    }
  if(database.keyExists("refinement_points"))
    { refinement_points=database.getDoubleVector("refinement_points"); }
  if(refinement_points.size()%3!=0)
    { TBOX_ERROR("The number of points in refinement_points must be "
                 "divisible by 3.  Read "
                 << refinement_points.size() << " points"); }

  auto edge_moduli_ptr(boost::make_shared<SAMRAI::pdat::EdgeVariable<double>>
                       (dimension,"Elastic::FAC:edge_moduli",depth));
  edge_moduli_id = vdb->registerVariableAndContext
    (edge_moduli_ptr,context,SAMRAI::hier::IntVector::getOne(dimension));

  auto v_ptr(boost::make_shared<SAMRAI::pdat::SideVariable<double>>
             (dimension, "Elastic::FAC:v", 1));
  v_id = vdb->registerVariableAndContext
    (v_ptr,context,SAMRAI::hier::IntVector::getOne(dimension));

  auto v_rhs_ptr(boost::make_shared<SAMRAI::pdat::SideVariable<double>>
                 (dimension,"Elastic::FAC:v right hand side"));
  v_rhs_id =
    vdb->registerVariableAndContext(v_rhs_ptr,context,
                                    SAMRAI::hier::IntVector::getOne(dimension));

  d_adaption_threshold=database.getDoubleWithDefault("adaption_threshold",
                                                      1.0e-15);
  min_full_refinement_level
    =database.getIntegerWithDefault("min_full_refinement_level",0);

  const std::string geometry_string (database.getStringWithDefault("geometry",
                                                                   "cartesian"));
  if (geometry_string == "cartesian")
    { geometry=Geometry::CARTESIAN; }
  else if (geometry_string == "spherical")
    { geometry=Geometry::SPHERICAL; }
  else if (geometry_string == "topography")
    { geometry=Geometry::TOPOGRAPHY; }
  else
    { TBOX_ERROR("Invalid setting for geometry: '" << geometry_string
                 << "'.  It must be one of 'cartesian', 'spherical', "
                 << "or 'topography'."); }

  if(geometry!=Geometry::CARTESIAN)
    {
      auto xyz(boost::make_shared<SAMRAI::pdat::NodeVariable<double>>
               (dimension,"xyz", dim));
      xyz_id = vdb->registerVariableAndContext
        (xyz,context,SAMRAI::hier::IntVector::getOne(dimension));

      auto gg(boost::make_shared<SAMRAI::pdat::SideVariable<double>>
              (dimension,"gg", dim-1));
      gg_id = vdb->registerVariableAndContext
        (gg,context,SAMRAI::hier::IntVector::getOne(dimension));
      
      auto christof_cell(boost::make_shared<SAMRAI::pdat::CellVariable<double>>
                         (dimension,"christof_cell", dim==2 ? 4 : 9));
      christof_cell_id = vdb->registerVariableAndContext
        (christof_cell,context,SAMRAI::hier::IntVector::getOne(dimension));

      auto christof_edge
        (boost::make_shared<SAMRAI::pdat::EdgeVariable<double>>
         (dimension,"christof_edge", dim==2 ? 4 : 9));
      christof_edge_id = vdb->registerVariableAndContext
        (christof_edge,context,SAMRAI::hier::IntVector::getOne(dimension));
    }
}
