/// Copyright © 2016, 2017 California Institute of Technology

#include "check_array_size.hxx"
#include "../Fault.hxx"
#include <vector>
#include <map>
#include <SAMRAI/tbox/Database.h>

namespace
{
  FTensor::Tensor1<double,3> get_vertex
  (const std::map<int,FTensor::Tensor1<double,3> > &vertices,
   const double &index)
  {
    std::map<int,FTensor::Tensor1<double,3> >::const_iterator
      v(vertices.find(static_cast<int>(index)));
    if(v==vertices.end())
      TBOX_ERROR("Unknown vertex number: " << index);
    return v->second;
  }
}

namespace Elastic
{
  std::vector<Fault> extract_faults(SAMRAI::tbox::Database &database)
  {
    std::vector<Fault> result;
    const double pi=4*atan(1);

    if(database.keyExists("faults"))
      {
        std::vector<double> faults (database.getDoubleVector("faults"));
        const size_t num_constructor_args (9);
        check_array_size(faults,num_constructor_args,"faults");
        for(size_t ii=0; ii<faults.size(); ii+=num_constructor_args)
          {
            result.push_back(Fault(-faults[ii],
                                   FTensor::Tensor1<double,3>(faults[ii+1],faults[ii+2],
                                                              faults[ii+3]),
                                   FTensor::Tensor1<double,2>(faults[ii+4],faults[ii+5]),
                                   pi/2-faults[ii+6]*pi/180,
                                   faults[ii+7]*pi/180,faults[ii+8]*pi/180));
          }
      }
    
    if(database.keyExists("faults_2D"))
      {
        std::vector<double> faults (database.getDoubleVector("faults_2D"));
        const size_t num_constructor_args (5);
        check_array_size(faults,num_constructor_args,"faults_2D");
        for(size_t ii=0; ii<faults.size(); ii+=num_constructor_args)
          {
            result.push_back(Fault(-faults[ii],
                                   FTensor::Tensor1<double,3>(faults[ii+1],faults[ii+2],0),
                                   FTensor::Tensor1<double,2>(faults[ii+3],0),
                                   pi/2-faults[ii+4]*pi/180,pi/2,0));
          }
      }
    
    if(database.keyExists("triangular_faults"))
      {
        std::vector<double> triangular_faults (database.getDoubleVector
                                               ("triangular_faults"));
        const size_t num_constructor_args (12);
        check_array_size(triangular_faults,num_constructor_args,
                         "triangular_faults");
        for(size_t ii=0; ii<triangular_faults.size(); ii+=num_constructor_args)
          {
            FTensor::Tensor1<double,3> u, v;
            const FTensor::Tensor1<double,3>
              origin(triangular_faults[ii],triangular_faults[ii+1],
                 triangular_faults[ii+2]),
              p1(triangular_faults[ii+3],triangular_faults[ii+4],
                 triangular_faults[ii+5]),
              p2(triangular_faults[ii+6],triangular_faults[ii+7],
                 triangular_faults[ii+8]),
              jump(-triangular_faults[ii+9],-triangular_faults[ii+10],
                   -triangular_faults[ii+11]);
            FTensor::Index<'a',3> a;
            u(a) = p1(a) - origin(a);
            v(a) = p2(a) - origin(a);
            result.push_back(Fault(origin,u,v,jump));
          }
      }

    if(database.keyExists("unicycle_slip_on_triangles"))
      {
        if(database.keyExists("vertices"))
          {
            std::vector<double> vertex_array (database.getDoubleVector
                                              ("vertices"));
            const size_t num_vertex_args (4);
            check_array_size(vertex_array,num_vertex_args,
                             "vertices");
            std::map<int,FTensor::Tensor1<double,3> > vertices;
            for(size_t ii=0; ii<vertex_array.size(); ii+=num_vertex_args)
              {
                vertices.insert
                  (std::make_pair
                   (static_cast<int>(vertex_array[ii]),
                    FTensor::Tensor1<double,3>(vertex_array[ii+1],
                                               vertex_array[ii+2],
                                               vertex_array[ii+3])));
              }
            std::vector<double> slip_array (database.getDoubleVector
                                            ("unicycle_slip_on_triangles"));
            const size_t num_slip_args (6);
            for(size_t ii=0; ii<slip_array.size(); ii+=num_slip_args)
              {
                /// Format from Unicycle input files.  A bit odd and
                /// problematic when the fault is nearly horizontal.
                /// The format is Slip Id, Slip magnitude, Vertex Id 1,
                /// Vertex Id 2, Vertex Id 3, rake(deg)
                double slip_magnitude (slip_array[ii+1]),
                  rake (slip_array[ii+5]*pi/180);
                FTensor::Tensor1<double,3> dv1, dv2, normal;
                const FTensor::Tensor1<double,3>
                  v0(get_vertex(vertices,slip_array[ii+2])),
                  v1(get_vertex(vertices,slip_array[ii+3])),
                  v2(get_vertex(vertices,slip_array[ii+4]));

                FTensor::Index<'a',3> a;
                dv1(a) = v1(a) - v0(a);
                dv2(a) = v2(a) - v0(a);

                normal(0) = dv1(1)*dv2(2) - dv1(2)*dv2(1);
                normal(1) = dv1(2)*dv2(0) - dv1(0)*dv2(2);
                normal(2) = dv1(0)*dv2(1) - dv1(1)*dv2(0);
                normal.normalize();
                /// Make the norm always point up
                if (normal(2) < 0)
                  normal(a) = -normal(a);

                double angle (atan2(normal(1),normal(0)));
                FTensor::Tensor1<double,3> strike(-sin(angle), cos(angle), 0),
                  dip, slip;
                dip(0) = normal(1)*strike(2) - normal(2)*strike(1);
                dip(1) = normal(2)*strike(0) - normal(0)*strike(2);
                dip(2) = normal(0)*strike(1) - normal(1)*strike(0);
                slip(a) = -slip_magnitude*(cos(rake) * strike(a)
                                           + sin(rake) * dip(a));
                result.push_back(Fault(v0, dv1, dv2, slip));
              }
          }
        else
          {
            TBOX_ERROR("Found 'unicycle_slip_on_triangles', but no "
                       "corresponding 'vertices'.");
          }
      }
    return result;
  }
}
