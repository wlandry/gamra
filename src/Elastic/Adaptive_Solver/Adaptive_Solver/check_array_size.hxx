#pragma once
/// Copyright © 2016, 2017 California Institute of Technology

#include <vector>
#include <string>
#include <SAMRAI/tbox/Utilities.h>

namespace Elastic
{
  inline void check_array_size(const std::vector<double> &array,
                               const size_t &num_constructor_args,
                               const std::string &array_name)
  {
    if(array.size()%num_constructor_args!=0)
      TBOX_ERROR("The number of points in '"
                 << array_name << "' must be divisible by "
                 << num_constructor_args << ", but read "
                 << array.size() << " points.");
  }
}
