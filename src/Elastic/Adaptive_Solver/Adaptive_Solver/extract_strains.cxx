/// Copyright © 2016, 2017 California Institute of Technology

#include "check_array_size.hxx"
#include "../Strain.hxx"
#include <SAMRAI/tbox/Database.h>

namespace Elastic
{
  std::vector<Strain> extract_strains(SAMRAI::tbox::Database &database)
  {
    std::vector<Strain> result;
    const double pi=4*atan(1);

    if(database.keyExists("strains"))
      {
        std::vector<double> strains (database.getDoubleVector("strains"));
        const size_t num_constructor_args (14);
        check_array_size(strains,num_constructor_args,"strains");
        for(size_t ii=0; ii<strains.size(); ii+=num_constructor_args)
          {
            result.push_back
              (Strain(FTensor::Tensor2_symmetric<double,3>
                      (strains[ii],strains[ii+1],strains[ii+2],
                       strains[ii+3],strains[ii+4],strains[ii+5]),
                      FTensor::Tensor1<double,3>(strains[ii+6],strains[ii+7],
                                                 strains[ii+8]),
                      FTensor::Tensor1<double,3>(strains[ii+9],strains[ii+10],
                                                 strains[ii+11]),
                      pi/2-strains[ii+12]*pi/180,
                      strains[ii+13]*pi/180));
          }
      }
    if(database.keyExists("strains_tetrahedra"))
      {
        std::vector<double>
          strains (database.getDoubleVector("strains_tetrahedra"));
        const size_t num_constructor_args (21);
        check_array_size(strains,num_constructor_args,"strains_tetrahedra");
        for(size_t ii=0; ii<strains.size(); ii+=num_constructor_args)
          {
            result.push_back
              (Strain(FTensor::Tensor2<double,3,3>
                      (strains[ii],strains[ii+1],strains[ii+2],
                       strains[ii+3],strains[ii+4],strains[ii+5],
                       strains[ii+6],strains[ii+7],strains[ii+8]),
                      FTensor::Tensor1<double,3>(strains[ii+9],strains[ii+10],
                                                 strains[ii+11]),
                      FTensor::Tensor1<double,3>(strains[ii+12],strains[ii+13],
                                                 strains[ii+14]),
                      FTensor::Tensor1<double,3>(strains[ii+15],strains[ii+16],
                                                 strains[ii+17]),
                      FTensor::Tensor1<double,3>(strains[ii+18],strains[ii+19],
                                                 strains[ii+20])));
          }
      }
    if(database.keyExists("strains_2d"))
      {
        std::vector<double> strains (database.getDoubleVector("strains_2d"));
        const size_t num_constructor_args (8);
        check_array_size(strains,num_constructor_args,"strains_2d");
        for(size_t ii=0; ii<strains.size(); ii+=num_constructor_args)
          {
            result.push_back
              (Strain(FTensor::Tensor2_symmetric<double,2>
                      (strains[ii],strains[ii+1],strains[ii+2]),
                      FTensor::Tensor1<double,2>(strains[ii+3],strains[ii+4]),
                      FTensor::Tensor1<double,2>(strains[ii+5],strains[ii+6]),
                      pi/2-strains[ii+7]*pi/180));
          }
      }
    if(database.keyExists("strains_triangle"))
      {
        std::vector<double> strains (database.getDoubleVector("strains_triangle"));
        const size_t num_constructor_args (10);
        check_array_size(strains,num_constructor_args,"strains_triangle");
        for(size_t ii=0; ii<strains.size(); ii+=num_constructor_args)
          {
            result.push_back
              (Strain(FTensor::Tensor2<double,2,2>
                      (strains[ii],strains[ii+1],strains[ii+2],strains[ii+3]),
                      FTensor::Tensor1<double,2>(strains[ii+4],strains[ii+5]),
                      FTensor::Tensor1<double,2>(strains[ii+6],strains[ii+7]),
                      FTensor::Tensor1<double,2>(strains[ii+8],strains[ii+9])));

          }
      }
    return result;
  }
}
