#pragma once

/// Copyright © 2016-2018 California Institute of Technology

#include <FTensor.hpp>

#include "Dir.hxx"
#include "Affine_Parameters_2D.hxx"
#include "Affine_Parameters_3D.hxx"

namespace Elastic
{
  class Strain
  {
  public:
    FTensor::Tensor1<double,3> origin, u, v, w;
    FTensor::Tensor2<double,3,3> strain;
    bool is_tetrahedra;

    /// rectangle
    Strain (const FTensor::Tensor2_symmetric<double,2> &strain_relative_to_fault,
            const FTensor::Tensor1<double,2> &Origin,
            const FTensor::Tensor1<double,2> &proportions,
            const double &strike);

    /// triangle
    Strain (const FTensor::Tensor2<double,2,2> &absolute_strain,
            const FTensor::Tensor1<double,2> &vertex0,
            const FTensor::Tensor1<double,2> &vertex1,
            const FTensor::Tensor1<double,2> &vertex2);

    /// box
    Strain (const FTensor::Tensor2_symmetric<double,3> &strain_relative_to_fault,
            const FTensor::Tensor1<double,3> &Origin,
            const FTensor::Tensor1<double,3> &proportions,
            const double &strike, const double &dip);

    /// tetrahedra
    Strain (const FTensor::Tensor2<double,3,3> &absolute_strain,
            const FTensor::Tensor1<double,3> &vertex0,
            const FTensor::Tensor1<double,3> &vertex1,
            const FTensor::Tensor1<double,3> &vertex2,
            const FTensor::Tensor1<double,3> &vertex3);

    double intersect_2D (const FTensor::Tensor1<double,3> &xyz,
                         const FTensor::Tensor1<double,3> &dx) const;
    
    double intersect_3D (const FTensor::Tensor1<double,3> &xyz,
                         const FTensor::Tensor1<double,3> &dx) const;

    double derivative_correction_2D (const Gamra::Dir &ix, const Gamra::Dir &iy,
                                     const FTensor::Tensor1<double,3> &xyz)
      const
    {
      return inside_2D(xyz) ? strain(ix,iy) : 0;
    }
    double derivative_correction_3D (const Gamra::Dir &ix, const Gamra::Dir &iy,
                                     const FTensor::Tensor1<double,3> &xyz) const
    {
      return inside_3D(xyz) ? strain(ix,iy) : 0;
    }

    bool inside_2D(const FTensor::Tensor1<double,3> &xyz) const
    {
      FTensor::Index<'a',3> a;
      FTensor::Tensor1<double,3> offset_xyz;
      offset_xyz(a)=xyz(a)-origin(a);
      if(!is_tetrahedra)
        {
          return Affine_Parameters_2D(offset_xyz,w,u,w,w).intersects;
        }
      else
        {
          FTensor::Tensor1<double,3> opposite;
          opposite(a)=w(a)-u(a);
          FTensor::Tensor1<double,3> u_normal(u(1),-u(0),0), w_normal(w(1),-w(0),0),
            opposite_normal(opposite(1),-opposite(0),0);
          FTensor::Index<'i',2> i;
          FTensor::Index<'j',2> j;
          u_normal(i)=levi_civita(i,j)*u(j);
          
          return Affine_Parameters_2D(offset_xyz,w,u,u_normal,w).intersects
            && Affine_Parameters_2D(offset_xyz,u,w,w_normal,u).intersects
            && Affine_Parameters_2D(offset_xyz,u,opposite,
                                    opposite_normal,u).intersects;
        }
    }
    bool inside_3D(const FTensor::Tensor1<double,3> &xyz) const
    {
      FTensor::Index<'a',3> a;
      FTensor::Tensor1<double,3> offset_xyz;
      offset_xyz(a)=xyz(a)-origin(a)-w(a);
      if(!is_tetrahedra)
        {
          return Affine_Parameters_3D(offset_xyz,w,u,v,w,
                                      is_tetrahedra).intersects;
        }
      else
        {
          FTensor::Index<'b',3> b;
          FTensor::Index<'c',3> c;
          FTensor::Tensor1<double,3> u_cross_v, v_cross_w, w_cross_u;
          u_cross_v(c)=FTensor::cross(u(a),v(b),c);
          v_cross_w(c)=FTensor::cross(v(a),w(b),c);
          w_cross_u(c)=FTensor::cross(w(a),u(b),c);
  
          return Affine_Parameters_3D(offset_xyz,w,u,v,u_cross_v,w,
                                      is_tetrahedra).intersects
            && Affine_Parameters_3D(offset_xyz,u,v,w,v_cross_w,u,
                                    is_tetrahedra).intersects            
            && Affine_Parameters_3D(offset_xyz,v,w,u,w_cross_u,v,
                                    is_tetrahedra).intersects;
        }
    }

    double jump(const Gamra::Dir &ix) const
    {
      FTensor::Index<'a',3> a;
      return strain(ix,a)*w(a);
    }
  };
}
