#include "../../Adaptive_Solver.hxx"

#include <SAMRAI/pdat/NodeData.h>
#include <SAMRAI/geom/CartesianPatchGeometry.h>

void Elastic::Adaptive_Solver::initialize_node_data
(const SAMRAI::hier::Patch &patch,
 const boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
 const double *dx, const int &dim)
{
  /// xyz for non-cartesian geometries
  if(geometry!=Geometry::CARTESIAN)
    {
      std::vector<double> coord(dim);
      boost::shared_ptr<SAMRAI::pdat::NodeData<double> >
        xyz{boost::dynamic_pointer_cast<SAMRAI::pdat::NodeData<double>>
          (patch.getPatchData(xyz_id))};
      SAMRAI::hier::Box xyz_box = xyz->getBox();

      auto end(SAMRAI::pdat::NodeGeometry::end (xyz->getGhostBox()));
      for(auto
            ni(SAMRAI::pdat::NodeGeometry::begin (xyz->getGhostBox()));
          ni!=end; ++ni)
        {
          const SAMRAI::pdat::NodeIndex &x(*ni);
          for(int d=0;d<dim;++d)
            {
              coord[d]=geom->getXLower()[d]
                + dx[d]*(x[d]-xyz_box.lower()[d]);
            }
          switch(geometry)
            {
            case Geometry::SPHERICAL:
              {
                double sin_theta (sin(coord[1])),
                  cos_theta (cos(coord[1]));
                switch(dim)
                  {
                  case 2:
                    (*xyz)(x,0) = coord[0]*cos_theta;
                    (*xyz)(x,1) = coord[0]*sin_theta;
                    break;
                  case 3:
                    {
                      double sin_phi (sin(coord[2])),
                        cos_phi (cos(coord[2]));
                      (*xyz)(x,0) = coord[0]*sin_theta*cos_phi;
                      (*xyz)(x,1) = coord[0]*sin_theta*sin_phi;
                      (*xyz)(x,2) = coord[0]*cos_theta;
                    }
                    break;
                  }
              }
              break;
            case Geometry::TOPOGRAPHY:
              {
                double height=topography.eval(coord.data());
                (*xyz)(x,0) = coord[0];
                switch(dim)
                  {
                  case 2:
                    (*xyz)(x,1) = coord[1]*height;
                    break;
                  case 3:
                    (*xyz)(x,1) = coord[1];
                    (*xyz)(x,2) = coord[2]*height;
                    break;
                  }
              }
              break;
            default:
              break;
            }
        }
    }
}
