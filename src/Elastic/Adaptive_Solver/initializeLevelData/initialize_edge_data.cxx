#include "../../Adaptive_Solver.hxx"

#include <SAMRAI/pdat/EdgeData.h>
#include <SAMRAI/geom/CartesianPatchGeometry.h>

void Elastic::Adaptive_Solver::initialize_edge_data
(const SAMRAI::hier::Patch &patch,
 const boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
 const double *dx, const int &dim)
{
  if(geometry!=Geometry::CARTESIAN)
    {
      auto &christof_edge
        (*boost::dynamic_pointer_cast<SAMRAI::pdat::EdgeData<double> >
         (patch.getPatchData(christof_edge_id)));
      
      SAMRAI::hier::Box edge_box = christof_edge.getBox();
      for(Gamra::Dir ix=0; ix<dim; ++ix)
        {
          std::array<double,3> offset{0,0,0};
          offset[ix]=0.5;

          auto edge_end(SAMRAI::pdat::EdgeGeometry::end
                        (christof_edge.getGhostBox(),ix));
          for(auto edge(SAMRAI::pdat::EdgeGeometry::begin
                        (christof_edge.getGhostBox(),ix));
              edge!=edge_end; ++edge)
            {
              const SAMRAI::pdat::EdgeIndex &e(*edge);
              switch(geometry)
                {
                case Geometry::SPHERICAL:
                  {
                    std::vector<double> coord(dim);
                    for(int d=0;d<dim;++d)
                      { coord[d]=geom->getXLower()[d]
                          + dx[d]*(e[d]-edge_box.lower()[d] + offset[d]); }
                    /// 2D: C_cell={rrr, rrt, rtt, trr, trt, ttt}
                    switch(dim)
                      {
                      case 2:
                        christof_edge(e,0)=0;
                        christof_edge(e,1)=0;
                        christof_edge(e,2)=1/coord[0];
                        christof_edge(e,3)=0;
                        break;
                      case 3:
                        // TODO
                        /// 3D: C_edge={rrr, rrt, rrp, rtt, rpp,
                        ///             trr, trt, ttt, ttp, tpp,
                        ///             prr, prp, ptt, ptp, ppp}
                        /// t==theta, p=phi
                        // const double sin_theta(sin(coord[1])),
                        //   cos_theta(cos(coord[1]));
              
                        // christof_edge(e,0)=0;
                        // christof_edge(e,1)=0;
                        // christof_edge(e,2)=0;
                        // christof_edge(e,3)=-coord[0];
                        // christof_edge(e,4)=-coord[0]*sin_theta*sin_theta;

                        // christof_edge(e,5)=0;
                        // christof_edge(e,6)=1/coord[0];
                        // christof_edge(e,7)=0;
                        // christof_edge(e,8)=0;
                        // christof_edge(e,9)=-sin_theta*cos_theta;

                        // christof_edge(e,10)=0;
                        // christof_edge(e,11)=1/coord[0];
                        // christof_edge(e,12)=0;
                        // christof_edge(e,13)=cos_theta/sin_theta;
                        // christof_edge(e,14)=0;
                        break;
                      }
                  }
                  break;
                case Geometry::TOPOGRAPHY:
                  {
                    // FIXME: TODO
                    switch(dim)
                      {
                      case 2:
                        break;
                      case 3:
                        break;
                      }
                  }
                  break;
                default:
                  {
                    break;
                  }
                }
            }
        }
    }
}
