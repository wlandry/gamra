/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../../Adaptive_Solver.hxx"
#include "Constants.hxx"

#include <SAMRAI/pdat/NodeData.h>
#include <SAMRAI/pdat/SideData.h>
#include <SAMRAI/geom/CartesianPatchGeometry.h>

/// Initialize data on a level, including allocating memory.  It does
/// not set the terms due to the faults, since the moduli have to be
/// fixed first.

void Elastic::Adaptive_Solver::initializeLevelData
(const boost::shared_ptr<SAMRAI::hier::PatchHierarchy> &patch_hierarchy,
 const int level_number,
 const double ,
 const bool ,
 const bool ,
 const boost::shared_ptr<SAMRAI::hier::PatchLevel>& ,
 const bool allocate_data)
{
  boost::shared_ptr<SAMRAI::hier::PatchHierarchy>
    hierarchy = patch_hierarchy;

  SAMRAI::hier::PatchLevel &level = *hierarchy->getPatchLevel(level_number);
  const int dim=dimension.getValue();

  if(allocate_data)
    {
      level.allocatePatchData(cell_moduli_id);
      level.allocatePatchData(edge_moduli_id);
      level.allocatePatchData(v_id);
      level.allocatePatchData(v_rhs_id);
      if(have_jump_corrections())
        {
          level.allocatePatchData(dv_diagonal_id);
          level.allocatePatchData(dv_mixed_id);
          level.allocatePatchData(dstrain_diagonal_id);
          level.allocatePatchData(dstrain_mixed_id);
        }
      if(geometry!=Geometry::CARTESIAN)
        {
          level.allocatePatchData(xyz_id);
          level.allocatePatchData(gg_id);
          level.allocatePatchData(christof_cell_id);
          level.allocatePatchData(christof_edge_id);
        }
    }
  /// Initialize data in all patches in the level.
  for (SAMRAI::hier::PatchLevel::Iterator p(level.begin()); p!=level.end();
       ++p)
    {
      SAMRAI::hier::Patch &patch = **p;
      boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> geom =
        boost::dynamic_pointer_cast<SAMRAI::geom::CartesianPatchGeometry>
        (patch.getPatchGeometry());
      const double *dx=geom->getDx();

      initialize_cell_data(patch,geom,dx,dim);
      initialize_side_data(patch,geom,dx,dim);
      initialize_edge_data(patch,geom,dx,dim);
      initialize_node_data(patch,geom,dx,dim);
    }
}
