#include "../../Adaptive_Solver.hxx"

#include <SAMRAI/pdat/SideData.h>
#include <SAMRAI/geom/CartesianPatchGeometry.h>

void Elastic::Adaptive_Solver::initialize_side_data
(const SAMRAI::hier::Patch &patch,
 const boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
 const double *dx, const int &dim)
{
  boost::shared_ptr<SAMRAI::pdat::SideData<double> >
    v_rhs{boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
      (patch.getPatchData(v_rhs_id))};

  boost::shared_ptr<SAMRAI::pdat::SideData<double> >
    gg{boost::dynamic_pointer_cast<SAMRAI::pdat::SideData<double> >
      (patch.getPatchData(gg_id))};
  
  SAMRAI::hier::Box v_rhs_box(v_rhs->getBox());
  for(Gamra::Dir ix=0; ix<dim; ++ix)
    {
      double offset[]={0.5,0.5,0.5};
      offset[ix]=0;
      std::vector<double> coord(dim);

      auto end(SAMRAI::pdat::SideGeometry::end(v_rhs_box,ix));
          
      for(auto si(SAMRAI::pdat::SideGeometry::begin(v_rhs_box,ix));
          si!=end; ++si)
        {
          const SAMRAI::pdat::SideIndex &x(*si);

          for(int d=0;d<dim;++d)
            { coord[d]=geom->getXLower()[d]
                + dx[d]*(x[d]-v_rhs_box.lower()[d]+offset[d]); }
          (*v_rhs)(x)=v_rhs_input[ix].eval(coord.data());

          switch(geometry)
            {
            case Geometry::SPHERICAL:
              {
                if(dim==2)
                  {
                    switch(ix)
                      {
                      case 0:
                        (*gg)(x)=1/(coord[0]*coord[0]);
                        break;
                      case 1:
                        (*gg)(x)=coord[0]*coord[0];
                        break;
                      }
                  }
                // TODO: Implement 3D
                break;
              }
            }
        }
    }
}
