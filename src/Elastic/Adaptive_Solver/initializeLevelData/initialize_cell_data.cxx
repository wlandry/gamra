#include "../../Adaptive_Solver.hxx"

#include <SAMRAI/pdat/CellData.h>
#include <SAMRAI/geom/CartesianPatchGeometry.h>

void Elastic::Adaptive_Solver::initialize_cell_data
(const SAMRAI::hier::Patch &patch,
 const boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
 const double *dx, const int &dim)
{
  /// cell moduli
  boost::shared_ptr<SAMRAI::pdat::CellData<double> >
    cell_moduli {boost::dynamic_pointer_cast<SAMRAI::pdat::CellData<double> >
      (patch.getPatchData(cell_moduli_id))},
    christof_cell{geometry==Geometry::CARTESIAN ? nullptr :
        boost::dynamic_pointer_cast<SAMRAI::pdat::CellData<double> >
        (patch.getPatchData(christof_cell_id))};
      
  SAMRAI::hier::Box cell_moduli_box = cell_moduli->getBox();
  
  SAMRAI::pdat::CellIterator
    cend(SAMRAI::pdat::CellGeometry::end(cell_moduli->getGhostBox()));
  for(SAMRAI::pdat::CellIterator
        ci(SAMRAI::pdat::CellGeometry::begin(cell_moduli->getGhostBox()));
      ci!=cend; ++ci)
    {
      const SAMRAI::pdat::CellIndex &c(*ci);
      double coord[3];
      for(int d=0;d<dim;++d)
        { coord[d]=geom->getXLower()[d]
            + dx[d]*(c[d]-cell_moduli_box.lower()[d] + 0.5); }

      (*cell_moduli)(c,0)=lambda.eval(coord);
      (*cell_moduli)(c,1)=mu.eval(coord);

      switch(geometry)
        {
        case Geometry::SPHERICAL:
          // 2D: C_cell={Γ^r_rr, Γ^r_rt, Γ^t_tr, Γ^t_tt}
          // Others come from symmetries and orhogonality of the metric.
          switch(dim)
            {
            case 2:
              (*christof_cell)(c,0)=0;
              (*christof_cell)(c,1)=0;
              (*christof_cell)(c,2)=1/coord[0];
              (*christof_cell)(c,3)=0;
              break;
            case 3:
              // TODO
              /// 3D: C_cell={rrr, rrt, rrp, rtt, rpp,
              ///             trr, trt, ttt, ttp, tpp,
              ///             prr, prp, ptt, ptp, ppp}
              /// t==theta, p=phi
              // const double sin_theta(sin(coord[1])),
              //   cos_theta(cos(coord[1]));
              
              // (*christof_cell)(c,0)=0;
              // (*christof_cell)(c,1)=0;
              // (*christof_cell)(c,2)=0;
              // (*christof_cell)(c,3)=-coord[0];
              // (*christof_cell)(c,4)=-coord[0]*sin_theta*sin_theta;

              // (*christof_cell)(c,5)=0;
              // (*christof_cell)(c,6)=1/coord[0];
              // (*christof_cell)(c,7)=0;
              // (*christof_cell)(c,8)=0;
              // (*christof_cell)(c,9)=-sin_theta*cos_theta;

              // (*christof_cell)(c,10)=0;
              // (*christof_cell)(c,11)=1/coord[0];
              // (*christof_cell)(c,12)=0;
              // (*christof_cell)(c,13)=cos_theta/sin_theta;
              // (*christof_cell)(c,14)=0;
              break;
            }
          break;
        case Geometry::TOPOGRAPHY:
          /// FIXME: TODO
          switch(dim)
            {
            case 2:
              break;
            case 3:
              break;
            }
          break;
        default:
          break;
        }
    }
}
