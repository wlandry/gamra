#include "Affine_Parameters_2D.hxx"

#include <boost/math/special_functions/sign.hpp>

#include <limits>

/// From http://geomalgorithms.com/a06-_intersect-2.html,

/// Intersection of a Ray/Segment with a Plane
///
/// Assume we have a ray R (or segment S) from P0 to P1, and a plane P
/// through V0 with normal n. The intersection of the parametric line
/// L: P(r)=P0+r(P1-P0) and the plane P occurs at the point P(rI) with
/// parameter value:

/// r1 = (n . (V0 - P0))/(n . (P1 - P0))

/// When the denominator n . (P1-P0)=0, the line L is parallel to the
/// plane P, and thus either does not intersect it or else lies
/// completely in the plane (whenever either P0 or P1 is in
/// P). Otherwise, when the denominator is nonzero and r1 is a real
/// number, then the ray R intersects the plane P only when r1 >= 0. A
/// segment S intersects P only if 0 <= r1 <= 1. In all algorithms,
/// the additional test r1 <= 1 is the only difference for a segment
/// instead of a ray.

void Elastic::Affine_Parameters_2D::init
(const FTensor::Tensor1<double,3> &xyz,
 const FTensor::Tensor1<double,3> &dx,
 const FTensor::Tensor1<double,3> &fault_segment,
 const FTensor::Tensor1<double,3> &scaled_normal)
{
  r=std::numeric_limits<double>::infinity();
  sign=0;
  intersects=false;
  line_intersects=false;
  
  FTensor::Index<'a',2> a;
  const double normal_dot_segment (dx(a)*scaled_normal(a));
  const double small_number (1e-100);
  if (std::abs(normal_dot_segment) > small_number)
    {
      r = -(scaled_normal(a)*xyz(a)) / normal_dot_segment;
      sign=boost::math::sign(normal_dot_segment);

      FTensor::Tensor1<double,3> w;
      w(a) = xyz(a) + r*dx(a);
      const double w_dot_u (w(a)*fault_segment(a)),
        u_dot_u(fault_segment(a)*fault_segment(a));
      const double s(w_dot_u/u_dot_u);
      line_intersects = (s>=0 && s<=1);
      intersects = (line_intersects && r>=0 && r<=1);
    }
}
