#pragma once

/// Copyright © 2016, 2017 California Institute of Technology

#include <FTensor.hpp>

#include "Dir.hxx"
#include "Affine_Parameters_2D.hxx"
#include "Affine_Parameters_3D.hxx"

namespace Elastic
{
  class Fault
  {
  public:
    FTensor::Tensor1<double,3> origin, jump, u, v, normal;
    bool is_triangle;

    /// Rectangular faults
    Fault (const double &scale,
           const FTensor::Tensor1<double,3> &Origin,
           const FTensor::Tensor1<double,2> &proportions,
           const double &strike, const double &dip, const double &rake) :
      origin(Origin), is_triangle(false)
    {
      const FTensor::Tensor2<double,3,3>
        rotation_strike(std::cos(strike),-std::sin(strike),0,
                        std::sin(strike),std::cos(strike),0,
                        0,0,1),
        rotation_dip(std::sin(dip),0,std::cos(dip),
                     0,1,0,
                     -std::cos(dip),0,std::sin(dip));

      FTensor::Index<'a',3> a;
      FTensor::Index<'b',3> b;
      FTensor::Index<'c',3> c;
      FTensor::Tensor2<double,3,3> rotation;
      rotation(a,b)=rotation_dip(a,c)*rotation_strike(c,b);

      FTensor::Tensor1<double,3> slip(0,scale,0);
      const FTensor::Tensor2<double,3,3>
        rotation_rake(1,0,0,
                      0,std::cos(rake),std::sin(rake),
                      0,-std::sin(rake),std::cos(rake));
      jump(c)=rotation(b,c)*rotation_rake(b,a)*slip(a);

      FTensor::Tensor1<double,3> normal_unrotated(1,0,0),
        u_unrotated (0,proportions(0),0), v_unrotated(0,0,proportions(1));
      u(a) = u_unrotated(b)*rotation(b,a);
      v(a) = v_unrotated(b)*rotation(b,a);
      normal(a) = normal_unrotated(b)*rotation(b,a);
    }

    /// Triangular faults.
    Fault (const FTensor::Tensor1<double,3> Origin,
           const FTensor::Tensor1<double,3> U,
           const FTensor::Tensor1<double,3> V,
           const FTensor::Tensor1<double,3> Jump) :
      origin(Origin), jump(Jump), u(U), v(V), is_triangle(true)
    {
      // FIXME: Make FTensor do the cross product
      normal(0) = u(1)*v(2) - u(2)*v(1);
      normal(1) = u(2)*v(0) - u(0)*v(2);
      normal(2) = u(0)*v(1) - u(1)*v(0);
    }

    double intersect_2D (const FTensor::Tensor1<double,3> &xyz,
                         const FTensor::Tensor1<double,3> &dx) const
    {
      const FTensor::Tensor1<double,3> scaled_normal (u(1),-u(0),0);
      Affine_Parameters_2D affine(xyz,dx,u,scaled_normal);
      return (affine.intersects ? affine.sign : 0);
    }
    
    double intersect_3D (const FTensor::Tensor1<double,3> &xyz,
                         const FTensor::Tensor1<double,3> &dx) const
    {
      Affine_Parameters_3D affine(xyz,dx,u,v,normal,is_triangle);
      return (affine.intersects ? affine.sign : 0);
    }
  };
}
