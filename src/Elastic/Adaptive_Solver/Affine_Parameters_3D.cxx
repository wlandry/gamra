#include "Affine_Parameters_3D.hxx"

#include <boost/math/special_functions/sign.hpp>

#include <limits>

void Elastic::Affine_Parameters_3D::init
(const FTensor::Tensor1<double,3> &xyz,
 const FTensor::Tensor1<double,3> &dx,
 const FTensor::Tensor1<double,3> &u,
 const FTensor::Tensor1<double,3> &v,
 const FTensor::Tensor1<double,3> &normal,
 const bool &is_triangle)
{
  r=std::numeric_limits<double>::infinity();
  sign=0;
  intersects=false;
  line_intersects=false;

  FTensor::Index<'a',3> a;
  const double normal_dot_segment (dx(a)*normal(a));
  const double small_number (1e-100);
  if (std::abs(normal_dot_segment) > small_number)
    {
      r = (-(normal(a)*xyz(a))/normal_dot_segment);
      sign = boost::math::sign(normal_dot_segment);

      FTensor::Tensor1<double,3> w;
      w(a) = xyz(a) + r*dx(a);
      const double u_dot_u (u(a)*u(a)),
        v_dot_v (v(a)*v(a)), w_dot_v (w(a)*v(a)), w_dot_u(w(a)*u(a));
      if (is_triangle)
        {
          const double u_dot_v (u(a)*v(a));
          const double denominator (u_dot_v*u_dot_v - u_dot_u*v_dot_v);
          const double s ((u_dot_v*w_dot_v - v_dot_v*w_dot_u)/denominator),
            t ((u_dot_v*w_dot_u - u_dot_u*w_dot_v)/denominator);
          line_intersects = (s>=0 && t>=0 && s+t<=1);
        }
      else
        {
          /// For box, u_dot_v == 0, so this expression is simpler
          /// than for triangles.
          const double denominator (-u_dot_u*v_dot_v);
          const double s ((-v_dot_v*w_dot_u)/denominator),
            t ((-u_dot_u*w_dot_v)/denominator);
          line_intersects = (s>=0 && s<=1 && t>=0 && t<=1);
        }
      intersects = (line_intersects && r>=0 && r<=1);
    }
}
