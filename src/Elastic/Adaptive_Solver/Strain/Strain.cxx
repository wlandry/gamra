/// Copyright © 2016-2018 California Institute of Technology

#include "../Strain.hxx"

/// rectangle
Elastic::Strain::Strain (const FTensor::Tensor2_symmetric<double,2>
                         &strain_relative_to_fault,
                         const FTensor::Tensor1<double,2> &Origin,
                         const FTensor::Tensor1<double,2> &proportions,
                         const double &strike) :
  is_tetrahedra(false)
{
  FTensor::Index<'i',3> i;
  FTensor::Index<'j',3> j;
  origin(i)=0;
  u(i)=0;
  v(i)=0;
  w(i)=0;
  strain(i,j)=0;

  const FTensor::Tensor2<double,2,2>
    rotation(std::cos(strike),-std::sin(strike),
             std::sin(strike),std::cos(strike));

  FTensor::Index<'a',2> a;
  FTensor::Index<'b',2> b;
  FTensor::Index<'c',2> c;
  FTensor::Index<'d',2> d;

  origin(a)=Origin(a);
      
  FTensor::Tensor1<double,2> u_unrotated (0,proportions(0)),
    w_unrotated (proportions(1),0);
  u(a) = u_unrotated(b)*rotation(b,a);
  w(a) = w_unrotated(b)*rotation(b,a);

  /// Offset the origin by half the thickness.
  origin(a) -= w(a)/2;
      
  strain(a,b)=(strain_relative_to_fault(c,d)*rotation(c,a))*rotation(d,b);
}

/// triangle
Elastic::Strain::Strain (const FTensor::Tensor2<double,2,2> &absolute_strain,
                         const FTensor::Tensor1<double,2> &vertex0,
                         const FTensor::Tensor1<double,2> &vertex1,
                         const FTensor::Tensor1<double,2> &vertex2) :
  is_tetrahedra(true)
{
  FTensor::Index<'i',3> i;
  FTensor::Index<'j',3> j;
  origin(i)=0;
  u(i)=0;
  v(i)=0;
  w(i)=0;
  strain(i,j)=0;

  FTensor::Index<'a',2> a;
  FTensor::Index<'b',2> b;
  origin(a)=vertex0(a);
  u(a) = vertex1(a)-origin(a);
  w(a) = vertex2(a)-origin(a);
  strain(a,b)=absolute_strain(a,b);
}

/// box
Elastic::Strain::Strain (const FTensor::Tensor2_symmetric<double,3> &strain_relative_to_fault,
                         const FTensor::Tensor1<double,3> &Origin,
                         const FTensor::Tensor1<double,3> &proportions,
                         const double &strike, const double &dip) :
  origin(Origin), is_tetrahedra(false)
{
  const FTensor::Tensor2<double,3,3>
    rotation_strike(std::cos(strike),-std::sin(strike),0,
                    std::sin(strike),std::cos(strike),0,
                    0,0,1),
    rotation_dip(std::sin(dip),0,std::cos(dip),
                 0,1,0,
                 -std::cos(dip),0,std::sin(dip));

  FTensor::Index<'a',3> a;
  FTensor::Index<'b',3> b;
  FTensor::Index<'c',3> c;
  FTensor::Index<'d',3> d;
  FTensor::Tensor2<double,3,3> rotation;
  rotation(a,b)=rotation_dip(a,c)*rotation_strike(c,b);

  FTensor::Tensor1<double,3> u_unrotated (0,proportions(0),0),
    v_unrotated(0,0,proportions(1)), w_unrotated (proportions(2),0,0);
  u(a) = u_unrotated(b)*rotation(b,a);
  v(a) = v_unrotated(b)*rotation(b,a);
  w(a) = w_unrotated(b)*rotation(b,a);

  /// Offset the origin by half the thickness.
  origin(a) -= w(a)/2;
      
  strain(a,b)=(strain_relative_to_fault(c,d)*rotation(c,a))*rotation(d,b);
}

/// tetrahedra
Elastic::Strain::Strain (const FTensor::Tensor2<double,3,3> &absolute_strain,
                         const FTensor::Tensor1<double,3> &vertex0,
                         const FTensor::Tensor1<double,3> &vertex1,
                         const FTensor::Tensor1<double,3> &vertex2,
                         const FTensor::Tensor1<double,3> &vertex3) :
  origin(vertex0), strain(absolute_strain), is_tetrahedra(true)
{
  FTensor::Index<'a',3> a;
  u(a)=vertex1(a)-vertex0(a);
  v(a)=vertex2(a)-vertex0(a);
  w(a)=vertex3(a)-vertex0(a);
}
