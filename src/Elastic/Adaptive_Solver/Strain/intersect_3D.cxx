/// Copyright © 2016, 2017 California Institute of Technology

#include "../Affine_Parameters_3D.hxx"

#include "../Strain.hxx"

double Elastic::Strain::intersect_3D
(const FTensor::Tensor1<double,3> &xyz,
 const FTensor::Tensor1<double,3> &dx) const
{
  /// Extra computation in some cases, but otherwise we end up
  /// repeating ourselves everywhere.
  double result (0);
  if (!is_tetrahedra)
    {
      Affine_Parameters_3D left(xyz,dx,u,v,w,is_tetrahedra),
        right(xyz,dx,u,v,w,w,is_tetrahedra),
        bottom(xyz,dx,v,w,u,is_tetrahedra),
        top(xyz,dx,v,w,u,u,is_tetrahedra),
        front(xyz,dx,w,u,v,is_tetrahedra),
        back(xyz,dx,w,u,v,v,is_tetrahedra);

      if (left.intersects)
        {
          double r (right.intersects ? right.r :
                    (bottom.intersects ? bottom.r :
                     (top.intersects ? top.r :
                      (front.intersects ? front.r :
                       (back.intersects ? back.r :
                        (right.r > left.r ? 1 : 0))))));
          result = r-left.r;
        }
      else if (right.intersects)
        {
          double r (bottom.intersects ? bottom.r :
                    (top.intersects ? top.r :
                     (front.intersects ? front.r :
                      (back.intersects ? back.r :
                       (right.r > left.r ? 0 : 1)))));
          result = right.r-r;
        }
      else if (bottom.intersects)
        {
          double r (top.intersects ? top.r :
                    (front.intersects ? front.r :
                     (back.intersects ? back.r :
                      (bottom.sign==1 ? 1 : 0))));
          result = r - bottom.r;
        }
      else if (top.intersects)
        {
          double r (front.intersects ? front.r :
                    (back.intersects ? back.r :
                     (bottom.sign==1 ? 0 : 1)));
          result = top.r - r;
        }
      else if (front.intersects)
        {
          double r (back.intersects ? back.r :
                    (back.sign==1 ? 1 : 0));
          result = r - front.r;
        }
      else if (back.intersects)
        {
          double r (back.sign==1 ? 0 : 1);
          result = back.r - r;
        }
      else if (((left.is_parallel() && (bottom.line_intersects
                                        || front.line_intersects))
                || ((left.r<=0 && right.r>=1) || (left.r>=1 && right.r<=0)))
               && ((bottom.is_parallel() && (left.line_intersects
                                             || front.line_intersects))
                   || ((bottom.r<=0 && top.r>=1) || (top.r<=0 && bottom.r>=1)))
               && ((front.is_parallel() && (left.line_intersects
                                            || bottom.line_intersects))
                   || ((front.r<=0 && back.r>=1) || (back.r<=0 && front.r>=1))))
        {
          result = 1;
        }
    }
  else
    {
      FTensor::Index<'a',3> a;
      FTensor::Index<'b',3> b;
      FTensor::Index<'c',3> c;
      FTensor::Tensor1<double,3> u_cross_v, v_cross_w, w_cross_u;
      u_cross_v(c)=FTensor::cross(u(a),v(b),c);
      v_cross_w(c)=FTensor::cross(v(a),w(b),c);
      w_cross_u(c)=FTensor::cross(w(a),u(b),c);
      
      FTensor::Tensor1<double,3> opposite_uv, opposite_wv, opposite_cross;
      opposite_uv(a)=u(a)-v(a);
      opposite_wv(a)=w(a)-v(a);
      opposite_cross(c)=FTensor::cross(opposite_uv(a),opposite_wv(b),c);

      Affine_Parameters_3D uv(xyz,dx,u,v,u_cross_v,is_tetrahedra),
        vw(xyz,dx,v,w,v_cross_w,is_tetrahedra),
        wu(xyz,dx,w,u,w_cross_u,is_tetrahedra),
        opposite(xyz,dx,opposite_uv,opposite_wv,opposite_cross,v,is_tetrahedra);

      if (uv.intersects)
        {
          double r (vw.intersects ? vw.r :
                    (wu.intersects ? wu.r :
                     (opposite.intersects ? opposite.r :
                      (vw.line_intersects ? (vw.r > uv.r ? 1 : 0) :
                       (wu.line_intersects ? (wu.r > uv.r ? 1 : 0) :
                        (opposite.r > uv.r ? 1 : 0))))));
          result = r-uv.r;
        }
      else if (vw.intersects)
        {
          double r (wu.intersects ? wu.r :
                    (opposite.intersects ? opposite.r :
                     (uv.line_intersects ? (uv.r > vw.r ? 1 : 0) :
                      (wu.line_intersects ? (wu.r > vw.r ? 1 : 0) :
                       (opposite.r > vw.r ? 1 : 0)))));
          result = r-vw.r;
        }
      else if (wu.intersects)
        {
          double r (opposite.intersects ? opposite.r :
                    (uv.line_intersects ? (uv.r > wu.r ? 1 : 0) :
                     (vw.line_intersects ? (vw.r > wu.r ? 1 : 0) :
                      (opposite.r > wu.r ? 1 : 0))));
          result = r-wu.r;
        }
      else if (opposite.intersects)
        {
          double r (uv.line_intersects ? (uv.r > opposite.r ? 1 : 0) :
                    (vw.line_intersects ? (vw.r > opposite.r ? 1 : 0) :
                     (wu.r > opposite.r ? 1 : 0)));
          result = r-opposite.r;
        }
      /// This could be optimized to use fewer comparisons.  It would
      /// make it a bit harder to follow.
      else if ((uv.line_intersects && vw.line_intersects
                && ((uv.r<=0 && vw.r>=1) || (uv.r>=1 && vw.r<=0)))
               || (vw.line_intersects && wu.line_intersects
                   && ((vw.r<=0 && wu.r>=1) || (vw.r>=1 && wu.r<=0)))
               || (wu.line_intersects && uv.line_intersects
                   && ((wu.r<=0 && uv.r>=1) || (wu.r>=1 && uv.r<=0)))
               || (uv.line_intersects && opposite.line_intersects
                   && ((uv.r<=0 && opposite.r>=1) || (uv.r>=1 && opposite.r<=0)))
               || (vw.line_intersects && opposite.line_intersects
                   && ((vw.r<=0 && opposite.r>=1) || (vw.r>=1 && opposite.r<=0)))
               || (wu.line_intersects && opposite.line_intersects
                   && ((wu.r<=0 && opposite.r>=1)
                       || (wu.r>=1 && opposite.r<=0))))
        {
          result = 1;
        }
    }
  return std::abs(result);
}

