/// Copyright © 2016, 2017 California Institute of Technology

#include "../Affine_Parameters_2D.hxx"
#include "../Strain.hxx"

#include <boost/math/special_functions/sign.hpp>

#include <cmath>

double Elastic::Strain::intersect_2D(const FTensor::Tensor1<double,3> &xyz,
                                     const FTensor::Tensor1<double,3> &dx) const
{
  double result (0);
  /// Extra computation in some cases, but otherwise we end up
  /// repeating ourselves everywhere.
  if(!is_tetrahedra)
    {
      Affine_Parameters_2D left(xyz,dx,u,w);
      Affine_Parameters_2D bottom(xyz,dx,w,u);
      Affine_Parameters_2D right(xyz,dx,u,w,w);
      Affine_Parameters_2D top(xyz,dx,w,u,u);

      if (left.intersects)
        {
          double r (right.intersects ? right.r :
                    (bottom.intersects ? bottom.r :
                     (top.intersects ? top.r :
                      (right.r > left.r ? 1 : 0))));
          result = r-left.r;
        }
      else if (right.intersects)
        {
          double r (bottom.intersects ? bottom.r :
                    (top.intersects ? top.r :
                     (right.r > left.r ? 0 : 1)));
          result = right.r-r;
        }
      else if (bottom.intersects)
        {
          double r (top.intersects ? top.r :
                    (bottom.sign==1 ? 1 : 0));
          result = r - bottom.r;
        }
      else if (top.intersects)
        {
          double r (bottom.sign==1 ? 0 : 1);
          result = top.r - r;
        }
      /// If it is parallel, then r=infinity.
      else if (((left.is_parallel() && bottom.line_intersects)
                || ((left.r<=0 && right.r>=1) || (left.r>=1 && right.r<=0)))
               && ((bottom.is_parallel() && left.line_intersects)
                   || ((bottom.r<=0 && top.r>=1) || (top.r<=0 && bottom.r>=1))))
        {
          result = 1;
        }
    }
  else
    {
      const FTensor::Index<'a',3> a;
      FTensor::Tensor1<double,3> opposite_side;
      opposite_side(a)=u(a)-w(a);
      FTensor::Tensor1<double,3> u_normal(u(1),-u(0),0),
        w_normal(w(1),-w(0),0), opposite_normal(opposite_side(1),
                                                -opposite_side(0),0);
      
      Affine_Parameters_2D left(xyz,dx,u,u_normal);
      Affine_Parameters_2D bottom(xyz,dx,w,w_normal);
      Affine_Parameters_2D opposite(xyz,dx,opposite_side,opposite_normal,w);

      if (left.intersects)
        {
          double r =
            (bottom.intersects ? bottom.r :
             (opposite.intersects ? opposite.r :
              (bottom.line_intersects ? ((bottom.r > left.r) ? 1 : 0) :
               ((opposite.r > left.r) ? 1 : 0))));
          result = r - left.r;
        }
      else if (bottom.intersects)
        {
          double r (opposite.intersects ? opposite.r :
                    (opposite.line_intersects ?
                     ((opposite.r > bottom.r) ? 1 : 0) :
                     ((left.r > bottom.r) ? 1 : 0)));
          result = r - bottom.r;
        }
      else if (opposite.intersects)
        {
          double r (left.line_intersects ? ((left.r > opposite.r) ? 1 : 0) :
                    ((bottom.r > opposite.r) ? 1 : 0));
          result = r-opposite.r;
        }
      else if ((left.line_intersects && bottom.line_intersects &&
                ((left.r<=0 && bottom.r>=1) || (left.r>=1 && bottom.r<=0)))
               || (left.line_intersects && opposite.line_intersects &&
                   ((left.r<=0 && opposite.r>=1)
                    || (left.r>=1 && opposite.r<=0)))
               || (bottom.line_intersects && opposite.line_intersects &&
                   ((bottom.r<=0 && opposite.r>=1)
                    || (bottom.r>=1 && opposite.r<=0))))
        {
          result = 1;
        }
    }
  return std::abs(result);
}
