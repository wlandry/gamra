#pragma once

#include <FTensor.hpp>

namespace Elastic
{
class Affine_Parameters_3D
{
public:
  Affine_Parameters_3D (const FTensor::Tensor1<double,3> &xyz,
                        const FTensor::Tensor1<double,3> &dx,
                        const FTensor::Tensor1<double,3> &u,
                        const FTensor::Tensor1<double,3> &v,
                        const FTensor::Tensor1<double,3> &normal,
                        const FTensor::Tensor1<double,3> &offset,
                        const bool &is_triangle)
  {
    FTensor::Index<'a',3> a;
    FTensor::Tensor1<double,3> xyz_offset;
    xyz_offset(a)=xyz(a)-offset(a);
    init(xyz_offset,dx,u,v,normal,is_triangle);
  }
  Affine_Parameters_3D (const FTensor::Tensor1<double,3> &xyz,
                        const FTensor::Tensor1<double,3> &dx,
                        const FTensor::Tensor1<double,3> &u,
                        const FTensor::Tensor1<double,3> &v,
                        const FTensor::Tensor1<double,3> &normal,
                        const bool &is_triangle)
  {
    init(xyz,dx,u,v,normal,is_triangle);
  }
  void init(const FTensor::Tensor1<double,3> &xyz,
            const FTensor::Tensor1<double,3> &dx,
            const FTensor::Tensor1<double,3> &u,
            const FTensor::Tensor1<double,3> &v,
            const FTensor::Tensor1<double,3> &normal,
            const bool &is_triangle);
  double r;
  int sign;
  bool intersects, line_intersects;
  bool is_parallel () const
  {
    return sign==0;
  }
};
}
