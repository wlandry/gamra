#pragma once

/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "Constants.hxx"
#include "Input_Expression.hxx"
#include "Geometry.hxx"
#include "Adaptive_Solver/Strain.hxx"
#include "Adaptive_Solver/Fault.hxx"

#include <SAMRAI/mesh/StandardTagAndInitStrategy.h>
#include <SAMRAI/geom/CartesianPatchGeometry.h>

#include <array>

namespace Elastic
{
  class Adaptive_Solver: public SAMRAI::mesh::StandardTagAndInitStrategy
  {
  public:
    Adaptive_Solver(const SAMRAI::tbox::Dimension& dim,
                    SAMRAI::tbox::Database &database);
    virtual ~Adaptive_Solver() {}

    virtual void
    initializeLevelData
    (const boost::shared_ptr<SAMRAI::hier::PatchHierarchy>& hierarchy,
     const int level_number,
     const double init_data_time,
     const bool can_be_refined,
     const bool initial_time,
     const boost::shared_ptr<SAMRAI::hier::PatchLevel>& old_level,
     const bool allocate_data);

    void initialize_cell_data
    (const SAMRAI::hier::Patch &patch,
     const boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
     const double *dx, const int &dim);
    void initialize_side_data
    (const SAMRAI::hier::Patch &patch,
     const boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
     const double *dx, const int &dim);
    void initialize_edge_data
    (const SAMRAI::hier::Patch &patch,
     const boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
     const double *dx, const int &dim);
    void initialize_node_data
    (const SAMRAI::hier::Patch &patch,
     const boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
     const double *dx, const int &dim);


    
    virtual void
    resetHierarchyConfiguration
    (const boost::shared_ptr<SAMRAI::hier::PatchHierarchy>& new_hierarchy,
     int coarsest_level,
     int finest_level);

    virtual void
    applyGradientDetector
    (const boost::shared_ptr<SAMRAI::hier::PatchHierarchy>& hierarchy,
     const int level_number,
     const double error_data_time,
     const int tag_index,
     const bool initial_time,
     const bool uses_richardson_extrapolation);

    bool solve_hierarchy();

    std::vector<std::string> default_output_fields() const
    {
      std::vector<std::string> result;
      result.push_back("Displacement");
      result.push_back("Induced Stress");
      return result;
    }
    
    SAMRAI::tbox::Database &database;
    const SAMRAI::tbox::Dimension dimension;

    void fix_moduli();
    boost::shared_ptr<SAMRAI::hier::PatchHierarchy> hierarchy;


    boost::shared_ptr<SAMRAI::hier::VariableContext> context;
    double d_adaption_threshold;
    int min_full_refinement_level;

    int cell_moduli_id{invalid_id}, edge_moduli_id{invalid_id},
      v_id{invalid_id}, v_rhs_id{invalid_id}, dv_diagonal_id{invalid_id},
      dv_mixed_id{invalid_id}, dstrain_diagonal_id{invalid_id},
      dstrain_mixed_id{invalid_id}, xyz_id{invalid_id},
      gg_id{invalid_id},
      christof_cell_id{invalid_id}, christof_edge_id{invalid_id};
    Input_Expression lambda, mu, topography;
    std::vector<Input_Expression> v_rhs_input, v_initial;
    Geometry geometry = Geometry::CARTESIAN;
    
    std::vector<Strain> strains;
    std::vector<Fault> faults;
    std::vector<double> refinement_points;
    
    bool have_jump_corrections() const
    {
      return !strains.empty() || !faults.empty();
    }
    void setup_fault_corrections();
  };
}
