#pragma once

/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "Input_Expression.hxx"
#include "Adaptive_Solver/Multigrid.hxx"

#include <SAMRAI/tbox/Database.h>
#include <SAMRAI/hier/Box.h>
#include <SAMRAI/solv/LocationIndexRobinBcCoefs.h>
#include <SAMRAI/hier/Patch.h>
#include <SAMRAI/hier/PatchHierarchy.h>
#include <SAMRAI/hier/PatchLevel.h>
#include <SAMRAI/mesh/StandardTagAndInitStrategy.h>
#include <SAMRAI/hier/VariableContext.h>
#include <SAMRAI/appu/VisDerivedDataStrategy.h>
#include <SAMRAI/appu/VisItDataWriter.h>

namespace Stokes
{
  class Adaptive_Solver:
    public SAMRAI::mesh::StandardTagAndInitStrategy
  {
  public:
    Adaptive_Solver(const SAMRAI::tbox::Dimension& dim,
                    SAMRAI::tbox::Database &database);
    virtual ~Adaptive_Solver() {}

    virtual void initializeLevelData
    (const boost::shared_ptr<SAMRAI::hier::PatchHierarchy>& hierarchy,
     const int level_number,
     const double init_data_time,
     const bool can_be_refined,
     const bool initial_time,
     const boost::shared_ptr<SAMRAI::hier::PatchLevel>& old_level,
     const bool allocate_data);

    virtual void resetHierarchyConfiguration
    (const boost::shared_ptr<SAMRAI::hier::PatchHierarchy>& new_hierarchy,
     const int coarsest_level,
     const int finest_level);
    
    virtual void applyGradientDetector
    (const boost::shared_ptr<SAMRAI::hier::PatchHierarchy> &hierarchy,
     const int level_number,
     const double error_data_time,
     const int tag_index,
     const bool initial_time,
     const bool uses_richardson_extrapolation);

    bool solve_hierarchy();

    std::vector<std::string> default_output_fields() const
    {
      std::vector<std::string> result;
      result.push_back("Velocity");
      result.push_back("Pressure");
      result.push_back("Deviatoric Stress");
      return result;
    }
    
    const SAMRAI::tbox::Dimension dimension;
  private:
    void fix_viscosity();

    boost::shared_ptr<SAMRAI::hier::PatchHierarchy> d_hierarchy;
    Stokes::Multigrid d_stokes_fac_solver;
    SAMRAI::solv::LocationIndexRobinBcCoefs d_bc_coefs;
    boost::shared_ptr<SAMRAI::hier::VariableContext> d_context;
    double d_adaption_threshold;
    int min_full_refinement_level;
  public:
    int p_id, cell_viscosity_id, edge_viscosity_id, dp_id, p_exact_id,
      p_rhs_id, v_id, v_rhs_id;

    std::vector<double> viscosity, viscosity_xyz_max, viscosity_xyz_min;
    std::vector<int> viscosity_ijk;

    std::vector<double> v_rhs, v_rhs_xyz_max, v_rhs_xyz_min;
    std::vector<int> v_rhs_ijk;

    std::vector<double> p_initial, p_initial_xyz_max, p_initial_xyz_min;
    std::vector<int> p_initial_ijk;
    /// FIXME: v_initial is not used.  This is just so that I do not
    /// need special code in Visit_Writer.
    std::vector<Input_Expression> v_initial;
  };
}


