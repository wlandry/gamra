#pragma once

/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <SAMRAI/solv/FACPreconditioner.h>
#include <SAMRAI/solv/FACOperatorStrategy.h>
#include <SAMRAI/tbox/Database.h>
#include <SAMRAI/tbox/Timer.h>
#include <SAMRAI/xfer/RefineSchedule.h>
#include <SAMRAI/xfer/CoarsenSchedule.h>

#include "Operators/P_Refine_Patch_Strategy.hxx"
#include "Operators/V_Refine_Patch_Strategy.hxx"
#include "Operators/V_Coarsen_Patch_Strategy.hxx"

namespace Stokes
{
  class Operators: public SAMRAI::solv::FACOperatorStrategy
  {

  public:
    Operators(
           const SAMRAI::tbox::Dimension& dim,
           const std::string& object_name = std::string(),
           boost::shared_ptr<SAMRAI::tbox::Database> database =
           boost::shared_ptr<SAMRAI::tbox::Database>());
    ~Operators(void) {}
    void enableLogging(bool enable_logging)
    {
      d_enable_logging = enable_logging;
    }
    void setCoarsestLevelSolverTolerance(double tol)
    {
      d_coarse_solver_tolerance = tol;
    }
    void setCoarsestLevelSolverMaxIterations(int max_iterations)
    {
      d_coarse_solver_max_iterations = max_iterations;
    }

    void setCoarseFineDiscretization(const std::string& coarsefine_method);

    void set_P_ProlongationMethod(const std::string& prolongation_method);

    void set_viscosity_dp_id(const int &cell_viscosity,
                             const int &edge_viscosity, const int &dp)
    {
      cell_viscosity_id=cell_viscosity;
      edge_viscosity_id=edge_viscosity;
      dp_id=dp;
    }
    void setPreconditioner(const SAMRAI::solv::FACPreconditioner* preconditioner)
    {
      d_preconditioner = preconditioner;
    }

    virtual void restrictSolution
    (const SAMRAI::solv::SAMRAIVectorReal<double>& source,
     SAMRAI::solv::SAMRAIVectorReal<double>& dest,
     int dest_level);
    virtual void restrictResidual
    (const SAMRAI::solv::SAMRAIVectorReal<double>& source,
     SAMRAI::solv::SAMRAIVectorReal<double>& dest,
     int dest_level);

    virtual void prolongErrorAndCorrect
    (const SAMRAI::solv::SAMRAIVectorReal<double>& source,
     SAMRAI::solv::SAMRAIVectorReal<double>& dest,
     int dest_level);

    virtual void smoothError
    (SAMRAI::solv::SAMRAIVectorReal<double>& error,
     const SAMRAI::solv::SAMRAIVectorReal<double>& residual,
     int level,
     int num_sweeps);

    virtual int solveCoarsestLevel
    (SAMRAI::solv::SAMRAIVectorReal<double>& error,
     const SAMRAI::solv::SAMRAIVectorReal<double>& residual,
     int coarsest_level);

    virtual void computeCompositeResidualOnLevel
    (SAMRAI::solv::SAMRAIVectorReal<double>& residual,
     const SAMRAI::solv::SAMRAIVectorReal<double>& solution,
     const SAMRAI::solv::SAMRAIVectorReal<double>& rhs,
     int level,
     bool error_equation_indicator);

    void residual_2D
    (SAMRAI::pdat::CellData<double> &p,
     SAMRAI::pdat::SideData<double> &v,
     SAMRAI::pdat::CellData<double> &cell_viscosity,
     SAMRAI::pdat::CellData<double> &p_rhs,
     SAMRAI::pdat::SideData<double> &v_rhs,
     SAMRAI::pdat::CellData<double> &p_resid,
     SAMRAI::pdat::SideData<double> &v_resid,
     SAMRAI::hier::Patch &patch,
     const SAMRAI::hier::Box &pbox,
     const SAMRAI::geom::CartesianPatchGeometry &geom);

    void residual_3D
    (SAMRAI::pdat::CellData<double> &p,
     SAMRAI::pdat::SideData<double> &v,
     SAMRAI::pdat::CellData<double> &cell_viscosity,
     SAMRAI::pdat::CellData<double> &p_rhs,
     SAMRAI::pdat::SideData<double> &v_rhs,
     SAMRAI::pdat::CellData<double> &p_resid,
     SAMRAI::pdat::SideData<double> &v_resid,
     SAMRAI::hier::Patch &patch,
     const SAMRAI::hier::Box &pbox,
     const SAMRAI::geom::CartesianPatchGeometry &geom);

    virtual double computeResidualNorm
    (const SAMRAI::solv::SAMRAIVectorReal<double>& residual,
     int fine_level,
     int coarse_level);

    virtual void initializeOperatorState
    (const SAMRAI::solv::SAMRAIVectorReal<double>& solution,
     const SAMRAI::solv::SAMRAIVectorReal<double>& rhs);

    virtual void deallocateOperatorState();

    virtual void postprocessOneCycle
    (int fac_cycle_num,
     const SAMRAI::solv::SAMRAIVectorReal<double>& current_soln,
     const SAMRAI::solv::SAMRAIVectorReal<double>& residual);

    void set_physical_boundaries(const int &p_id, const int &v_id, const int &l)
    {
      set_physical_boundaries(p_id,v_id,l,true);
    }
    void set_physical_boundaries(const int &p_id, const int &v_id, const int &l,
                                 const bool &rhs)
    {
      boost::shared_ptr<SAMRAI::hier::PatchLevel> level =
        d_hierarchy->getPatchLevel(l);
      set_physical_boundaries(p_id,v_id,level,rhs);
    }
    void set_physical_boundaries
    (const int &p_id, const int &v_id,
     boost::shared_ptr<SAMRAI::hier::PatchLevel> &level)
    {
      set_physical_boundaries(p_id,v_id,level,true);
    }
    void set_physical_boundaries
    (const int &p_id, const int &v_id, 
     boost::shared_ptr<SAMRAI::hier::PatchLevel> &level,
     const bool &rhs);

  private:
    void smooth_Tackley_2D
    (SAMRAI::solv::SAMRAIVectorReal<double>& error,
     const SAMRAI::solv::SAMRAIVectorReal<double>& residual,
     int level,
     int num_sweeps,
     double residual_tolerance = -1.0);

    void smooth_Tackley_3D
    (SAMRAI::solv::SAMRAIVectorReal<double>& error,
     const SAMRAI::solv::SAMRAIVectorReal<double>& residual,
     int level,
     int num_sweeps,
     double residual_tolerance = -1.0);

    void smooth_V_2D
    (const Gamra::Dir &axis,
     const SAMRAI::hier::Box &pbox,
     boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
     const SAMRAI::pdat::CellIndex &center,
     const SAMRAI::hier::Index &ip,
     const SAMRAI::hier::Index &jp,
     SAMRAI::pdat::CellData<double> &p,
     SAMRAI::pdat::SideData<double> &v,
     SAMRAI::pdat::SideData<double> &v_rhs,
     double &maxres,
     const double &dx,
     const double &dy,
     SAMRAI::pdat::CellData<double> &cell_viscosity,
     SAMRAI::pdat::EdgeData<double> &edge_viscosity,
     const double &theta_momentum);

    void smooth_V_3D
    (const Gamra::Dir &ix,
     const SAMRAI::hier::Box &pbox,
     boost::shared_ptr<SAMRAI::geom::CartesianPatchGeometry> &geom,
     SAMRAI::pdat::CellData<double> &p,
     SAMRAI::pdat::SideData<double> &v,
     SAMRAI::pdat::SideData<double> &v_rhs,
     SAMRAI::pdat::CellData<double> &cell_viscosity,
     SAMRAI::pdat::EdgeData<double> &edge_viscosity,
     const SAMRAI::pdat::CellIndex &center,
     const double dx[3],
     const double &theta_momentum,
     const SAMRAI::hier::Index pp[3],
     double &maxres);

    double dtau_mixed(SAMRAI::pdat::SideData<double> &v,
                      const SAMRAI::pdat::EdgeData<double> &edge_viscosity,
                      const SAMRAI::pdat::SideIndex &x,
                      const SAMRAI::pdat::SideIndex &y,
                      const SAMRAI::pdat::EdgeIndex &edge,
                      const SAMRAI::hier::Index &ip,
                      const SAMRAI::hier::Index &jp,
                      const double &dx,
                      const double &dy)
    {
      return ((v(x+jp)-v(x))/(dy*dy)
              + (v(y+jp)-v(y+jp-ip))/(dx*dy)) * edge_viscosity(edge+jp)
        - ((v(x)-v(x-jp))/(dy*dy)
           + (v(y)-v(y-ip))/(dx*dy)) * edge_viscosity(edge);
    }

    /// The action of the velocity operator. It is written from the
    /// perspective of vx, but pass in different values for center_x
    /// etc. to get vy.

    double v_operator_2D(SAMRAI::pdat::SideData<double> &v,
                         SAMRAI::pdat::CellData<double> &p,
                         SAMRAI::pdat::CellData<double> &cell_viscosity,
                         SAMRAI::pdat::EdgeData<double> &edge_viscosity,
                         const SAMRAI::pdat::CellIndex &center,
                         const SAMRAI::pdat::EdgeIndex &edge,
                         const SAMRAI::pdat::SideIndex &x,
                         const SAMRAI::pdat::SideIndex &y,
                         const SAMRAI::hier::Index &ip,
                         const SAMRAI::hier::Index &jp,
                         const double &dx,
                         const double &dy)
    {
      double dtau_xx_dx=2*((v(x+ip)-v(x))*cell_viscosity(center)
                           - (v(x)-v(x-ip))*cell_viscosity(center-ip))/(dx*dx);
      double dtau_xy_dy=dtau_mixed(v,edge_viscosity,x,y,edge,ip,jp,dx,dy);
      double dp_dx=(p(center)-p(center-ip))/dx;

      return dtau_xx_dx + dtau_xy_dy - dp_dx;
    }

    double v_operator_3D(SAMRAI::pdat::SideData<double> &v,
                         SAMRAI::pdat::CellData<double> &p,
                         SAMRAI::pdat::CellData<double> &cell_viscosity,
                         SAMRAI::pdat::EdgeData<double> &edge_viscosity,
                         const SAMRAI::pdat::CellIndex &center,
                         const SAMRAI::pdat::EdgeIndex &edge_y,
                         const SAMRAI::pdat::EdgeIndex &edge_z,
                         const SAMRAI::pdat::SideIndex &x,
                         const SAMRAI::pdat::SideIndex &y,
                         const SAMRAI::pdat::SideIndex &z,
                         const SAMRAI::hier::Index &ip,
                         const SAMRAI::hier::Index &jp,
                         const SAMRAI::hier::Index &kp,
                         const double &dx,
                         const double &dy,
                         const double &dz)
    {
      double dtau_xx_dx=2*((v(x+ip)-v(x))*cell_viscosity(center)
                           - (v(x)-v(x-ip))*cell_viscosity(center-ip))/(dx*dx);
      double dtau_xy_dy=dtau_mixed(v,edge_viscosity,x,y,edge_z,ip,jp,dx,dy);
      double dtau_xz_dz=dtau_mixed(v,edge_viscosity,x,z,edge_y,ip,kp,dx,dz);
      double dp_dx=(p(center)-p(center-ip))/dx;

      return dtau_xx_dx + dtau_xy_dy + dtau_xz_dz - dp_dx;
    }

    void
    redOrBlackSmoothingOnPatch(
                               const SAMRAI::hier::Patch& patch,
                               const SAMRAI::pdat::SideData<double>& flux_data,
                               const SAMRAI::pdat::CellData<double>& rhs_data,
                               SAMRAI::pdat::CellData<double>& soln_data,
                               char red_or_black,
                               double* p_maxres = NULL) const;

    void xeqScheduleProlongation(int p_dst, int p_src, int p_scr,
                                 int v_dst, int v_src, int v_scr,
                                 int dest_level);
    void xeqScheduleURestriction(int p_dst, int p_src, int v_dst, int v_src,
                                 int dest_level);
    void xeqScheduleRRestriction(int p_dst, int p_src, int v_dst, int v_src,
                                 int dest_level);
    void xeqScheduleGhostFill(int p_id, int v_id, int dest_level);
    void xeqScheduleGhostFillNoCoarse(int p_id, int v_id, int dest_level);
    static void finalizeCallback();

    const SAMRAI::tbox::Dimension d_dim;
    std::string d_object_name;
    boost::shared_ptr<SAMRAI::hier::PatchHierarchy> d_hierarchy;
    int d_level_min;
    int d_level_max;
    std::vector<boost::shared_ptr<SAMRAI::hier::CoarseFineBoundary> >
    d_cf_boundary;
    std::string d_smoothing_choice;
    std::string d_coarse_solver_choice;
    std::string p_rrestriction_method;
    double d_coarse_solver_tolerance;
    int d_coarse_solver_max_iterations;
    double d_residual_tolerance_during_smoothing;
    int cell_viscosity_id, edge_viscosity_id, dp_id;

    static boost::shared_ptr<SAMRAI::pdat::CellVariable<double> >
    s_cell_scratch_var[SAMRAI::MAX_DIM_VAL];

    static boost::shared_ptr<SAMRAI::pdat::SideVariable<double> >
    s_side_scratch_var[SAMRAI::MAX_DIM_VAL];

    boost::shared_ptr<SAMRAI::hier::VariableContext> d_context;

    int d_cell_scratch_id, d_side_scratch_id;

    boost::shared_ptr<SAMRAI::hier::RefineOperator>
    p_prolongation_refine_operator;
    std::vector<boost::shared_ptr<SAMRAI::xfer::RefineSchedule> >
    p_prolongation_refine_schedules;

    boost::shared_ptr<SAMRAI::hier::RefineOperator>
    v_prolongation_refine_operator;
    std::vector<boost::shared_ptr<SAMRAI::xfer::RefineSchedule> >
    v_prolongation_refine_schedules;

    boost::shared_ptr<SAMRAI::hier::CoarsenOperator>
    p_urestriction_coarsen_operator;
    std::vector<boost::shared_ptr<SAMRAI::xfer::CoarsenSchedule> >
    p_urestriction_coarsen_schedules;

    boost::shared_ptr<SAMRAI::hier::CoarsenOperator>
    v_urestriction_coarsen_operator;
    std::vector<boost::shared_ptr<SAMRAI::xfer::CoarsenSchedule> >
    v_urestriction_coarsen_schedules;

    boost::shared_ptr<SAMRAI::hier::CoarsenOperator>
    p_rrestriction_coarsen_operator;
    std::vector<boost::shared_ptr<SAMRAI::xfer::CoarsenSchedule> >
    p_rrestriction_coarsen_schedules;

    boost::shared_ptr<SAMRAI::hier::CoarsenOperator>
    v_rrestriction_coarsen_operator;
    std::vector<boost::shared_ptr<SAMRAI::xfer::CoarsenSchedule> >
    v_rrestriction_coarsen_schedules;

    boost::shared_ptr<SAMRAI::hier::RefineOperator> p_ghostfill_refine_operator;
    std::vector<boost::shared_ptr<SAMRAI::xfer::RefineSchedule> >
    p_ghostfill_refine_schedules;

    boost::shared_ptr<SAMRAI::hier::RefineOperator> v_ghostfill_refine_operator;
    std::vector<boost::shared_ptr<SAMRAI::xfer::RefineSchedule> >
    v_ghostfill_refine_schedules;

    std::vector<boost::shared_ptr<SAMRAI::xfer::RefineSchedule> >
    p_nocoarse_refine_schedules;

    std::vector<boost::shared_ptr<SAMRAI::xfer::RefineSchedule> >
    v_nocoarse_refine_schedules;

    P_Refine_Patch_Strategy p_refine_patch_strategy;
    V_Refine_Patch_Strategy v_refine_patch_strategy;
    V_Coarsen_Patch_Strategy v_coarsen_patch_strategy;

    bool d_enable_logging;
    const SAMRAI::solv::FACPreconditioner* d_preconditioner;

    boost::shared_ptr<SAMRAI::tbox::Timer> t_restrict_solution;
    boost::shared_ptr<SAMRAI::tbox::Timer> t_restrict_residual;
    boost::shared_ptr<SAMRAI::tbox::Timer> t_prolong;
    boost::shared_ptr<SAMRAI::tbox::Timer> t_smooth_error;
    boost::shared_ptr<SAMRAI::tbox::Timer> t_solve_coarsest;
    boost::shared_ptr<SAMRAI::tbox::Timer> t_compute_composite_residual;
    boost::shared_ptr<SAMRAI::tbox::Timer> t_compute_residual_norm;

    static SAMRAI::tbox::StartupShutdownManager::Handler
    s_finalize_handler;
  };

}


