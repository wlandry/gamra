/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <SAMRAI/geom/CartesianGridGeometry.h>
#include <SAMRAI/xfer/PatchLevelFullFillPattern.h>
#include <SAMRAI/xfer/CoarsenAlgorithm.h>

#include "../../Operators.hxx"
#include "P_Refine.hxx"
#include "V_Refine.hxx"
#include "P_Boundary_Refine.hxx"
#include "V_Boundary_Refine.hxx"
#include "V_Coarsen.hxx"
#include "Resid_Coarsen.hxx"

void Stokes::Operators::initializeOperatorState
(const SAMRAI::solv::SAMRAIVectorReal<double>& solution,
 const SAMRAI::solv::SAMRAIVectorReal<double>& rhs)
{
  deallocateOperatorState();
  int level;
  SAMRAI::hier::VariableDatabase* vdb =
    SAMRAI::hier::VariableDatabase::getDatabase();

  d_hierarchy = solution.getPatchHierarchy();
  d_level_min = solution.getCoarsestLevelNumber();
  d_level_max = solution.getFinestLevelNumber();

  d_cf_boundary.resize(d_hierarchy->getNumberOfLevels());

  SAMRAI::hier::IntVector max_gcw(d_dim, 1);
  for (level = d_level_min; level <= d_level_max; ++level)
    {
      d_cf_boundary[level] = boost::make_shared<SAMRAI::hier::CoarseFineBoundary>
        (*d_hierarchy,level,max_gcw);
    }

  v_coarsen_patch_strategy.coarse_fine=d_cf_boundary;

  boost::shared_ptr<SAMRAI::geom::CartesianGridGeometry> geometry =
    boost::dynamic_pointer_cast<SAMRAI::geom::CartesianGridGeometry>
    (d_hierarchy->getGridGeometry());
  boost::shared_ptr<SAMRAI::hier::Variable> variable;

  vdb->mapIndexToVariable(d_cell_scratch_id, variable);
  p_prolongation_refine_operator =
    boost::shared_ptr<SAMRAI::hier::RefineOperator>(new Stokes::P_Refine());

  vdb->mapIndexToVariable(d_side_scratch_id, variable);
  v_prolongation_refine_operator =
    boost::shared_ptr<SAMRAI::hier::RefineOperator>(new Stokes::V_Refine());

  vdb->mapIndexToVariable(d_cell_scratch_id, variable);
  p_urestriction_coarsen_operator =
    geometry->lookupCoarsenOperator(variable,
                                    "CONSERVATIVE_COARSEN");
  p_rrestriction_coarsen_operator =
    boost::shared_ptr<SAMRAI::hier::CoarsenOperator>
    (new Stokes::Resid_Coarsen(cell_viscosity_id));

  vdb->mapIndexToVariable(d_side_scratch_id, variable);
  v_urestriction_coarsen_operator =
    v_rrestriction_coarsen_operator =
    boost::shared_ptr<SAMRAI::hier::CoarsenOperator> (new Stokes::V_Coarsen());

  vdb->mapIndexToVariable(d_cell_scratch_id, variable);
  p_ghostfill_refine_operator = boost::shared_ptr<SAMRAI::hier::RefineOperator>
    (new Stokes::P_Boundary_Refine());

  vdb->mapIndexToVariable(d_side_scratch_id, variable);
  v_ghostfill_refine_operator = boost::shared_ptr<SAMRAI::hier::RefineOperator>
    (new Stokes::V_Boundary_Refine());

  /// Make space for saving communication schedules.  There is no need
  /// to delete the old schedules first because we have deallocated
  /// the solver state above.
   
  p_prolongation_refine_schedules.resize(d_level_max + 1);
  v_prolongation_refine_schedules.resize(d_level_max + 1);
  p_ghostfill_refine_schedules.resize(d_level_max + 1);
  v_ghostfill_refine_schedules.resize(d_level_max + 1);
  p_nocoarse_refine_schedules.resize(d_level_max + 1);
  v_nocoarse_refine_schedules.resize(d_level_max + 1);
  p_urestriction_coarsen_schedules.resize(d_level_max + 1);
  p_rrestriction_coarsen_schedules.resize(d_level_max + 1);
  v_urestriction_coarsen_schedules.resize(d_level_max + 1);
  v_rrestriction_coarsen_schedules.resize(d_level_max + 1);

  SAMRAI::xfer::RefineAlgorithm p_prolongation_refine_algorithm,
    v_prolongation_refine_algorithm,
    p_ghostfill_refine_algorithm,
    v_ghostfill_refine_algorithm,
    p_nocoarse_refine_algorithm,
    v_nocoarse_refine_algorithm;
  SAMRAI::xfer::CoarsenAlgorithm p_urestriction_coarsen_algorithm(d_dim),
    p_rrestriction_coarsen_algorithm(d_dim),
    v_urestriction_coarsen_algorithm(d_dim),
    v_rrestriction_coarsen_algorithm(d_dim);

  /// This is a little confusing.  The only real purpose here is to
  /// create a communication schedule.  That communication schedule is
  /// then reused later when refining, though with a different source,
  /// scratch, and destination.  So the arguments to registerRefine
  /// are not all that important, because a different refineAlgorithm
  /// will be used then.

  p_prolongation_refine_algorithm.
    registerRefine(d_cell_scratch_id,
                   solution.getComponentDescriptorIndex(0),
                   d_cell_scratch_id,
                   p_prolongation_refine_operator);
  v_prolongation_refine_algorithm.
    registerRefine(d_side_scratch_id,
                   solution.getComponentDescriptorIndex(1),
                   d_side_scratch_id,
                   v_prolongation_refine_operator);
  p_urestriction_coarsen_algorithm.
    registerCoarsen(solution.getComponentDescriptorIndex(0),
                    solution.getComponentDescriptorIndex(0),
                    p_urestriction_coarsen_operator);
  p_rrestriction_coarsen_algorithm.
    registerCoarsen(rhs.getComponentDescriptorIndex(0),
                    rhs.getComponentDescriptorIndex(0),
                    p_rrestriction_coarsen_operator);
  v_urestriction_coarsen_algorithm.
    registerCoarsen(solution.getComponentDescriptorIndex(1),
                    solution.getComponentDescriptorIndex(1),
                    v_urestriction_coarsen_operator);
  v_rrestriction_coarsen_algorithm.
    registerCoarsen(rhs.getComponentDescriptorIndex(1),
                    rhs.getComponentDescriptorIndex(1),
                    v_rrestriction_coarsen_operator);
  p_ghostfill_refine_algorithm.
    registerRefine(solution.getComponentDescriptorIndex(0),
                   solution.getComponentDescriptorIndex(0),
                   solution.getComponentDescriptorIndex(0),
                   p_ghostfill_refine_operator);
  v_ghostfill_refine_algorithm.
    registerRefine(solution.getComponentDescriptorIndex(1),
                   solution.getComponentDescriptorIndex(1),
                   solution.getComponentDescriptorIndex(1),
                   v_ghostfill_refine_operator);
  p_nocoarse_refine_algorithm.
    registerRefine(solution.getComponentDescriptorIndex(0),
                   solution.getComponentDescriptorIndex(0),
                   solution.getComponentDescriptorIndex(0),
                   boost::shared_ptr<SAMRAI::hier::RefineOperator>());
  v_nocoarse_refine_algorithm.
    registerRefine(solution.getComponentDescriptorIndex(1),
                   solution.getComponentDescriptorIndex(1),
                   solution.getComponentDescriptorIndex(1),
                   boost::shared_ptr<SAMRAI::hier::RefineOperator>());

  /// Refinement and ghost fill operators
  for (int dest_level = d_level_min + 1; dest_level <= d_level_max;
       ++dest_level)
    {
      boost::shared_ptr<SAMRAI::xfer::PatchLevelFullFillPattern>
        fill_pattern(new SAMRAI::xfer::PatchLevelFullFillPattern());
      p_prolongation_refine_schedules[dest_level] =
        p_prolongation_refine_algorithm.
        createSchedule(fill_pattern,
                       d_hierarchy->getPatchLevel(dest_level),
                       boost::shared_ptr<SAMRAI::hier::PatchLevel>(),
                       dest_level - 1,
                       d_hierarchy);
      if (!p_prolongation_refine_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a refine schedule for p "
                     "prolongation!\n"); }
      v_prolongation_refine_schedules[dest_level] =
        v_prolongation_refine_algorithm.
        createSchedule(fill_pattern,
                       d_hierarchy->getPatchLevel(dest_level),
                       boost::shared_ptr<SAMRAI::hier::PatchLevel>(),
                       dest_level - 1,
                       d_hierarchy);
      if (!v_prolongation_refine_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a refine schedule for v "
                     "prolongation!\n"); }
      p_ghostfill_refine_schedules[dest_level] =
        p_ghostfill_refine_algorithm.
        createSchedule(d_hierarchy->getPatchLevel(dest_level),
                       dest_level - 1,
                       d_hierarchy,
                       &p_refine_patch_strategy);
      if (!p_ghostfill_refine_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a refine schedule for ghost "
                     "filling!\n"); }
      v_ghostfill_refine_schedules[dest_level] =
        v_ghostfill_refine_algorithm.
        createSchedule(d_hierarchy->getPatchLevel(dest_level),
                       dest_level - 1,
                       d_hierarchy,
                       &v_refine_patch_strategy);
      if (!v_ghostfill_refine_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a refine schedule for ghost "
                     "filling!\n"); }
      p_nocoarse_refine_schedules[dest_level] =
        p_nocoarse_refine_algorithm.
        createSchedule(d_hierarchy->getPatchLevel(dest_level));
      if (!p_nocoarse_refine_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a refine schedule for ghost filling "
                     "on bottom level!\n"); }
      v_nocoarse_refine_schedules[dest_level] =
        v_nocoarse_refine_algorithm.
        createSchedule(d_hierarchy->getPatchLevel(dest_level));
      if (!v_nocoarse_refine_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a refine schedule for ghost filling "
                     "on bottom level!\n"); }
    }

  /// Coarsening operators
  for (int dest_level = d_level_min; dest_level < d_level_max; ++dest_level)
    {
      p_urestriction_coarsen_schedules[dest_level] =
        p_urestriction_coarsen_algorithm.
        createSchedule(d_hierarchy->getPatchLevel(dest_level),
                       d_hierarchy->getPatchLevel(dest_level + 1));
      if (!p_urestriction_coarsen_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a coarsen schedule for U p "
                     "restriction!\n"); }
      p_rrestriction_coarsen_schedules[dest_level] =
        p_rrestriction_coarsen_algorithm.
        createSchedule(d_hierarchy->getPatchLevel(dest_level),
                       d_hierarchy->getPatchLevel(dest_level + 1));
      if (!p_rrestriction_coarsen_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a coarsen schedule for R p "
                     "restriction!\n"); }

      v_urestriction_coarsen_schedules[dest_level] =
        v_urestriction_coarsen_algorithm.
        createSchedule(d_hierarchy->getPatchLevel(dest_level),
                       d_hierarchy->getPatchLevel(dest_level + 1),
                       &v_coarsen_patch_strategy);
      if (!v_urestriction_coarsen_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a coarsen schedule for U v "
                     "restriction!\n"); }
      v_rrestriction_coarsen_schedules[dest_level] =
        v_rrestriction_coarsen_algorithm.
        createSchedule(d_hierarchy->getPatchLevel(dest_level),
                       d_hierarchy->getPatchLevel(dest_level + 1),
                       &v_coarsen_patch_strategy);
      if (!v_rrestriction_coarsen_schedules[dest_level])
        { TBOX_ERROR(d_object_name
                     << ": Cannot create a coarsen schedule for R v "
                     "restriction!\n"); }
    }

  /// Ordinary ghost fill operator on the coarsest level
  p_nocoarse_refine_schedules[d_level_min] =
    p_nocoarse_refine_algorithm.
    createSchedule(d_hierarchy->getPatchLevel(d_level_min));
  if (!p_nocoarse_refine_schedules[d_level_min])
    { TBOX_ERROR(d_object_name
               << ": Cannot create a refine schedule for p ghost filling "
                 "on bottom level!\n"); }
  v_nocoarse_refine_schedules[d_level_min] =
    v_nocoarse_refine_algorithm.
    createSchedule(d_hierarchy->getPatchLevel(d_level_min));
  if (!v_nocoarse_refine_schedules[d_level_min])
    { TBOX_ERROR(d_object_name
               << ": Cannot create a refine schedule for v ghost filling on "
                 "bottom level!\n"); }
}
