#pragma once

/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <SAMRAI/pdat/CellData.h>
#include <SAMRAI/pdat/EdgeData.h>
#include <SAMRAI/pdat/SideData.h>
#include "dRm_dv.hxx"
#include "Constants.hxx"

/// The derivative of the continuity equation with respect to
/// pressure.  Note that pressure does not appear in the continuity
/// equation, so we use Tackley's method to chain together derivatives

namespace Stokes
{
  inline double dRc_dp_2D(const SAMRAI::hier::Box &pbox,
                          const SAMRAI::pdat::CellIndex &center,
                          SAMRAI::pdat::CellData<double> &cell_viscosity,
                          SAMRAI::pdat::EdgeData<double> &edge_viscosity,
                          SAMRAI::pdat::SideData<double> &v,
                          const double Dx[2],
                          const SAMRAI::hier::Index pp[2])
  {
    double result(0);
    for(Gamra::Dir ix=0;ix<2;++ix)
      {
        const Gamra::Dir iy(ix.next(2));
        const SAMRAI::pdat::SideIndex
          x(center,ix,SAMRAI::pdat::SideIndex::Lower);
        const SAMRAI::pdat::EdgeIndex
          center_y(center,iy,SAMRAI::pdat::EdgeIndex::LowerLeft),
          up_y(center_y+pp[iy]);

        if(!(center[ix]==pbox.lower(ix) && v(x-pp[ix])==boundary_value))
          {
            result+=-1.0/(dRm_dv_2D(cell_viscosity,edge_viscosity,
                                    center,center-pp[ix],up_y,center_y,
                                    Dx[ix],Dx[iy])
                          * Dx[ix]*Dx[ix]);
          }

        if(!(center[ix]==pbox.upper(ix) && v(x+pp[ix]*2)==boundary_value))
          {
            result+=-1.0/(dRm_dv_2D(cell_viscosity,edge_viscosity,
                                    center+pp[ix],center,
                                    up_y+pp[ix],center_y+pp[ix],
                                    Dx[ix],Dx[iy])
                          * Dx[ix]*Dx[ix]);
          }
      }
    return result;
  }


  inline double dRc_dp_3D(const SAMRAI::hier::Box &pbox,
                          const SAMRAI::pdat::CellIndex &center,
                          SAMRAI::pdat::CellData<double> &cell_viscosity,
                          SAMRAI::pdat::EdgeData<double> &edge_viscosity,
                          SAMRAI::pdat::SideData<double> &v,
                          const double Dx[3],
                          const SAMRAI::hier::Index pp[3])
  {
    double result(0);
    for(Gamra::Dir ix=0;ix<3;++ix)
      {
        const Gamra::Dir iy(ix.next(3));
        const Gamra::Dir iz(iy.next(3));
        const SAMRAI::pdat::SideIndex
          x(center,ix,SAMRAI::pdat::SideIndex::Lower);
        const SAMRAI::pdat::EdgeIndex
          center_y(center,iy,SAMRAI::pdat::EdgeIndex::LowerLeft),
          front_y(center_y+pp[iz]),
          center_z(center,iz,SAMRAI::pdat::EdgeIndex::LowerLeft),
          up_z(center_z+pp[iy]);

        if(!(center[ix]==pbox.lower(ix) && v(x-pp[ix])==boundary_value))
          {
            result+=-1.0/(dRm_dv_3D(cell_viscosity,edge_viscosity,
                                    center,center-pp[ix],front_y,center_y,
                                    up_z,center_z,Dx[ix],Dx[iy],Dx[iz])
                          * Dx[ix]*Dx[ix]);
          }

        if(!(center[ix]==pbox.upper(ix) && v(x+pp[ix]*2)==boundary_value))
          {
            result+=-1.0/(dRm_dv_3D(cell_viscosity,edge_viscosity,
                                    center+pp[ix],center,
                                    front_y+pp[ix],center_y+pp[ix],
                                    up_z+pp[ix],center_z+pp[ix],
                                    Dx[ix],Dx[iy],Dx[iz])
                          * Dx[ix]*Dx[ix]);
          }
      }
    return result;
  }
}
