/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2017 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include "../Operators.hxx"

void Stokes::Operators::deallocateOperatorState()
{
  if (d_hierarchy) {
    d_cf_boundary.clear();
    d_hierarchy.reset();
    d_level_min = -1;
    d_level_max = -1;

    p_prolongation_refine_schedules.clear();
    v_prolongation_refine_schedules.clear();
    p_urestriction_coarsen_schedules.clear();
    v_urestriction_coarsen_schedules.clear();
    p_rrestriction_coarsen_schedules.clear();
    v_rrestriction_coarsen_schedules.clear();
    p_ghostfill_refine_schedules.clear();
    v_ghostfill_refine_schedules.clear();
    p_nocoarse_refine_schedules.clear();
    v_nocoarse_refine_schedules.clear();
  }
}
