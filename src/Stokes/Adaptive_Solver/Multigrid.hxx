#pragma once

/// Copyright © 1997-2010 Lawrence Livermore National Security, LLC
/// Copyright © 2013-2016 California Institute of Technology
/// Copyright © 2013-2016 Nanyang Technical University

#include <SAMRAI/solv/FACPreconditioner.h>
#include <SAMRAI/solv/SimpleCellRobinBcCoefs.h>
#include <SAMRAI/tbox/Database.h>

#include "Multigrid/Operators.hxx"

namespace Stokes
{
  class Multigrid
  {
  public:
    Multigrid(const SAMRAI::tbox::Dimension& dim,
              const std::string& object_name,
              boost::shared_ptr<SAMRAI::tbox::Database> database =
              boost::shared_ptr<SAMRAI::tbox::Database>());
    ~Multigrid(void);

    void enableLogging(bool logging);
    bool solveSystem(const int p,
                     const int cell_viscosity,
                     const int edge_viscosity,
                     const int dp,
                     const int p_rhs,
                     const int v,
                     const int v_rhs,
                     boost::shared_ptr<SAMRAI::hier::PatchHierarchy> hierarchy,
                     int coarse_level = -1,
                     int fine_level = -1);
    bool solveSystem(const int p, const int p_rhs, const int v, const int v_rhs);
    void setBoundaries(const std::string& boundary_type,
                       const int fluxes = -1,
                       const int flags = -1,
                       int* bdry_types = NULL);
    void setBcObject(const SAMRAI::solv::RobinBcCoefStrategy* bc_object);

    void setDPatchDataId(int id);
    void setDConstant(double scalar);
    void setCPatchDataId(int id);
    void setCConstant(double scalar);
    void setCoarsestLevelSolverTolerance(double tol)
    {
      d_fac_ops->setCoarsestLevelSolverTolerance(tol);
    }
    void setCoarsestLevelSolverMaxIterations(int max_iterations)
    {
      d_fac_ops->setCoarsestLevelSolverMaxIterations(max_iterations);
    }
    void set_P_ProlongationMethod(const std::string& prolongation_method);
    void initializeSolverState
    (const int p,
     const int cell_viscosity,
     const int edge_viscosity,
     const int dp,
     const int p_rhs,
     const int v,
     const int v_rhs,
     boost::shared_ptr<SAMRAI::hier::PatchHierarchy> hierarchy,
     const int coarse_level = -1,
     const int fine_level = -1);

    void deallocateSolverState();

    int getNumberOfIterations() const
    {
      return d_fac_precond.getNumberOfIterations();
    }

    void getConvergenceFactors(double& avg_factor,
                                          double& final_factor)
      const
    {
      d_fac_precond.getConvergenceFactors(avg_factor, final_factor);
    }
    double getResidualNorm() const
    {
      return d_fac_precond.getResidualNorm();
    }

    void set_physical_boundaries
    (const int &p_id, const int &v_id,
     boost::shared_ptr<SAMRAI::hier::PatchLevel> &level,
     const bool &is_homogeneous)
    {
      d_fac_ops->set_physical_boundaries(p_id,v_id,level,is_homogeneous);
    }

  private:
    void getFromInput(boost::shared_ptr<SAMRAI::tbox::Database> database);
    void createVectorWrappers(int p, int p_rhs, int v, int v_rhs);
    void destroyVectorWrappers();
    const SAMRAI::tbox::Dimension d_dim;
    std::string d_object_name;
    boost::shared_ptr<Operators> d_fac_ops;
    SAMRAI::solv::FACPreconditioner d_fac_precond;
    const SAMRAI::solv::RobinBcCoefStrategy* d_bc_object;
    SAMRAI::solv::SimpleCellRobinBcCoefs d_simple_bc;

    boost::shared_ptr<SAMRAI::hier::PatchHierarchy> d_hierarchy;
    int d_level_min;
    int d_level_max;

    boost::shared_ptr<SAMRAI::hier::VariableContext> d_context;
    boost::shared_ptr<SAMRAI::solv::SAMRAIVectorReal<double> > d_uv;
    boost::shared_ptr<SAMRAI::solv::SAMRAIVectorReal<double> > d_fv;

    bool d_solver_is_initialized;
    bool d_enable_logging;
  };
}


