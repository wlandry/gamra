Main {
    dim = 3
    base_name = "strain3D_analytic_cuboid"
    log_all_nodes = FALSE
    intermediate_output = TRUE
    offset_vector_on_output=TRUE
    output_fields="Displacement", "Initial Displacement", "Fault Correction + RHS","Induced Strain","Applied Strain","Induced Stress"
}

Elastic {
    adaption_threshold = 1.0e-3
    min_full_refinement_level = 1

    lambda="1.5"
    mu="1.6"
    v_rhs_x="0"
    v_rhs_y="0"
    v_rhs_z="0"

    v_initial_x="strain_volume(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0)"
    v_initial_y="strain_volume(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 1)"
    v_initial_z="strain_volume(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 2)"

    // This angle makes a triangle with side of 3:4:5.
    strains= 0,-1600,0,0,0,0, -.2001, -.2001, 0.0001, 0.5, 0.25, 0.08, 36.8698976459, 90

    fac_solver {
        enable_logging = TRUE   // Bool flag to switch logging on/off
        max_cycles = 40         // Max number of FAC cycles to use
        residual_tol = 1e-3     // Residual tolerance to solve for
        num_pre_sweeps =  4      // Number of presmoothing sweeps to use
        num_post_sweeps =  4     // Number of postsmoothing sweeps to use
        coarse_solver_max_iterations =  32
        coarse_solver_tolerance = 1e-12
    }
    boundary_conditions {
        vx_x_upper="strain_volume(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0)"
        vx_x_upper_is_dirichlet=TRUE
        vx_x_lower="strain_volume(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0)"
        vx_x_lower_is_dirichlet=TRUE

        vy_y_upper="strain_volume(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 1)"
        vy_y_upper_is_dirichlet=TRUE
        vy_y_lower="strain_volume(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 1)"
        vy_y_lower_is_dirichlet=TRUE

        vz_z_upper="strain_volume(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 2)"
        vz_z_upper_is_dirichlet=TRUE
        vz_z_lower="strain_volume(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 2)"
        vz_z_lower_is_dirichlet=TRUE

        vx_y_upper="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0, 1)"
        vx_y_upper_is_dirichlet=FALSE
        vx_y_lower="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0, 1)"
        vx_y_lower_is_dirichlet=FALSE
        vx_z_upper="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0, 2)"
        vx_z_upper_is_dirichlet=FALSE
        vx_z_lower="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0, 2)"
        vx_z_lower_is_dirichlet=FALSE

        vy_x_upper="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0, 1)"
        vy_x_upper_is_dirichlet=FALSE
        vy_x_lower="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0, 1)"
        vy_x_lower_is_dirichlet=FALSE
        vy_z_upper="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 1, 2)"
        vy_z_upper_is_dirichlet=FALSE
        vy_z_lower="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 1, 2)"
        vy_z_lower_is_dirichlet=FALSE

        vz_x_upper="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0, 2)"
        vz_x_upper_is_dirichlet=FALSE
        vz_x_lower="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 0, 2)"
        vz_x_lower_is_dirichlet=FALSE
        vz_y_upper="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 1, 2)"
        vz_y_upper_is_dirichlet=FALSE
        vz_y_lower="strain_volume_strain(-.2001, -.2001, 0.0001, 0.5, 0.08, 0.25, 36.8698976459, 0,1600,0,0,0,0, 1.5, 1.6, x,y,z, 1, 2)"
        vz_y_lower_is_dirichlet=FALSE
    }
}

CartesianGridGeometry {
    domain_boxes = [(0,0,0), (3,3,3)]
    x_lo         = -0.8, -0.8, 0.
    x_up         = 0.8, 0.8, 1.6
}

StandardTagAndInitialize {
    tagging_method = "GRADIENT_DETECTOR"
}

PatchHierarchy {
    max_levels = 5
    proper_nesting_buffer = 2, 2, 2, 2, 2, 2
    ratio_to_coarser {
        level_1            = 2, 2, 2
        level_2            = 2, 2, 2
        level_3            = 2, 2, 2
        level_4            = 2, 2, 2
    }
    largest_patch_size {
        level_0 = -1, -1, -1
    }
    smallest_patch_size {
        level_0 = 4, 4, 4
    }
}

GriddingAlgorithm {
    enforce_proper_nesting = TRUE
    extend_to_domain_boundary = FALSE
    proper_nesting_buffer = 2, 2, 2, 2, 2, 2
    // load_balance = FALSE
    efficiency_tolerance = 0.80
    combine_efficiency = 0.75
    // write_regrid_boxes = TRUE
    // read_regrid_boxes = TRUE
    // regrid_boxes_filename = "grid"
    check_nonrefined_tags = "ERROR"
    check_overlapping_patches = "ERROR"
    extend_tags_to_bdry = TRUE
    sequentialize_patch_indices = TRUE
    barrier_before_regrid = TRUE
    barrier_before_find_refinement = TRUE
    barrier_before_cluster = TRUE
    barrier_before_nest = TRUE
    barrier_before_limit = TRUE
    barrier_before_extend = TRUE
    barrier_before_balance = TRUE
    barrier_after_find_refinement = TRUE
    coalesce_boxes = FALSE
}
