#include <FTensor.hpp>
#include <iostream>

int main()
{
  const double H(0.08), L(0.5), W(0.25);
  
  // /// 2D strain region
  // const int N(1024);
  // const double corner_x (-0.08 - H*0.6/2), corner_y(-0.06 + H*0.8/2);
  // /// Along short (0.08) axis
  // std::cout << "    faults_2D=";
  // for (int i=0; i<N; ++i)
  //   {
  //     std::cout << "    " << (1600*H)/N << ", "
  //               << (corner_x + i*H*0.6/(N-1)) << ", "
  //               << (corner_y - i*H*0.8/(N-1)) << ", "
  //               << L << ", 36.8698976459,\n";
  //   }
  // /// Along long (0.2) axis
  // for (int i=0; i<N; ++i)
  //   {
  //     std::cout << "    " << (-1600*L)/N << ", "
  //               << (corner_x + i*L*0.8/(N-1)) << ", "
  //               << (corner_y + i*L*0.6/(N-1)) << ", "
  //               << H << ", -53.130102354,\n";
  //   }


  // /// 3D strain region
  // const int N(1024);
  // const double corner_x (-.2001 - H*0.6/2), corner_y(-.2001 + H*0.8/2);
  // std::cout << "    faults=";
  // /// Along short (0.08) axis
  // for (int i=0; i<N; ++i)
  //   {
  //     std::cout << "    " << (1600*H)/N << ", "
  //               << (corner_x + i*H*0.6/(N-1)) << ", "
  //               << (corner_y - i*H*0.8/(N-1)) << ", "
  //               << "-0.0001, "
  //               << L << ", " << W << ", 36.8698976459, 90, 0,\n";
  //   }
  // /// Along long (0.2) axis
  // for (int i=0; i<N; ++i)
  //   {
  //     std::cout << "    " << (-1600*L)/N << ", "
  //               << (corner_x + i*L*0.8/(N-1)) << ", "
  //               << (corner_y + i*L*0.6/(N-1)) << ", "
  //               << "-0.0001, "
  //               << H << ", " << W << ", -53.130102354, 90, 0,\n";
  //   }

  
  // Tetrahedra
  const int N(1024);
  const double corner_x (-.2001 - H*0.6/2), corner_y(-.2001 + H*0.8/2),
    corner_z (0.0001);
  
  FTensor::Tensor1<double,3> v0(corner_x, corner_y, corner_z);
  FTensor::Tensor1<double,3> v1(corner_x + L*0.8, corner_y + L*0.6, corner_z);
  FTensor::Tensor1<double,3> v2(v1(0),v1(1), corner_z + W);
  FTensor::Tensor1<double,3> v3(corner_x + H*0.6, corner_y - H*0.8, corner_z);
  FTensor::Tensor1<double,3> u, v, w, wmu, wmv, umv, umw;
  
  FTensor::Index<'a',3> a;
  FTensor::Index<'b',3> b;
  FTensor::Index<'c',3> c;

  u(a)=v1(a)-v0(a);
  v(a)=v2(a)-v0(a);
  w(a)=v3(a)-v0(a);
  
  wmu(a)=w(a)-u(a);
  wmv(a)=w(a)-v(a);
  umv(a)=u(a)-v(a);
  umw(a)=u(a)-w(a);
  
  std::cout << "    triangular_faults=";
  /// First make faults along w
  
  for (int i=0; i<N; ++i)
    {
      double fraction (i*1.0/(N-1));
      FTensor::Tensor1<double,3> v0_new, v1_new, v2_new;
      v0_new(a)=v0(a)+fraction*w(a);
      v1_new(a)=v1(a)+fraction*wmu(a);
      v2_new(a)=v2(a)+fraction*wmv(a);
      
      std::cout << "    "
                << v0_new(0) << "," << v0_new(1) << "," << v0_new(2) << ", "
                << v1_new(0) << "," << v1_new(1) << "," << v1_new(2) << ", "
                << v2_new(0) << "," << v2_new(1) << "," << v2_new(2) << ", "
                << (1600*H/N)*0.8 << ","
                << (1600*H/N)*0.6 << ",0,\n";
    }

  // for (int i=0; i<N; ++i)
  //   {
  //     double fraction (i*1.0/(N-1));
  //     FTensor::Tensor1<double,3> v0_new, v1_new, v2_new;
  //     v0_new(a)=v0(a)+fraction*w(a);
  //     v1_new(a)=v1(a)+fraction*w(a);
  //     v2_new(a)=v2(a)+fraction*w(a);
      
  //     std::cout << "    "
  //               << v0_new(0) << "," << v0_new(1) << "," << v0_new(2) << ", "
  //               << v1_new(0) << "," << v1_new(1) << "," << v1_new(2) << ", "
  //               << v2_new(0) << "," << v2_new(1) << "," << v2_new(2) << ", "
  //               << (1600*H/N)*0.8 << ","
  //               << (1600*H/N)*0.6 << ",0,\n";
  //   }

  // FTensor::Tensor1<double,3> v4(corner_x, corner_y, corner_z + W);
  // for (int i=0; i<N; ++i)
  //   {
  //     double fraction (i*1.0/(N-1));
  //     FTensor::Tensor1<double,3> v0_new, v2_new, v4_new;
  //     v0_new(a)=v0(a)+fraction*w(a);
  //     v2_new(a)=v2(a)+fraction*w(a);
  //     v4_new(a)=v4(a)+fraction*w(a);
      
  //     std::cout << "    "
  //               << v0_new(0) << "," << v0_new(1) << "," << v0_new(2) << ", "
  //               << v2_new(0) << "," << v2_new(1) << "," << v2_new(2) << ", "
  //               << v4_new(0) << "," << v4_new(1) << "," << v4_new(2) << ", "
  //               << (1600*H/N)*0.8 << ","
  //               << (1600*H/N)*0.6 << ",0,\n";
  //   }


  /// Make transverse faults along u
  FTensor::Tensor1<double,3> v5(v3(0), v3(1), corner_z + W);
  for (int i=0; i<N; ++i)
    {
      double fraction (i*1.0/(N-1));
      FTensor::Tensor1<double,3> v0_new, v3_new, v5_new;
      v0_new(a)=v0(a)+fraction*u(a);
      v3_new(a)=v3(a)+fraction*umw(a);
      v5_new(a)=v3(a)-fraction*wmv(a);
      
      std::cout << "    "
                << v0_new(0) << "," << v0_new(1) << "," << v0_new(2) << ", "
                << v3_new(0) << "," << v3_new(1) << "," << v3_new(2) << ", "
                << v5_new(0) << "," << v5_new(1) << "," << v5_new(2) << ", "
                << -(1600*L/N)*0.6 << ","
                << (1600*L/N)*0.8 << ",0,\n";
    }

  for (int i=0; i<N; ++i)
    {
      double fraction (i*1.0/(N-1));
      FTensor::Tensor1<double,3> v0_new, v4_new, v5_new;
      v0_new(a)=v0(a)+fraction*u(a);
      v5_new(a)=v3(a)-fraction*wmv(a);
      v4_new(a)=v0(a)+fraction*v(a);
      
      std::cout << "    "
                << v0_new(0) << "," << v0_new(1) << "," << v0_new(2) << ", "
                << v5_new(0) << "," << v5_new(1) << "," << v5_new(2) << ", "
                << v4_new(0) << "," << v4_new(1) << "," << v4_new(2) << ", "
                << -(1600*L/N)*0.6 << ","
                << (1600*L/N)*0.8 << ",0,\n";
    }

  // FTensor::Tensor1<double,3> v5(v3(0), v3(1), corner_z + W);
  // for (int i=0; i<N; ++i)
  //   {
  //     double fraction (i*1.0/(N-1));
  //     FTensor::Tensor1<double,3> v0_new, v3_new, v5_new;
  //     v0_new(a)=v0(a)+fraction*u(a);
  //     v3_new(a)=v3(a)+fraction*u(a);
  //     v5_new(a)=v5(a)+fraction*u(a);
      
  //     std::cout << "    "
  //               << v0_new(0) << "," << v0_new(1) << "," << v0_new(2) << ", "
  //               << v3_new(0) << "," << v3_new(1) << "," << v3_new(2) << ", "
  //               << v5_new(0) << "," << v5_new(1) << "," << v5_new(2) << ", "
  //               << -(1600*L/N)*0.6 << ","
  //               << (1600*L/N)*0.8 << ",0,\n";
  //   }
  // for (int i=0; i<N; ++i)
  //   {
  //     double fraction (i*1.0/(N-1));
  //     FTensor::Tensor1<double,3> v0_new, v4_new, v5_new;
  //     v0_new(a)=v0(a)+fraction*u(a);
  //     v4_new(a)=v4(a)+fraction*u(a);
  //     v5_new(a)=v5(a)+fraction*u(a);
      
  //     std::cout << "    "
  //               << v0_new(0) << "," << v0_new(1) << "," << v0_new(2) << ", "
  //               << v5_new(0) << "," << v5_new(1) << "," << v5_new(2) << ", "
  //               << v4_new(0) << "," << v4_new(1) << "," << v4_new(2) << ", "
  //               << -(1600*L/N)*0.6 << ","
  //               << (1600*L/N)*0.8 << ",0,\n";
  //   }
}
