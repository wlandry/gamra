def options(opt):
    opt.load('compiler_cxx FTensor okada boost cxx14')

    samrai=opt.add_option_group('SAMRAI Options')
    samrai.add_option('--samrai-dir',
                   help='Base directory where samrai is installed')
    samrai.add_option('--samrai-incdir',
                   help='Directory where samrai include files are installed')
    samrai.add_option('--samrai-libdir',
                   help='Directory where samrai library files are installed')
    samrai.add_option('--samrai-libs',
                   help='Names of the samrai libraries without prefix or suffix\n'
                   '(e.g. "SAMRAI_appu SAMRAI_algs SAMRAI_solv"')

    muparser=opt.add_option_group('muParser Options')
    muparser.add_option('--muparser-dir',
                   help='Base directory where muparser is installed')
    muparser.add_option('--muparser-incdir',
                   help='Directory where muparser include files are installed')
    muparser.add_option('--muparser-libdir',
                   help='Directory where muparser library files are installed')
    muparser.add_option('--muparser-libs',
                   help='Names of the muparser libraries without prefix or suffix\n'
                   '(e.g. "muparser"')

    hdf5=opt.add_option_group('HDF5 Options')
    hdf5.add_option('--hdf5-dir',
                   help='Base directory where HDF5 is installed')
    hdf5.add_option('--hdf5-incdir',
                   help='Directory where HDF5 include files are installed')
    hdf5.add_option('--hdf5-libdir',
                   help='Directory where HDF5 library files are installed')
    hdf5.add_option('--hdf5-libs',
                   help='Names of the HDF5 libraries without prefix or suffix\n'
                   '(e.g. "hdf5"')

def configure(conf):
    conf.setenv('debug')
    configure_variant(conf);
    conf.setenv('release')
    configure_variant(conf);
    conf.setenv('prof')
    configure_variant(conf);

def configure_variant(conf):
    conf.load('compiler_cxx FTensor okada boost cxx14')
    # conf.check_boost()
    conf.check_boost(lib='filesystem system')

    # Find SAMRAI
    if conf.options.samrai_dir:
        if not conf.options.samrai_incdir:
            conf.options.samrai_incdir=conf.options.samrai_dir + "/include"
        if not conf.options.samrai_libdir:
            conf.options.samrai_libdir=conf.options.samrai_dir + "/lib"
    frag="#include \"SAMRAI/SAMRAI_config.h\"\n" + 'int main()"\n' \
        + "{}\n"
    if not conf.options.samrai_incdir:
        conf.options.samrai_incdir='/usr/include'

    conf.check_cxx(msg="Checking for SAMRAI",
                   header_name='SAMRAI/SAMRAI_config.h',
                   includes=[conf.options.samrai_incdir], uselib_store='samrai',
                   libpath=[conf.options.samrai_libdir],
                   rpath=[conf.options.samrai_libdir],
                   lib=['SAMRAI_appu', 'SAMRAI_algs', 'SAMRAI_solv',
                        'SAMRAI_geom', 'SAMRAI_mesh', 'SAMRAI_math',
                        'SAMRAI_pdat', 'SAMRAI_xfer', 'SAMRAI_hier',
                        'SAMRAI_tbox'],
                   use=['BOOST'])

    # Find muParser
    if conf.options.muparser_dir:
        if not conf.options.muparser_incdir:
            conf.options.muparser_incdir=conf.options.muparser_dir + "/include"
        if not conf.options.muparser_libdir:
            conf.options.muparser_libdir=conf.options.muparser_dir + "/lib"
    frag="#include \"muParser.h\"\n" + 'int main()"\n' \
        + "{}\n"

    conf.check_cxx(msg="Checking for muParser",
                  header_name='muParser.h',
                  includes=[conf.options.muparser_incdir], uselib_store='muparser',
                  libpath=[conf.options.muparser_libdir],
                  rpath=[conf.options.muparser_libdir],
                  lib=['muparser'])

    # Find Parallel HDF5
    if conf.options.hdf5_dir:
        if not conf.options.hdf5_incdir:
            conf.options.hdf5_incdir=conf.options.hdf5_dir + "/include"
        if not conf.options.hdf5_libdir:
            conf.options.hdf5_libdir=conf.options.hdf5_dir + "/lib"
    
    if not conf.options.hdf5_incdir and not conf.options.hdf5_libdir \
       and not conf.options.hdf5_libs:
        hdf5_config=[[[],[],['hdf5']],
                     [['/usr/include/hdf5/openmpi'], [],
                      ['hdf5_openmpi']]]
    else:
        if conf.options.hdf5_incdir:
            hdf5_inc=[conf.options.hdf5_incdir]
        else:
            hdf5_inc=[]

        hdf5_libs=["hdf5"]
        if conf.options.hdf5_libs:
            hdf5_libs=conf.options.hdf5_libs.split()

        hdf5_config=[[hdf5_inc,[conf.options.hdf5_libdir],hdf5_libs]]
        
    frag="#include \"mpi.h\"\n#include \"hdf5.h\"\n" + 'int main()"\n' \
        + "{}\n"

    found_hdf5=False
    for config in hdf5_config:
        try:
            conf.check_cxx(msg="Checking for hdf5",
                           header_name='hdf5.h',
                           includes=[conf.options.hdf5_incdir], uselib_store='hdf5',
                           libpath=[conf.options.hdf5_libdir],
                           rpath=[conf.options.hdf5_libdir],
                           lib=['hdf5'])
        except conf.errors.ConfigurationError:
            continue
        else:
            found_hdf5=True
            break
    if not found_hdf5:
        conf.fatal("Could not find HDF5 C libraries")
            

    # Optimization flags
    optimize_msg="Checking for optimization flag "
    optimize_fragment="int main() {}\n"
    optimize_flags=['-Ofast','-O3','-O2','-O']
    for flag in optimize_flags:
        try:
            conf.check_cxx(msg=optimize_msg+flag, fragment=optimize_fragment,
                           cxxflags=flag, uselib_store='optimize')
        except conf.errors.ConfigurationError:
            continue
        else:
            found_optimize=True
            break

def build(bld):
    default_flags=['-Wall', '-Wextra', '-Wconversion', '-Wvla']
    cxxflags_variant= {'release' : ['-Ofast', '-DTESTING=0'],
                    'prof' : ['-pg','-Ofast', '-DTESTING=0'],
                    'debug' : ['-g']}
    linkflags_variant={'release' : [],
                       'prof' : ['-pg'],
                       'debug' : []}

    use_array=['samrai','muparser','hdf5','FTensor','okada','BOOST','cxx14']
    if bld.variant=='release':
        use_array.append('optimize')

    bld.program(
        features     = ['cxx','cprogram'],
        source       = ['src/main.cxx',
                        'src/setup_visit_fields.cxx',
                        'src/Input_Expression/eval.cxx',
                        'src/Input_Expression/operator_equals.cxx',
                        'src/Input_Expression/Init/Init.cxx',
                        'src/Input_Expression/Init/okada_displacement.cxx',
                        'src/Input_Expression/Init/okada_internal.cxx',
                        'src/Input_Expression/Init/okada_strain.cxx',
                        'src/Input_Expression/Init/strain_volume_displacement.cxx',
                        'src/Input_Expression/Init/strain_volume_internal.cxx',
                        'src/Input_Expression/Init/strain_volume_strain.cxx',
                        'src/Input_Expression/Init/triangle_displacement.cxx',
                        'src/Input_Expression/Init/triangle_internal.cxx',
                        'src/Input_Expression/Init/triangle_strain.cxx',
                        'src/Input_Expression/Init/variable_factory.cxx',
                        'src/Visit_Writer/packDerivedDataIntoDoubleBuffer.cxx',
                        'src/Visit_Writer/pack_induced_strain.cxx',
                        'src/Visit_Writer/pack_induced_stress.cxx',
                        'src/Visit_Writer/pack_applied_strain.cxx',
                        'src/Visit_Writer/pack_vector.cxx',
                        'src/Visit_Writer/pack_Input_Expression.cxx',
                        'src/Visit_Writer/pack_christof.cxx',
                        'src/Elastic/Adaptive_Solver/Adaptive_Solver/Adaptive_Solver.cxx',
                        'src/Elastic/Adaptive_Solver/Adaptive_Solver/extract_strains.cxx',
                        'src/Elastic/Adaptive_Solver/Adaptive_Solver/extract_faults.cxx',
                        'src/Elastic/Adaptive_Solver/applyGradientDetector.cxx',
                        'src/Elastic/Adaptive_Solver/initializeLevelData/initializeLevelData.cxx',
                        'src/Elastic/Adaptive_Solver/initializeLevelData/initialize_cell_data.cxx',
                        'src/Elastic/Adaptive_Solver/initializeLevelData/initialize_side_data.cxx',
                        'src/Elastic/Adaptive_Solver/initializeLevelData/initialize_edge_data.cxx',
                        'src/Elastic/Adaptive_Solver/initializeLevelData/initialize_node_data.cxx',
                        'src/Elastic/Adaptive_Solver/resetHierarchyConfiguration.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/solve_hierarchy.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/fix_moduli.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/setup_fault_corrections/setup_fault_corrections.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/setup_fault_corrections/correct_rhs.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/setup_fault_corrections/compute_dv/compute_dv.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/setup_fault_corrections/compute_dv/corrections_from_strains.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/setup_fault_corrections/compute_dv/corrections_from_faults.cxx',
                        'src/Elastic/Adaptive_Solver/Strain/Strain.cxx',
                        'src/Elastic/Adaptive_Solver/Strain/intersect_2D.cxx',
                        'src/Elastic/Adaptive_Solver/Strain/intersect_3D.cxx',
                        'src/Elastic/Adaptive_Solver/Affine_Parameters_2D.cxx',
                        'src/Elastic/Adaptive_Solver/Affine_Parameters_3D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/V_Refine/refine.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/V_Refine/refine_box.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/V_Refine/refine_along_line.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/Coarse_Fine_Boundary_Refine/Coarse_Fine_Boundary_Refine.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/Coarse_Fine_Boundary_Refine/refine.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/Coarse_Fine_Boundary_Refine/refine_box.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/Coarse_Fine_Boundary_Refine/Update_V_2D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/Coarse_Fine_Boundary_Refine/Correction_2D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/Coarse_Fine_Boundary_Refine/Update_V_3D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/V_Coarsen_Patch_Strategy/postprocessCoarsen.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/V_Coarsen_Patch_Strategy/coarsen_2D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/V_Coarsen_Patch_Strategy/coarsen_3D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/V_Coarsen_Patch_Strategy/fix_boundary_elements_2D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/V_Coarsen_Patch_Strategy/fix_boundary_elements_3D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Boundary_Conditions/Boundary_Conditions.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Boundary_Conditions/set_physical_boundary.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Boundary_Conditions/set_regular_boundary/set_regular_boundary.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Boundary_Conditions/set_regular_boundary/set_dirichlet.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Boundary_Conditions/set_regular_boundary/set_shear_stress.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/Operators.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/computeCompositeResidualOnLevel/computeCompositeResidualOnLevel.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/computeCompositeResidualOnLevel/residual_2D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/computeCompositeResidualOnLevel/residual_3D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/computeResidualNorm.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/deallocateOperatorState.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/finalizeCallback.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/initializeOperatorState.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/postprocessOneCycle.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/prolongErrorAndCorrect.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/restrictResidual.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/restrictSolution.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/smooth/smooth.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/smooth/Gauss_Seidel_red_black_2D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/smooth/Gauss_Seidel_red_black_3D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/solveCoarsestLevel.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/update_V_2D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/update_V_3D.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/ghostfill/ghostfill.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/ghostfill/set_physical_boundaries.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/ghostfill_nocoarse.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/refine.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/coarsen_resid.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Operators/coarsen_solution.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Multigrid.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/Destructor.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/createVectorWrappers.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/deallocateSolverState.cxx',
                        'src/Elastic/Adaptive_Solver/solve_hierarchy/Multigrid/getFromInput.cxx',
                        'src/Stokes/Adaptive_Solver/Adaptive_Solver.cxx',
                        'src/Stokes/Adaptive_Solver/fix_viscosity.cxx',
                        'src/Stokes/Adaptive_Solver/applyGradientDetector.cxx',
                        'src/Stokes/Adaptive_Solver/initializeLevelData.cxx',
                        'src/Stokes/Adaptive_Solver/resetHierarchyConfiguration.cxx',
                        'src/Stokes/Adaptive_Solver/solve_hierarchy.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/Operators.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/computeCompositeResidualOnLevel.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/residual_2D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/residual_3D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/computeResidualNorm.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/deallocateOperatorState.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/finalizeCallback.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/initializeOperatorState.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/P_Refine.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/V_Refine.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/Resid_Coarsen.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/V_Coarsen/coarsen_2D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/V_Coarsen/coarsen_3D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/P_Boundary_Refine/refine.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/P_Boundary_Refine/Update_P_2D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/P_Boundary_Refine/Update_P_3D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/V_Boundary_Refine/refine.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/V_Boundary_Refine/refine_box.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/V_Boundary_Refine/Update_V_2D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/initializeOperatorState/V_Boundary_Refine/Update_V_3D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/postprocessOneCycle.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/prolongErrorAndCorrect.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/restrictResidual.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/restrictSolution.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/smoothError.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/smooth_Tackley_2D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/smooth_Tackley_3D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/set_physical_boundaries.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/set_boundary.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/solveCoarsestLevel.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/smooth_V_2D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/smooth_V_3D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/xeqScheduleGhostFill.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/xeqScheduleGhostFillNoCoarse.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/xeqScheduleProlongation.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/xeqScheduleRRestriction.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/xeqScheduleURestriction.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/V_Coarsen_Patch_Strategy/postprocessCoarsen_2D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Operators/V_Coarsen_Patch_Strategy/postprocessCoarsen_3D.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Multigrid.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/Multigrid_Destructor.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/createVectorWrappers.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/deallocateSolverState.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/destroyVectorWrappers.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/enableLogging.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/getFromInput.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/initializeSolverState.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/setBcObject.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/setBoundaries.cxx',
                        'src/Stokes/Adaptive_Solver/Multigrid/solveSystem.cxx'],
        target       = 'gamra',
        cxxflags     = cxxflags_variant[bld.variant] + default_flags,
        linkflags    = linkflags_variant[bld.variant],
        includes = ['src'],
        use=use_array
        )

    bld.program(
        features     = ['cxx','cprogram'],
        source       = ['input/benchmarks/Elastic/generate_strain_faults.cxx'],
        target       = 'generate_strain_faults',
        cxxflags     = cxxflags_variant[bld.variant] + default_flags,
        linkflags    = linkflags_variant[bld.variant],
        use=use_array
        )
    
from waflib.Build import BuildContext, CleanContext, \
    InstallContext, UninstallContext

for x in 'debug prof release'.split():
    for y in (BuildContext, CleanContext, InstallContext, UninstallContext):
        name = y.__name__.replace('Context','').lower()
        class tmp(y):
            if x=='release':
                cmd = name
            else:
                cmd = name + '_' + x
            variant = x
